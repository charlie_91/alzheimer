#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  2 22:22:45 2021

@author: charlottolofsson
"""


# =============================================================================
# Modules
# =============================================================================

# Data handling and plotting
import pandas as pd
import numpy as np

# Preprocessing and models
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import PowerTransformer
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.model_selection import cross_val_score
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

# Scoring
from sklearn.metrics import matthews_corrcoef
from sklearn.metrics import make_scorer

# Functions from other scripts
import sys
sys.path.append('../code/functions')
from learning_curve import learning_curve_plot
from evaluator import model_evaluator
from evaluator_default import model_evaluator_default


# =============================================================================
# Loading the data
# =============================================================================

# Block A
dataA = pd.read_csv('../data/blocks/dataA.csv', index_col=0)
dataA_dummy = pd.get_dummies(dataA)
dataA_descr = dataA_dummy.describe()

# Block B
dataB = pd.read_csv('../data/blocks/dataB.csv', index_col=0)
dataB_dummy = pd.get_dummies(dataB)
dataB_descr = dataB_dummy.describe()

# Block C
dataC = pd.read_csv('../data/blocks/dataC.csv', index_col=0)
dataC_descr = dataC.describe()

# Block D
dataD = pd.read_csv('../data/blocks/dataD.csv', index_col=0)
dataD_descr = dataD.describe()

# Block E
dataE = pd.read_csv('../data/blocks/dataE.csv', index_col=0)
dataE_descr = dataE.describe()


# Target
target_name = 'a'
target = pd.read_csv('../data/blocks/targetFull.csv', index_col=0)

targetAll = pd.read_csv('../data/blocks/targetAll.csv', index_col=0)




# Separating train and test data
# -----------------------------------------------------------------------------

test_index = pd.read_csv('../data/blocks/test_subjects.csv')
test_index = test_index.iloc[:, 1]


# Training data

xA = dataA_dummy.drop(test_index)
yA = np.array(targetAll.loc[xA.index]).reshape(-1,)

xB = dataB_dummy.drop(test_index)
yB = np.array(targetAll.loc[xB.index]).reshape(-1,)

xC = dataC.drop(test_index)
yC = np.array(targetAll.loc[xC.index]).reshape(-1,)

xD = dataD.drop(test_index)
yD = np.array(targetAll.loc[xD.index]).reshape(-1,)

xE = dataE.drop(test_index)
yE = np.array(targetAll.loc[xE.index]).reshape(-1,)
         


# Test data 

xA_test = dataA_dummy.loc[test_index]
yA_test = np.array(targetAll.loc[xA_test.index]).reshape(-1,)

xB_test = dataB_dummy.loc[test_index]
yB_test = np.array(targetAll.loc[xB_test.index]).reshape(-1,)

xC_test = dataC.loc[test_index]
yC_test = np.array(targetAll.loc[xC_test.index]).reshape(-1,)

xD_test = dataD.loc[test_index]
yD_test = np.array(targetAll.loc[xD_test.index]).reshape(-1,)

xE_test = dataE.loc[test_index]
yE_test = np.array(targetAll.loc[xE_test.index]).reshape(-1,)



# Lists of the data and names
block_train = [xA, xB, xC, xD, xE]

block_target = [yA, yB, yC, yD, yE]

block_test = [xA_test, xB_test, xC_test, xD_test, xE_test]

block_target_test = [yA_test, yB_test, yC_test, yD_test, yE_test]

block_names = ['BlockA', 'BlockB', 'BlockC', 'BlockD', 'BlockE']



# =============================================================================
# Define function for model evaluation
# =============================================================================

# evaluate a model with a given number of repeats
def evaluate_model(model, X, y, repeats, splits, metric):
 	
    # prepare the cross-validation procedure
 	cv = RepeatedStratifiedKFold(n_splits=splits, 
                                 n_repeats=repeats, 
                                 random_state=1)
 	
    # evaluate model
 	scores = cross_val_score(model, X, y, 
                             scoring=metric, 
                             cv=cv, 
                             n_jobs=-1,
                             verbose=0)
 	return scores



# =============================================================================
# Evaluating classifiers
# =============================================================================

# Creating scorer for Matthews correlation coefficient
mcc_scorer = make_scorer(matthews_corrcoef)
metric = mcc_scorer
scaler = StandardScaler()
power = PowerTransformer(method='yeo-johnson')



# Creating models
lg_model = LogisticRegression()

aggressive_model = PassiveAggressiveClassifier()

forest_model = RandomForestClassifier()

knn_model = KNeighborsClassifier()

svc_model = SVC()


# List of models and names
models = [lg_model, aggressive_model, forest_model, knn_model, svc_model]
modelnames = ['LogisticRegression', 'PassiveAggressiveClassifier', 
              'RandomForest', 'KNN', 'SVC']



# Defining parameters for grid search 
# -----------------------------------------------------------------------------

# Logistic Regression
param_range_lr  = [0.0001, 0.001, 0.01, 0.1, 1.0, 10.0, 100.0, 1000.0]
param_grid_lr   = [{'m__C' : param_range_lr, 
                    'm__solver' : ['lbfgs', 'liblinear']}]


# Passive Aggressive Classifier
param_range_pag   = [0.0001, 0.001, 0.01, 0.1, 1.0, 10.0, 100.0, 1000.0]
param_grid_pag   = [{'m__C' : param_range_pag,
                     'm__loss': ['hinge','squared_hinge']}]


# Random Forest
param_range_forest1   = [20, 40, 60, 80, 100, 120, 140, 160]
param_range_forest2   = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, None]
param_grid_forest  = [{'m__n_estimators' : param_range_forest1, 
                       'm__criterion' : ['gini','entropy'], 
                       'm__max_depth': param_range_forest2,
                       'm__max_features': ['auto','sqrt', 'log']}]



# KNN
param_range_knc = [2, 4, 5, 6, 7, 8, 9, 10]
param_grid_knc = [{'m__n_neighbors' : param_range_knc,
                   'm__weights' : ['uniform', 'distance'], 
                   'm__algorithm' : ['auto', 'ball_tree', 'kd_tree', 'brute'],
                   'm__p' : [1, 2]}]



# SVC
param_range_svc1 = [0.0001, 0.001, 0.01, 0.1, 1.0, 10.0, 100.0] 
param_range_svc2 = [0.0001, 0.0005, 0.001, 0.01, 0.05, 0.1, 1.0]         
param_grid_svc  = [{'m__C': param_range_svc1, 'm__kernel': ['linear']},
                   {'m__C': param_range_svc1, 'm__gamma': param_range_svc2, 
                    'm__kernel': ['rbf']}]



# List of the parameters for all the models
params = [param_grid_lr, param_grid_pag, param_grid_forest, 
          param_grid_knc, param_grid_svc]



# =============================================================================
# Standardised data
# =============================================================================
# =============================================================================
# Grid search
# =============================================================================


# Dictionary for storing results for all the blocks
grid_search_scores = {}


for x, y, name in zip(block_train, block_target, block_names):
    
    # Dictionary for storing results for all the models
    scores = {}
    
    for model, modelname, param in zip(models, modelnames, params):
        
        # Creating pipeline
        pipeline = Pipeline(steps=[('s', scaler), ('m', model)])
    
        # Learning curve, default parameters
        # learning_curve_plot(pipeline, name + '_' + modelname + '_default.pdf', x_train=x, y_train=y)
        
        # Grid search
        gs = GridSearchCV(estimator=pipeline, 
                          param_grid=param, 
                          scoring=metric, 
                          cv=5,
                          n_jobs=-1)
        
        gs = gs.fit(x, y)
        best = gs.best_estimator_
        
        # Storing best scores
        scores[modelname] =  [gs.best_score_, gs.best_params_]
        
    
        # Learning curve, best parameters
        # learning_curve_plot(best, name + '_' + modelname + '_best.pdf', x_train=x, y_train=y)
    
    grid_search_scores[name] = scores
    
    
    
# Creating dataframe and exporting as csv file    
grid_search_scores_df = pd.DataFrame(grid_search_scores)
grid_search_scores_df.to_csv('grid_search_scores.csv')



# =============================================================================
# Repeated stratified k-fold with best parameters
# =============================================================================


# Dictionary for storing results for all the blocks
k_fold_scores = {}

for x, y, name in zip(block_train, block_target, block_names):
    
    # Dictionary for storing results for all the models
    scores = {}
    
    for model, modelname in zip(models, modelnames):
        
        # Creating pipeline
        pipeline = Pipeline(steps=[('s', scaler), ('m', model)])
        
        # Fitting pipeline
        pipeline.fit(x, y)
        
        # Fitting model with best parameters
        best_parameters = grid_search_scores[name][modelname][1]
        pipeline.set_params(**best_parameters)
        
        # Repeated stratified k-fold
        rkf_scores = evaluate_model(pipeline,
                                    X=x, 
                                    y=y, 
                                    repeats=10,
                                    splits=5,
                                    metric=metric) 
        
        # Getting mean scores and storing in dictionary
        rkf_scores_mean = round(np.mean(rkf_scores),4)
        rkf_scores_std = round(np.std(rkf_scores, ddof=1),4)
        scores[modelname] = {'Mean score ' : rkf_scores_mean, 
                             'Std score' : rkf_scores_std}
        
    
    k_fold_scores[name] = scores
    

# Creating dataframe and exporting as csv file  
k_fold_scores_df = pd.DataFrame(k_fold_scores)
k_fold_scores_df.to_csv('k_fold_scores.csv')




# =============================================================================
# Yeo-Johnson power transformed
# =============================================================================
# =============================================================================
# Grid search
# =============================================================================


# Dictionary for storing results for all the blocks
grid_search_scores_yeo = {}


for x, y, name in zip(block_train, block_target, block_names):
    
    # Dictionary for storing results for all the models
    scores = {}
    
    for model, modelname, param in zip(models, modelnames, params):
        
        # Creating pipeline
        pipeline = Pipeline(steps=[('s', scaler), ('p', power), ('m', model)])
    
        # Learning curve, default parameters
        # learning_curve_plot(pipeline, name + '_' + modelname + '_default.pdf', x_train=x, y_train=y)
        
        # Grid search
        gs = GridSearchCV(estimator=pipeline, 
                          param_grid=param, 
                          scoring=metric, 
                          cv=5,
                          n_jobs=-1)
        
        gs = gs.fit(x, y)
        best = gs.best_estimator_
        
        # Storing best scores
        scores[modelname] =  [gs.best_score_, gs.best_params_]
        
    
        # Learning curve, best parameters
        # learning_curve_plot(best, name + '_' + modelname + '_best.pdf', x_train=x, y_train=y)
    
    grid_search_scores_yeo[name] = scores
    
    
    
# Creating dataframe and exporting as csv file    
grid_search_scores_yeo_df = pd.DataFrame(grid_search_scores_yeo)
grid_search_scores_yeo_df.to_csv('grid_search_scores_yeo.csv')



# =============================================================================
# Repeated stratified k-fold with best parameters
# =============================================================================


# Dictionary for storing results for all the blocks
k_fold_scores_yeo = {}

for x, y, name in zip(block_train, block_target, block_names):
    
    # Dictionary for storing results for all the models
    scores = {}
    
    for model, modelname in zip(models, modelnames):
        
        # Creating pipeline
        pipeline = Pipeline(steps=[('s', scaler), ('p', power), ('m', model)])
        
        # Fitting pipeline
        pipeline.fit(x, y)
        
        # Fitting model with best parameters
        best_parameters = grid_search_scores_yeo[name][modelname][1]
        pipeline.set_params(**best_parameters)
        
        # Repeated stratified k-fold
        rkf_scores = evaluate_model(pipeline,
                                    X=x, 
                                    y=y, 
                                    repeats=10,
                                    splits=5,
                                    metric=metric) 
        
        # Getting mean scores and storing in dictionary
        rkf_scores_mean = round(np.mean(rkf_scores),4)
        rkf_scores_std = round(np.std(rkf_scores, ddof=1),4)
        scores[modelname] = {'Mean score ' : rkf_scores_mean, 
                             'Std score' : rkf_scores_std}
        
    
    k_fold_scores_yeo[name] = scores
    

# Creating dataframe and exporting as csv file  
k_fold_scores_yeo_df = pd.DataFrame(k_fold_scores_yeo)
k_fold_scores_yeo_df.to_csv('k_fold_scores_yeo.csv')


# =============================================================================
# Predicting test labels
# =============================================================================


# Dictionary for storing results for all the blocks
test_scores = {}

for x, y, x_test, y_test, name in zip(block_train, block_target, block_test, 
                                      block_target_test, block_names):
    
    # Dictionary for storing results for all the models
    scores = {}
    
    for model, modelname in zip(models, modelnames):
        
        # Creating pipeline
        pipeline = Pipeline(steps=[('s', scaler), ('m', model)])
        
        # Fitting pipeline
        pipeline.fit(x, y)
        
        # Fitting model with best parameters
        best_parameters = grid_search_scores[name][modelname][1]
        pipeline.set_params(**best_parameters)
        
        # Predicting test labels and scoring
        pred = pipeline.predict(x_test)
        mcc_score = matthews_corrcoef(y_test, pred)
        
        scores[modelname] = mcc_score
        

    test_scores[name] = scores


# Creating dataframe and exporting as csv file      
test_scores_df = pd.DataFrame(test_scores)
test_scores_df.to_csv('test_scores.csv')
    


