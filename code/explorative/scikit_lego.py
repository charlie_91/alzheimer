#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  7 08:35:41 2021

@author: charlottolofsson
"""

# =============================================================================
# Modules
# =============================================================================

import pandas as pd
import numpy as np
import matplotlib.pylab as plt
from pandas.plotting import parallel_coordinates
from sklego.decomposition import UMAPOutlierDetection
from sklego.decomposition import PCAOutlierDetection

# Functions from other scripts
import sys
sys.path.append('../code/functions')
from data_preparation import dataPrep


# =============================================================================
# Data
# =============================================================================

# Target
targetAll = pd.read_csv('../data/blocks/targetAll.csv', index_col=0)


# Block A
dataA = pd.read_csv('../data/blocks/dataA.csv', index_col=0)
dataA_dummy = pd.get_dummies(dataA)
blockAStd = dataPrep(dataA_dummy, dataA_dummy.columns, std=True)
xA = blockAStd.copy()
yA = np.squeeze(targetAll.loc[xA.index].values).astype(int)


# Block B
dataB = pd.read_csv('../data/blocks/dataB.csv', index_col=0)
dataB_dummy = pd.get_dummies(dataB)
blockBStd = dataPrep(dataB_dummy, dataB_dummy.columns, std=True)
xB = blockBStd.copy()
yB = np.squeeze(targetAll.loc[xB.index].values).astype(int)


# Block C
dataC = pd.read_csv('../data/blocks/dataC.csv', index_col=0)
blockCStd = dataPrep(dataC, dataC.columns, std=True)
xC = blockCStd.copy()
yC = np.squeeze(targetAll.loc[xC.index].values).astype(int)


# Block D
dataD = pd.read_csv('../data/blocks/dataD.csv', index_col=0)
blockDStd = dataPrep(dataD, dataD.columns, std=True)
xD = blockDStd.copy()
yD = np.squeeze(targetAll.loc[xD.index].values).astype(int)


# Block E
dataE = pd.read_csv('../data/blocks/dataE.csv', index_col=0)
blockEStd = dataPrep(dataE, dataE.columns, std=True)
xE = blockEStd.copy()
yE = np.squeeze(targetAll.loc[xE.index].values).astype(int)


# =============================================================================
# Decomposition based detection
# =============================================================================


def plot_model(model, data, dataname, components, threshold):
    """
    Function based on code from source: 
    https://scikit-lego.readthedocs.io/en/latest/outliers.html

    Parameters
    ----------
    mod : sklego model
        The model to use when detecting outliers.
    data : Dataframe
        Containing the data to use.
    dataname : String
        Name of the block. Used when saving figure.
    components : Int
        Number of components used in the model.
    threshold : Float
        Threshold value used in the model.

    Returns
    -------
    None.

    """
    model = model(n_components=components, threshold=threshold).fit(data)
    X = data.copy()
    X['label'] = model.predict(X)

    plt.figure(figsize=(20, 10))
    plt.subplot(121)
    parallel_coordinates(X.loc[lambda d: d['label'] == 1], 
                         class_column='label', 
                         alpha=0.5)
    parallel_coordinates(X.loc[lambda d: d['label'] == -1], 
                         class_column='label', 
                         color='red', 
                         alpha=0.7)
    plt.title("outlier shown via parallel coordinates")
    plt.xticks(rotation=25)
    

    if components == 2:
        plt.subplot(122)
        X_reduced = model.transform(data)
        plt.scatter(X_reduced[:, 0], X_reduced[:, 1], c=X['label'])
        plt.title("outlier shown in 2d")
        
    plt.savefig(dataname + '_outlier_detection.png')        
    plt.show()



# =============================================================================
# Creating plots for the different blocks
# =============================================================================

blockdata = [xA, xB, xC, xD, xE]
dataname = ['blockA', 'blockB', 'blockC', 'blockD', 'blockE']


# PCA
for data, name in zip(blockdata, dataname):
    plot_model(PCAOutlierDetection, data, name, 
               components=2, threshold=0.1)

# UMAP
for data, name in zip(blockdata, dataname):
    plot_model(UMAPOutlierDetection, data, dataname, 
               components=2, threshold=0.2)
    

