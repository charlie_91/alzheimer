#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 18 19:19:06 2021

@author: charlottolofsson
"""

# =============================================================================
# Modules
# =============================================================================

# Data handling and plotting
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import phik


# =============================================================================
# Loading the data
# =============================================================================

# Block A
dataA = pd.read_csv('../data/blocks/dataA.csv', index_col=0)
dataA_dummy = pd.get_dummies(dataA)

# Correlation
corrA = dataA_dummy.corr()
# Phi_k correlation
corrAPhi = dataA_dummy.phik_matrix()


# Block B
dataB = pd.read_csv('../data/blocks/dataB.csv', index_col=0)
dataB_dummy = pd.get_dummies(dataB)

# Correlation
corrB = dataB.corr()
# Phi_k correlation
corrBPhi = dataB.phik_matrix()


# Block C
dataC = pd.read_csv('../data/blocks/dataC.csv', index_col=0)

# Correlation
corrC = dataC.corr()
# Phi_k correlation
corrCPhi = dataC.phik_matrix()


# Block D
dataD = pd.read_csv('../data/blocks/dataD.csv', index_col=0)

# Correlation
corrD = dataD.corr()
# Phi_k correlation
corrDPhi = dataD.phik_matrix()


# Block E
dataE = pd.read_csv('../data/blocks/dataE.csv', index_col=0)

# Correlation
corrE = dataE.corr()
# Phi_k correlation
corrEPhi = dataE.phik_matrix()



# Target
target_name = 'a'
target = pd.read_csv('../data/blocks/targetFull.csv', index_col=0)

targetAll = pd.read_csv('../data/blocks/targetAll.csv', index_col=0)



# =============================================================================
# Plotting heatmaps of correlations
# =============================================================================


# Block A
blockALabels = ['cognitive_symptom', 'previous_study',
                'family_control', 'control', 'orthopedic_control',
                'spouse_control']
fig, ax = plt.subplots(figsize=(20,10))
sns.heatmap(corrA,
            vmin=-1,
            vmax=1,
            cmap="YlGnBu",
            cbar=True,
            annot=True,
            square=True,
            fmt='.2f')
ax.set_xticklabels(blockALabels, rotation = 20, fontsize = 12)
ax.set_yticklabels(blockALabels, rotation = 0, fontsize = 12)
# ax.set_title('Block A', fontsize=28)
fig.savefig('correlationA.pdf')


# Block B
fig, ax = plt.subplots(figsize=(12,10))
blockBLables = ['age', 'edu_years', 'edu_level', 'smok', 'bp',
                'cardio_vasc', 'degree1_dem', 'female', 'male', 'E2/E2',
                'E2/E3', 'E2/E4', 'E3/E3', 'E3/E4', 'E4/E4']
blockBLabelsCat = ['age', 'edu_years', 'edu_level', 'smok', 'bp_recum_sys',
                   'cardiovascular', 'gender', 'degree1_dem', 'bl_apoe']
sns.heatmap(corrBPhi,
            vmin=-1,
            vmax=1,
            cmap="YlGnBu",
            cbar=True,
            annot=True,
            square=True,
            fmt='.2f')
ax.set_xticklabels(blockBLabelsCat, rotation = 40, fontsize = 15)
# ax.set_xticklabels([])
ax.set_yticklabels(blockBLabelsCat, rotation = 0, fontsize = 15)
# ax.set_title('Block B', fontsize=28)
fig.savefig('correlationBPhi.pdf')


# Block C
fig, ax = plt.subplots(figsize=(12,10))
sns.heatmap(corrC,
            vmin=-1,
            vmax=1,
            cmap="YlGnBu",
            cbar=True,
            annot=True,
            square=True,
            fmt='.2f')
ax.set_xticklabels(ax.get_yticklabels(), rotation = 30, fontsize = 15)
ax.set_yticklabels(ax.get_yticklabels(), rotation = 0, fontsize = 15)
# ax.set_title('Block C', fontsize=28)
fig.savefig('correlationC.pdf')


# Block D
fig, ax = plt.subplots(figsize=(12,10))
sns.heatmap(corrD,
            vmin=-1,
            vmax=1,
            cmap="YlGnBu",
            cbar=True,
            annot=True,
            square=True,
            fmt='.2f')
ax.set_xticklabels(ax.get_yticklabels(), rotation = 30, fontsize = 15)
ax.set_yticklabels(ax.get_yticklabels(), rotation = 0, fontsize = 15)
# ax.set_title('Block D', fontsize=28)
fig.savefig('correlationD.pdf')


# Block E
blockELabels = ['Anterior_hippocampus', 
                 'Posterior_hippocampus', 
                 'MISC', 'Meninges_PHC', 
                 'ERC', 'Br35', 'Br36',
                 'PHC', 'ColSul', 
                 'Meninges']
fig, ax = plt.subplots(figsize=(25,14))
sns.heatmap(corrE,
            vmin=-1,
            vmax=1,
            cmap="YlGnBu",
            cbar=True,
            annot=True,
            square=True,
            fmt='.2f')
ax.set_xticklabels(blockELabels, rotation = 40, fontsize = 15)
ax.set_yticklabels(blockELabels, rotation = 0, fontsize = 15)
# ax.set_title('Block E', fontsize=28)
fig.savefig('correlationE.pdf')

