#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 23 16:15:58 2021

@author: charlottolofsson
"""

# =============================================================================
# Modules
# =============================================================================

# Data handling and plotting
import pandas as pd
import numpy as np

# Preprocessing and models
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import PowerTransformer

# Functions from other scripts
import sys
sys.path.append('../code/functions')
from pca import pca


# =============================================================================
# Loading the data
# =============================================================================

# Block A
dataA = pd.read_csv('../data/blocks/dataA.csv', index_col=0)
dataA_dummy = pd.get_dummies(dataA)
dataA_descr = dataA_dummy.describe()

# Block B
dataB = pd.read_csv('../data/blocks/dataB.csv', index_col=0)
dataB_dummy = pd.get_dummies(dataB)
dataB_descr = dataB_dummy.describe()

# Block C
dataC = pd.read_csv('../data/blocks/dataC.csv', index_col=0)
dataC_descr = dataC.describe()

# Block D
dataD = pd.read_csv('../data/blocks/dataD.csv', index_col=0)
dataD_descr = dataD.describe()

# Block E
dataE = pd.read_csv('../data/blocks/dataE.csv', index_col=0)
dataE_descr = dataE.describe()



# Target
target_name = 'a'
target = pd.read_csv('../data/blocks/targetFull.csv', index_col=0)

targetAll = pd.read_csv('../data/blocks/targetAll.csv', index_col=0)


# =============================================================================
# Standardising data separately
# =============================================================================

"""
Standardising separately in order to only transform data in block D.
"""

sc = StandardScaler()
minmax = MinMaxScaler(feature_range=(1,2))


dataAstd = sc.fit_transform(dataA_dummy)
dataAsc = pd.DataFrame(dataAstd, columns=dataA_dummy.columns, 
                       index=dataA_dummy.index)


dataBstd = sc.fit_transform(dataB_dummy)
dataBsc = pd.DataFrame(dataBstd, columns=dataB_dummy.columns, 
                       index=dataB_dummy.index)


dataCstd = sc.fit_transform(dataC)
dataCsc = pd.DataFrame(dataCstd, columns=dataC.columns, 
                       index=dataC.index)


dataDstd = sc.fit_transform(dataD)
dataDsc = pd.DataFrame(dataDstd, columns=dataD.columns, 
                       index=dataD.index)

dataDminmax = minmax.fit_transform(dataD)
dataDmm = pd.DataFrame(dataDminmax, columns=dataD.columns, 
                       index=dataD.index)


dataEstd = sc.fit_transform(dataE)
dataEsc = pd.DataFrame(dataEstd, columns=dataE.columns, 
                       index=dataE.index)



# =============================================================================
# Power transformation on block D
# =============================================================================

# Yeo Johnson transformation 
pt_yeo = PowerTransformer(method='yeo-johnson')
dataD_yeo = pt_yeo.fit_transform(dataDstd)
dataDYeo = pd.DataFrame(dataD_yeo, columns=dataD.columns, index=dataD.index)


# BoxCox transformation
pt_box = PowerTransformer(method='box-cox')
dataD_box = pt_box.fit_transform(dataDmm)
dataDBox = pd.DataFrame(dataD_box, columns=dataD.columns, index=dataD.index)




# Separating train and test data
# -----------------------------------------------------------------------------

test_index = pd.read_csv('../data/blocks/test_subjects.csv')
test_index = test_index.iloc[:, 1]


# Training data

xA = dataAsc.drop(test_index)
yA = np.array(targetAll.loc[xA.index]).reshape(-1,)
yA_ = targetAll.loc[xA.index]

xB = dataBsc.drop(test_index)
yB = np.array(targetAll.loc[xB.index]).reshape(-1,)
yB_ = targetAll.loc[xB.index]

xC = dataCsc.drop(test_index)
yC = np.array(targetAll.loc[xC.index]).reshape(-1,)
yC_ = targetAll.loc[xC.index]

xD = dataDsc.drop(test_index)
yD = np.array(targetAll.loc[xD.index]).reshape(-1,)
yD_ = targetAll.loc[xD.index]

xE = dataEsc.drop(test_index)
yE = np.array(targetAll.loc[xE.index]).reshape(-1,)
yE_ = targetAll.loc[xE.index]
       


# Test data 

xA_test = dataAsc.loc[test_index]
yA_test = np.array(targetAll.loc[xA_test.index]).reshape(-1,)

xB_test = dataBsc.loc[test_index]
yB_test = np.array(targetAll.loc[xB_test.index]).reshape(-1,)

xC_test = dataCsc.loc[test_index]
yC_test = np.array(targetAll.loc[xC_test.index]).reshape(-1,)

xD_test = dataDsc.loc[test_index]
yD_test = np.array(targetAll.loc[xD_test.index]).reshape(-1,)

xE_test = dataEsc.loc[test_index]
yE_test = np.array(targetAll.loc[xE_test.index]).reshape(-1,)




# =============================================================================
# PCA
# =============================================================================


# Block A
pcaA = pca(xA, components=6, plot=True)
exVarA = pcaA['Cumulative calibrated explained variance']
exVarA.to_csv('exVarA.csv')


# Block B
pcaB = pca(xB, components=6, plot=True)
exVarB = pcaB['Cumulative calibrated explained variance']
exVarB.to_csv('exVarB.csv')


# Block C
pcaC = pca(xC, components=6, plot=True)
exVarC = pcaC['Cumulative calibrated explained variance']
exVarC.to_csv('exVarC.csv')


# Block D
pcaD = pca(xD, components=6, plot=True)
exVarD = pcaD['Cumulative calibrated explained variance']
exVarD.to_csv('exVarD.csv')

# Block E
pcaE = pca(xE, components=6, plot=True)
exVarE = pcaE['Cumulative calibrated explained variance']
exVarE.to_csv('exVarE.csv')




