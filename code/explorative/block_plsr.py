#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  5 16:12:33 2021

@author: charlottolofsson
"""


# =============================================================================
# Modules
# =============================================================================

# Data handling and plotting
import pandas as pd
# Functions from other scripts
import sys
sys.path.append('../code/functions')
from plsr import plsr
from data_preparation import dataPrep

# =============================================================================
# Loading the data
# =============================================================================

# Target
target_name = 'a'
target = pd.read_csv('../data/blocks/targetAll.csv', index_col=0)

# Block A
dataA = pd.read_csv('../data/blocks/dataA.csv', index_col=0)
dataA_dummy = pd.get_dummies(dataA)
yA = target.loc[dataA_dummy.index]

# Block B
dataB = pd.read_csv('../data/blocks/dataB.csv', index_col=0)
dataB_dummy = pd.get_dummies(dataB)
yB = target.loc[dataB_dummy.index]

# Block C
dataC = pd.read_csv('../data/blocks/dataC.csv', index_col=0)
yC = target.loc[dataC.index]

# Block D
dataD = pd.read_csv('../data/blocks/dataD.csv', index_col=0)
yD = target.loc[dataD.index]

# Block E
dataE = pd.read_csv('../data/blocks/dataE.csv', index_col=0)
yE = target.loc[dataE.index]



# =============================================================================
# Standardising
# =============================================================================

blockAStd = dataPrep(dataA_dummy, dataA_dummy.columns, std=True)

blockBStd = dataPrep(dataB_dummy, dataB_dummy.columns, std=True)

blockCStd = dataPrep(dataC, dataC.columns, std=True)

blockDStd = dataPrep(dataD, dataD.columns, std=True)

blockEStd = dataPrep(dataE, dataE.columns, std=True)

# =============================================================================
# PLSR
# =============================================================================

# Block A
plsrA = plsr(blockAStd, yA, components=10, plot=True)

# Block B
plsrB = plsr(blockBStd, yB, components=10, plot=True)

# Block C
plsrC = plsr(blockCStd, yC, components=10, plot=True)

# Block D
plsrD = plsr(blockDStd, yD, components=10, plot=True)

# Block E
plsrE = plsr(blockEStd, yE, components=10, plot=True)


# =============================================================================
# Explained variance x
# =============================================================================

import hoggormplot as hopl
import hoggorm as ho


# Get the values from the data frame
X = blockCStd.values
y = yE.values

# Get the variable or columns names
X_features = list(blockEStd.columns)
y_features = list(yE.columns)

# Get the object or row names
X_objects = list(blockEStd.index)
y_objects = list(yE.index)

# Initialising model
model = ho.nipalsPLS1(arrX=X, Xstand=False, 
                  vecy=y,
                  cvType=["loo"], 
                  numComp=10)

hopl.plot(model, comp=[1, 2], 
          plots=['explainedVariance'], 
          which = ['X'],
          objNames=X_objects, 
          XvarNames=X_features,
          YvarNames=y_features)



