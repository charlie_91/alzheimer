#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 23 16:15:58 2021

@author: charlottolofsson
"""

# =============================================================================
# Modules
# =============================================================================

# Data handling and plotting
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# Preprocessing and models
import hoggorm as ho

# Functions from other scripts
import sys
sys.path.append('../code/functions')
from pca import pca
from data_preparation import dataPrep


# =============================================================================
# Loading the data
# =============================================================================

# Different dimensions for the blocks
# -----------------------------------------------------------------------------
# Block A
dataA = pd.read_csv('../data/blocks/dataA.csv', index_col=0)
dataA_dummy = pd.get_dummies(dataA)
dataA_descr = dataA_dummy.describe()

# Block B
dataB = pd.read_csv('../data/blocks/dataB.csv', index_col=0)
dataB_dummy = pd.get_dummies(dataB)
dataB_descr = dataB_dummy.describe()

# Block C
dataC = pd.read_csv('../data/blocks/dataC.csv', index_col=0)
dataC_descr = dataC.describe()

# Block D
dataD = pd.read_csv('../data/blocks/dataD.csv', index_col=0)
dataD_descr = dataD.describe()

# Block E
dataE = pd.read_csv('../data/blocks/dataE.csv', index_col=0)
dataE_descr = dataE.describe()


# Target
target_name = 'a'
target = pd.read_csv('../data/blocks/targetFull.csv', index_col=0)



# Data with subjects with no missing values across all the blocks
# -----------------------------------------------------------------------------
# Block A
DataA = pd.read_csv('../data/blocks/DataA_.csv', index_col=0)
DataA_dummy = pd.get_dummies(DataA)
DataA_descr = dataA_dummy.describe()

# Block B
DataB = pd.read_csv('../data/blocks/DataB_.csv', index_col=0)
DataB_dummy = pd.get_dummies(DataB)
DataB_descr = DataB_dummy.describe()

# Block C
DataC = pd.read_csv('../data/blocks/DataC_.csv', index_col=0)
DataC_descr = DataC.describe()

# Block D
DataD = pd.read_csv('../data/blocks/DataD_.csv', index_col=0)
DataD_descr = DataD.describe()

# Block E
DataE = pd.read_csv('../data/blocks/DataE_.csv', index_col=0)
DataE_descr = DataE.describe()




# =============================================================================
# Concatenate blocks into one large block
# =============================================================================

dataAll = pd.concat([dataA_dummy, dataB_dummy, dataC, dataD, dataE], axis=1)

DataAll = pd.concat([DataA_dummy, DataB_dummy, DataC, DataD, DataE], axis=1)



# =============================================================================
# Standardising
# =============================================================================

# Different dimensions for the blocks
# -----------------------------------------------------------------------------
blockAStd = dataPrep(dataA_dummy, dataA_dummy.columns, std=True)

blockBStd = dataPrep(dataB_dummy, dataB_dummy.columns, std=True)

blockCStd = dataPrep(dataC, dataC.columns, std=True)

blockDStd = dataPrep(dataD, dataD.columns, std=True)

blockEStd = dataPrep(dataE, dataE.columns, std=True)



# Data with subjects with no missing values across all the blocks
# -----------------------------------------------------------------------------
BlockAStd = dataPrep(DataA_dummy, DataA_dummy.columns, std=True)

BlockBStd = dataPrep(DataB_dummy, DataB_dummy.columns, std=True)

BlockCStd = dataPrep(DataC, DataC.columns, std=True)

BlockDStd = dataPrep(DataD, DataD.columns, std=True)

BlockEStd = dataPrep(DataE, DataE.columns, std=True)


# =============================================================================
# Matrix correlation coefficient
# =============================================================================


rv2 = ho.mat_corr_coeff.RV2coeff([BlockAStd, BlockBStd, BlockCStd,
                                  BlockDStd, BlockEStd])

rv2_rounded = np.round(rv2, 3)

# Creating heatmap
fig = plt.figure(figsize=(11,7))
ax = fig.add_axes([0.1, 0.1, 0.8, 0.8]) 
sns.heatmap(rv2_rounded, annot=True, cbar=False, annot_kws={"size": 16})
# plt.title('Matrix Correlation Coefficient', size=18)
ax.set_xticklabels(['A','B', 'C', 'D','E'], size=18)
ax.set_yticklabels(['A', 'B', 'C', 'D', 'E'], size=18)
plt.savefig('matrix_correlation_coeff_heatmap.png')
plt.show()


# =============================================================================
# PCA
# =============================================================================

# Block A
pcaA = pca(blockAStd, components=3, plot=True)

# Block B
blockBstd = blockBStd.drop(columns=['gender_male'])
pcaB = pca(blockBstd, components=6, plot=True)

# Block C
pcaC = pca(blockCStd, components=6, plot=True)

# Block D
pcaD = pca(blockDStd, components=6, plot=True)

# Block E
pcaE = pca(blockEStd, components=6, plot=True)



