#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 11 22:26:39 2021

@author: charlottolofsson
"""


# =============================================================================
# Modules
# =============================================================================

# Data handling and plotting
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
import seaborn as sns

# Preprocessing and models
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import PowerTransformer
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.model_selection import cross_val_score
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

# Scoring
from sklearn.metrics import matthews_corrcoef
from sklearn.metrics import make_scorer

# Functions from other scripts
import sys
sys.path.append('../code/functions')
from rent_function import rent
from params_mcc import paramsMcc


# =============================================================================
# Loading the data
# =============================================================================

"""
Loading the data and creating new dataframes with the features selected
from RENT. 
"""


# Block A
dataA = pd.read_csv('../data/UniqueBlocks/dataA.csv', index_col=0)
dataA_dummy = pd.get_dummies(dataA)

# Block B
dataB = pd.read_csv('../data/UniqueBlocks/dataB.csv', index_col=0)
dataB_dummy = pd.get_dummies(dataB)
B_features = ['age', 'bl_apoe_max_E4/E4', 'bl_apoe_max_E3/E4', 'bl_apoe_max_E2/E4',
              'smok', 'degree1_dem', 'cardiovascular_disease', 'bl_apoe_max_E2/E3', 
              'edu_level', 'bp_recum_sys']
DataB = dataB_dummy.loc[:, B_features]

# Block C
dataC = pd.read_csv('../data/UniqueBlocks/dataC.csv', index_col=0)
C_features = ['mmse_total', 'cerad_recall', 'cowat_tscore', 'tmtb_sec']
DataC = dataC.loc[:, C_features]

# Block D
dataD = pd.read_csv('../data/UniqueBlocks/dataD.csv', index_col=0)
D_features = ['PSMD', 'WMHo_rV', 'LesO', 'LesT']
# LesT vs LesF
# D_features = ['PSMD', 'WMHo_rV', 'LesO', 'LesF', 'LesT', 'LesP']
DataD = dataD.loc[:, D_features]

# Block E
dataE = pd.read_csv('../data/UniqueBlocks/dataE.csv', index_col=0)
E_features = ['ERC.combat_ATN', 'PHC.combat_ATN', 
              'MISC.combat_ATN', 'Anterior_hippocampus.combat_ATN']
# 'Posterior_hippocampus.combat_ATN'
# 'Br36.combat_ATN', 'Br35.combat_ATN',
DataE = dataE.loc[:, E_features]


# Target
target_name = 'a'
target = pd.read_csv('../data/UniqueBlocks/targetFull.csv', index_col=0)

targetAll = pd.read_csv('../data/UniqueBlocks/targetAll.csv', index_col=0)


# Checking distribution of classes
sum(targetAll.loc[:, 'a'] == 1)    
# 327
sum(targetAll.loc[:, 'a'] == 0)   
# 698




# Separating train and test data
# -----------------------------------------------------------------------------

test_index = pd.read_csv('../data/UniqueBlocks/test_subjects.csv')
test_index = test_index.iloc[:, 1]


# Training data

xA = dataA_dummy.drop(test_index)
yA = np.array(targetAll.loc[xA.index]).reshape(-1,)
yA_ = targetAll.loc[xA.index]


xB = DataB.drop(test_index)
yB = np.array(targetAll.loc[xB.index]).reshape(-1,)
yB_ = targetAll.loc[xB.index]


xC = DataC.drop(test_index)
yC = np.array(targetAll.loc[xC.index]).reshape(-1,)
yC_ = targetAll.loc[xC.index]


xD = DataD.drop(test_index)
yD = np.array(targetAll.loc[xD.index]).reshape(-1,)
yD_ = targetAll.loc[xD.index]


xE = DataE.drop(test_index)
yE = np.array(targetAll.loc[xE.index]).reshape(-1,)
yE_ = targetAll.loc[xE.index]


       

# Test data 
# y is the same for all blocks.. 

xA_test = dataA_dummy.loc[test_index]
yA_test = np.array(targetAll.loc[xA_test.index]).reshape(-1,)

xB_test = DataB.loc[test_index]
yB_test = np.array(targetAll.loc[xB_test.index]).reshape(-1,)

xC_test = DataC.loc[test_index]
yC_test = np.array(targetAll.loc[xC_test.index]).reshape(-1,)

xD_test = DataD.loc[test_index]
yD_test = np.array(targetAll.loc[xD_test.index]).reshape(-1,)

xE_test = DataE.loc[test_index]
yE_test = np.array(targetAll.loc[xE_test.index]).reshape(-1,)




# =============================================================================
# Standardising and power transformation
# =============================================================================

# Yeo Johnson transformation 
pt = PowerTransformer(method='yeo-johnson')
# Standard scaler
sc = StandardScaler()




scA = sc.fit(xA)
xAsc = pd.DataFrame(scA.transform(xA), columns=xA.columns, index=xA.index)
xAsc_test = scA.transform(xA_test)


scB = sc.fit(xB)
xBsc = pd.DataFrame(scB.transform(xB), columns=xB.columns, index=xB.index)
xBsc_test = scB.transform(xB_test)


scC = sc.fit(xC)
xCsc = pd.DataFrame(scC.transform(xC), columns=xC.columns, index=xC.index)
xCsc_test = scC.transform(xC_test)


ptD = pt.fit(xD)
xDpt = pd.DataFrame(ptD.transform(xD), columns=xD.columns, index=xD.index)
xDpt_test = ptD.transform(xD_test)


scE = sc.fit(xE)
xEsc = pd.DataFrame(scE.transform(xE), columns=xE.columns, index=xE.index)
xEsc_test = scE.transform(xE_test)




# =============================================================================
# Making lists for looping
# =============================================================================

# Lists of the data and names
Xtrain = [xAsc, xBsc, xCsc, xDpt, xEsc]

Ytrain = [yA, yB, yC, yD, yE]

block_names = ['BlockA', 'BlockB', 'BlockC', 
               'BlockD', 'BlockE']


Xtest = [xAsc_test, xBsc_test, xCsc_test, xDpt_test, xEsc_test]

Ytest = [yA_test, yB_test, yC_test, yD_test, yE_test]




# =============================================================================
# Define function for model evaluation
# =============================================================================

# evaluate a model with a given number of repeats
def evaluate_model(model, X, y, repeats, splits, metric):
 	
    # prepare the cross-validation procedure
 	cv = RepeatedStratifiedKFold(n_splits=splits, 
                                 n_repeats=repeats, 
                                 random_state=1)
 	
    # evaluate model
 	scores = cross_val_score(model, X, y, 
                             scoring=metric, 
                             cv=cv, 
                             n_jobs=-1,
                             verbose=0)
 	return scores



# =============================================================================
# Evaluating classifiers
# =============================================================================

# Creating scorer for Matthews correlation coefficient
mcc_scorer = make_scorer(matthews_corrcoef)
metric = mcc_scorer
scaler = StandardScaler()
power = PowerTransformer(method='yeo-johnson')



# Creating models
lg_model = LogisticRegression()

aggressive_model = PassiveAggressiveClassifier()

forest_model = RandomForestClassifier()

knn_model = KNeighborsClassifier()

svc_model = SVC()


# List of models and names
models = [lg_model, aggressive_model, forest_model, knn_model, svc_model]
modelnames = ['LogisticRegression', 'PassiveAggressiveClassifier', 
              'RandomForest', 'KNN', 'SVC']



# Defining parameters for grid search 
# -----------------------------------------------------------------------------

# Logistic Regression
param_range_lr  = [0.0001, 0.001, 0.01, 0.1, 1.0, 10.0, 100.0, 1000.0]
param_grid_lr   = [{'m__C' : param_range_lr, 
                    'm__solver' : ['lbfgs', 'liblinear']}]


# Passive Aggressive Classifier
param_range_pag   = [0.0001, 0.001, 0.01, 0.1, 1.0, 10.0, 100.0, 1000.0]
param_grid_pag   = [{'m__C' : param_range_pag,
                     'm__loss': ['hinge','squared_hinge']}]


# Random Forest
param_range_forest1   = [20, 40, 60, 80, 100, 120, 140, 160]
param_range_forest2   = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, None]
param_grid_forest  = [{'m__n_estimators' : param_range_forest1, 
                       'm__criterion' : ['gini','entropy'], 
                       'm__max_depth': param_range_forest2,
                       'm__max_features': ['auto','sqrt', 'log']}]



# KNN
param_range_knc = [2, 4, 5, 6, 7, 8, 9, 10]
param_grid_knc = [{'m__n_neighbors' : param_range_knc,
                   'm__weights' : ['uniform', 'distance'], 
                   'm__algorithm' : ['auto', 'ball_tree', 'kd_tree', 'brute'],
                   'm__p' : [1, 2]}]



# SVC
param_range_svc1 = [0.0001, 0.001, 0.01, 0.1, 1.0, 10.0, 100.0] 
param_range_svc2 = [0.0001, 0.0005, 0.001, 0.01, 0.05, 0.1, 1.0]         
param_grid_svc  = [{'m__C': param_range_svc1, 'm__kernel': ['linear']},
                   {'m__C': param_range_svc1, 'm__gamma': param_range_svc2, 
                    'm__kernel': ['rbf']}]



# List of the parameters for all the models
params = [param_grid_lr, param_grid_pag, param_grid_forest, 
          param_grid_knc, param_grid_svc]


# =============================================================================
# Grid search
# =============================================================================


# Dictionary for storing results for all the blocks
grid_search_scores = {}


for x, y, name in zip(Xtrain, Ytrain, block_names):
    
    # Dictionary for storing results for all the models
    scores = {}
    
    for model, modelname, param in zip(models, modelnames, params):
        
        # Creating pipeline
        pipeline = Pipeline(steps=[('m', model)])
        
        # Grid search
        gs = GridSearchCV(estimator=pipeline, 
                          param_grid=param, 
                          scoring=metric, 
                          cv=5,
                          n_jobs=-1)
        
        gs = gs.fit(x, y)
        best = gs.best_estimator_
        
        # Storing best scores
        scores[modelname] =  [gs.best_score_, gs.best_params_]
        
    grid_search_scores[name] = scores
    
    
# Creating dataframe and exporting as csv file    
grid_search_scores_df = pd.DataFrame(grid_search_scores)
grid_search_scores_df.to_csv('grid_search_scores.csv')


# =============================================================================
# Repeated stratified k-fold with best parameters
# =============================================================================


# Dictionary for storing results for all the blocks
k_fold_scores = {}

for x, y, name in zip(Xtrain, Ytrain, block_names):
    
    # Dictionary for storing results for all the models
    scores = {}
    
    for model, modelname in zip(models, modelnames):
        
        # Creating pipeline
        pipeline = Pipeline(steps=[('m', model)])
        
        # Fitting pipeline
        pipeline.fit(x, y)
        
        # Fitting model with best parameters
        best_parameters = grid_search_scores[name][modelname][1]
        pipeline.set_params(**best_parameters)
        
        # Repeated stratified k-fold
        rkf_scores = evaluate_model(pipeline,
                                    X=x, 
                                    y=y, 
                                    repeats=10,
                                    splits=5,
                                    metric=metric) 
        
        # Getting mean scores and storing in dictionary
        rkf_scores_mean = round(np.mean(rkf_scores),4)
        rkf_scores_std = round(np.std(rkf_scores, ddof=1),4)
        scores[modelname] = {'Mean score ' : rkf_scores_mean, 
                             'Std score' : rkf_scores_std}
        
    
    k_fold_scores[name] = scores
    

# Creating dataframe and exporting as csv file  
k_fold_scores_df = pd.DataFrame(k_fold_scores)
k_fold_scores_df.to_csv('k_fold_scores.csv')



# =============================================================================
# Predicting test labels
# =============================================================================


# Dictionary for storing results for all the blocks
test_scores = {}

for x, y, x_test, y_test, name in zip(Xtrain, Ytrain, Xtest, 
                                      Ytest, block_names):
    
    # Dictionary for storing results for all the models
    scores = {}
    
    for model, modelname in zip(models, modelnames):
        
        # Creating pipeline
        pipeline = Pipeline(steps=[('m', model)])
        
        # Fitting pipeline
        pipeline.fit(x, y)
        
        # Fitting model with best parameters
        best_parameters = grid_search_scores[name][modelname][1]
        pipeline.set_params(**best_parameters)
        
        # Predicting test labels and scoring
        pred = pipeline.predict(x_test)
        mcc_score = matthews_corrcoef(y_test, pred)
        
        scores[modelname] = mcc_score
        

    test_scores[name] = scores


# Creating dataframe and exporting as csv file     
test_scores_df = pd.DataFrame(test_scores)
test_scores_df.to_csv('test_scores.csv')


# =============================================================================
# Final models and predictions
# =============================================================================

"""
modelA = PassiveAggressiveClassifier(C=10, loss='hinge') 
modelB = PassiveAggressiveClassifier(C=0.01, loss='hinge') 
modelC = LogisticRegression(C=0.001, solver='liblinear')
modelD = LogisticRegression(C=0.01, solver='liblinear')
modelE = LogisticRegression(C=0.0001, solver='liblinear')
"""

modelA = KNeighborsClassifier(algorithm='brute', n_neighbors=5, 
                              p=1, weights='uniform')
modelB = LogisticRegression(C=1.0, solver='lbfgs')
modelC = LogisticRegression(C=0.1, solver='lbfgs')
modelD = KNeighborsClassifier(algorithm='auto', n_neighbors=7, 
                              p=2, weights='uniform')
modelE = LogisticRegression(C=0.0001, solver='liblinear')


final_models =[modelA, modelB, modelC, modelD, modelE]


# Dictionary for storing results for all the blocks
test_scores = {}
predictions = {}

for x, y, x_test, y_test, name, model in zip(Xtrain, Ytrain, Xtest, 
                                             Ytest, block_names,
                                             final_models):
    
    
    # Fitting model
    model.fit(x,y)
    
    # Predicting test labels
    pred = model.predict(x_test)    
    predictions[name] = pred
    


# Creating dataframe and exporting as csv file  
predictions_df = pd.DataFrame(predictions)
predictions_df.to_csv('predictions.csv')   

predictions_df['actual'] =  y_test



# Predictions
predictions_df = pd.read_csv('../data/predictions/predictions.csv', index_col=0)
y_test = yB_test.copy()
predictions_df['actual'] =  y_test

predictions_sort = predictions_df.sort_values(by=['actual'], ascending=False)

# =============================================================================
# Heatmap of predictions
# =============================================================================

fig = plt.figure(figsize=(30,22))
ax = fig.add_axes([0.1, 0.1, 0.8, 0.8]) 

# Define colors
colors = ['deepskyblue', 'midnightblue']
cmap = LinearSegmentedColormap.from_list('Custom', colors, len(colors))

sns.heatmap(predictions_sort, cmap=cmap, annot_kws={"size": 16})
ax.set_xticklabels(['Block A','Block B', 'Block C', 
                    'Block D','Block E', 'Actual'], size=30)

# Set the colorbar labels
colorbar = ax.collections[0].colorbar
colorbar.set_ticks([0.25,0.75])
colorbar.set_ticklabels(['0', '1'])
colorbar.ax.tick_params(labelsize=40) 
plt.savefig('predictions_heatmap_sorted.png')
plt.show()

