#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 14 15:49:46 2021

@author: charlottolofsson
"""


# =============================================================================
# Modules
# =============================================================================

# Data handling and plotting
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

# Preprocessing and models
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.model_selection import cross_val_score
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PowerTransformer
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import PassiveAggressiveClassifier 
from sklearn.ensemble import RandomForestClassifier 
from sklearn.neighbors import KNeighborsClassifier 
from sklearn.svm import SVC
from RENT import RENT, stability

# Scoring
from sklearn.metrics import matthews_corrcoef
from sklearn.metrics import make_scorer

import warnings
warnings.filterwarnings("ignore")



# =============================================================================
# Loading the data
# =============================================================================

# Block A
dataA = pd.read_csv('../data/blocks/DataA_.csv', index_col=0)
dataA_dummy = pd.get_dummies(dataA)
dataA_descr = dataA_dummy.describe()


# Block B
dataB = pd.read_csv('../data/blocks/DataB_.csv', index_col=0)
dataB_dummy = pd.get_dummies(dataB)
dataB_descr = dataB.describe()


# Block C
dataC = pd.read_csv('../data/blocks/DataC_.csv', index_col=0)
dataC_descr = dataC.describe()


# Block D
dataD = pd.read_csv('../data/blocks/DataD_.csv', index_col=0)
dataD_descr = dataD.describe()


# Block E
dataE = pd.read_csv('../data/blocks/DataE_.csv', index_col=0)
dataE_descr = dataE.describe()


# Target
target_name = 'a'
target = pd.read_csv('../data/blocks/targetFull.csv', index_col=0)


# =============================================================================
# Concatenating block into one block
# =============================================================================

dataAll = pd.concat([dataA_dummy, dataB_dummy, dataC, dataD, dataE], 
                     axis=1)

# Final data
X = dataAll.copy()



# =============================================================================
# Standardising data separately
# =============================================================================

"""
Standardising separately in order to only transform data in block D.
"""

sc = StandardScaler()
minmax = MinMaxScaler(feature_range=(1,2))


dataAstd = sc.fit_transform(dataA_dummy)
dataAsc = pd.DataFrame(dataAstd, columns=dataA_dummy.columns, 
                       index=dataA_dummy.index)


dataBstd = sc.fit_transform(dataB_dummy)
dataBsc = pd.DataFrame(dataBstd, columns=dataB_dummy.columns, 
                       index=dataB_dummy.index)


dataCstd = sc.fit_transform(dataC)
dataCsc = pd.DataFrame(dataCstd, columns=dataC.columns, 
                       index=dataC.index)


dataDstd = sc.fit_transform(dataD)
dataDsc = pd.DataFrame(dataDstd, columns=dataD.columns, 
                       index=dataD.index)

dataDminmax = minmax.fit_transform(dataD)
dataDmm = pd.DataFrame(dataDminmax, columns=dataD.columns, 
                       index=dataD.index)


dataEstd = sc.fit_transform(dataE)
dataEsc = pd.DataFrame(dataEstd, columns=dataE.columns, 
                       index=dataE.index)



# =============================================================================
# Power transformation on block D
# =============================================================================

# Yeo Johnson transformation 
pt_yeo = PowerTransformer(method='yeo-johnson')
dataD_yeo = pt_yeo.fit_transform(dataDstd)
dataDYeo = pd.DataFrame(dataD_yeo, columns=dataD.columns, index=dataD.index)


# BoxCox transformation
pt_box = PowerTransformer(method='box-cox')
dataD_box = pt_box.fit_transform(dataDmm)
dataDBox = pd.DataFrame(dataD_box, columns=dataD.columns, index=dataD.index)


# =============================================================================
# Concatenate blocks into one block
# =============================================================================

# Standardised data
dataSc = pd.concat([dataAsc, dataBsc, dataCsc, dataDsc, dataEsc], 
                    axis=1)

XSc = dataSc.copy()


# Yeo Johnson transformation of data in block D
dataYeo = pd.concat([dataAsc, dataBsc, dataCsc, dataDYeo, dataEsc], 
                     axis=1)

XYeo = dataYeo.copy()


# BoxCox transformation of block D
dataBox = pd.concat([dataAsc, dataBsc, dataCsc, dataDBox, dataEsc], 
                     axis=1)

XBox = dataBox.copy()


# =============================================================================
# Target
# =============================================================================

y = np.squeeze(target.values.reshape(1, -1))
# Checking distribution of classes
sum(target.loc[:, 'a'] == 1)    
sum(target.loc[:, 'a'] == 0)    


# Checking shapes
XSc.shape   
XYeo.shape
XBox.shape                   
y.shape                



# =============================================================================
# RENT
# =============================================================================

"""
https://github.com/NMBU-Data-Science/RENT/blob/master/examples/Classification_example.ipynb
"""

# Define a range of regularisation parameters C for elastic net. 
my_C_params = [0.01, 0.1, 1, 10]

# Define a reange of l1-ratios for elastic net.  
my_l1_ratios = [0, 0.1, 0.25, 0.5, 0.75, 0.9, 1]

# Define setting for RENT
modelRent = RENT.RENT_Classification(data=XBox, 
                                     target=y, 
                                     feat_names=XBox.columns, 
                                     C=my_C_params, 
                                     l1_ratios=my_l1_ratios,
                                     autoEnetParSel=False,
                                     poly='OFF',
                                     testsize_range=(0.25,0.25),
                                     scoring='mcc',
                                     classifier='logreg',
                                     K=500,
                                     random_state=0,
                                     verbose=1)




# Training the model
# -----------------------------------------------------------------------------
modelRent.train()
# Getting best parameters
modelRent.get_enet_params()    # (0.1, 0.75)
# Runtime
modelRent.get_runtime()

# modelRent.set_enet_params(C=0.1, l1_ratio=0.75)
#modelRent.get_enet_params()

# Score  matrices
cv_score, cv_zeros, cv_harmonic_mean = modelRent.get_enetParam_matrices()
# cv_score, cv_zeros, cv_harmonic_mean = modelRent.get_cv_matrices()

# Visualising
# CV score
# -------------------------------------------------------------------------
scores_array = np.array(cv_score, dtype=float)
scores_arr = np.round(scores_array, 4)


fig, ax = plt.subplots(figsize=(30,10))
im = ax.imshow(scores_arr)

# Setting ticks and ticklabels
ax.set_xticks(np.arange(len(cv_score.columns)))
ax.set_yticks(np.arange(len(cv_score.index)))
ax.set_xticklabels(cv_score.columns)
ax.set_yticklabels(cv_score.index)
plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
         rotation_mode="anchor")

# Loop over data dimensions and create text annotations.
for i in range(len(cv_score.index)):
    for j in range(len(cv_score.columns)):
        text = ax.text(j, i, scores_arr[i, j],
                       ha="center", va="center", color="w")

ax.set_title('scores')
plt.savefig('scores.png')
plt.show()


# CV zeros
# -------------------------------------------------------------------------
zeroes_array = np.array(cv_zeros, dtype=float)
zeroes_arr = np.round(zeroes_array, 4)


fig, ax = plt.subplots(figsize=(30,10))
im = ax.imshow(zeroes_arr)

# Setting ticks and ticklabels
ax.set_xticks(np.arange(len(cv_zeros.columns)))
ax.set_yticks(np.arange(len(cv_zeros.index)))
ax.set_xticklabels(cv_zeros.columns)
ax.set_yticklabels(cv_zeros.index)
plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
         rotation_mode="anchor")

# Loop over data dimensions and create text annotations.
for i in range(len(cv_zeros.index)):
    for j in range(len(cv_zeros.columns)):
        text = ax.text(j, i, zeroes_arr[i, j],
                       ha="center", va="center", color="w")

ax.set_title('zeroes')
plt.savefig('zeroes.png')
plt.show()



# CV harmonic mean
# -------------------------------------------------------------------------
harmonic_mean_array = np.array(cv_harmonic_mean, dtype=float)
harmonic_mean_arr = np.round(harmonic_mean_array, 4)


fig, ax = plt.subplots(figsize=(30,10))
im = ax.imshow(harmonic_mean_arr)

# Setting ticks and ticklabels
ax.set_xticks(np.arange(len(cv_harmonic_mean.columns)))
ax.set_yticks(np.arange(len(cv_harmonic_mean.index)))
ax.set_xticklabels(cv_harmonic_mean.columns)
ax.set_yticklabels(cv_harmonic_mean.index)
plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
         rotation_mode="anchor")

# Loop over data dimensions and create text annotations.
for i in range(len(cv_harmonic_mean.index)):
    for j in range(len(cv_harmonic_mean.columns)):
        text = ax.text(j, i, harmonic_mean_arr[i, j],
                       ha="center", va="center", color="w")

ax.set_title('harmonic_mean')
plt.savefig('harmonic_mean.png')
plt.show()




# Plots
# -----------------------------------------------------------------------------
selected_features = modelRent.select_features(tau_1_cutoff=0.8, 
                                              tau_2_cutoff=0.8, 
                                              tau_3_cutoff=0.975)
summary = modelRent.get_summary_criteria()


modelRent.plot_selection_frequency()

# Weight distribution
weight_distribution = modelRent.get_weight_distributions(binary=False)

# Plot
ax = sns.distplot(weight_distribution.values[:, 27], kde=False)
ax = sns.distplot(weight_distribution.values[:, 15], kde=False)
modelRent.plot_elementary_models()



# Setting value for C and l1-ratio manually
# -----------------------------------------------------------------------------
"""
Standardised data
modelRent.set_enet_params(C=0.1, l1_ratio=0.25)
modelRent.get_enet_params()
"""

# Yeo
modelRent.set_enet_params(C=0.1, l1_ratio=0.5)
modelRent.get_enet_params()


# =============================================================================
# Define function for model evaluation
# =============================================================================

# evaluate a model with a given number of repeats
def evaluate_model(model, X, y, repeats, splits, metric):
 	
    # prepare the cross-validation procedure
 	cv = RepeatedStratifiedKFold(n_splits=splits, 
                                 n_repeats=repeats, 
                                 random_state=1)
 	
    # evaluate model
 	scores = cross_val_score(model, X, y, 
                             scoring=metric, 
                             cv=cv, 
                             n_jobs=-1,
                             verbose=0)
 	return scores



# =============================================================================
# Run repeated K-fold
# =============================================================================


# Standardised data 
# -----------------------------------------------------------------------------
mcc_scorer = make_scorer(matthews_corrcoef)
metric = mcc_scorer
scaler = StandardScaler()

# Creating models
lg_model = LogisticRegression(C=0.01, solver='liblinear')

aggressive_model = PassiveAggressiveClassifier(C=0.0001, 
                                               loss='hinge')

forest_model = RandomForestClassifier(criterion='gini',
                                      max_features='auto',
                                      max_depth=10,
                                      n_estimators=40, 
                                      random_state=1,
                                      n_jobs=-1)

knn_model = KNeighborsClassifier(n_neighbors=10,
                                 algorithm='auto',
                                 p=2, weights='distance')

svc_model = SVC(kernel='rbf', C=10.0, gamma=0.01, random_state=1)



# Creating lists with model and modelnames
models = [lg_model, aggressive_model, forest_model, knn_model, svc_model]
modelnames = ['LogisticRegression', 'PassiveAggressiveClassifier', 
              'RandomForest', 'KNN', 'SVC']



# Checking performance for different cutoffs for tau1 and tau2 parameter
# of RENT. 
tau = np.arange(0.2, 0.96, 0.05)
selected_features_ = []
tau_scores_std = {}

for i, value in enumerate(tau):

    scores_ = []
    
    # Selecting features
    selected_features = modelRent.select_features(tau_1_cutoff=value, 
                                                  tau_2_cutoff=value, 
                                                  tau_3_cutoff=0.975)
    features = selected_features
    feature_names = XSc.columns[features]
    selected_features_.append(feature_names)
    train_data_ = XSc.iloc[:, features]
    
    
    scores_std = {}
    
    for model, modelname in zip(models, modelnames):
        
        # Creating pipeline
        pipeline = Pipeline(steps=[('m', model)])
        
        # Evaluating model
        rkf_scores = evaluate_model(pipeline,
                                    X=train_data_, 
                                    y=y, 
                                    repeats=10,
                                    splits=5,
                                    metric=metric) 
        
        # Getting mean scores and storing
        rkf_scores_mean = round(np.mean(rkf_scores),4)
        rkf_scores_std = round(np.std(rkf_scores, ddof=1),4)
        scores_std[modelname] = {'Mean score' : rkf_scores_mean, 
                                 'Std score' : rkf_scores_std}

        tau_scores_std[i] = scores_std


# Creating dataframe and exporting files
selected_features_df = pd.DataFrame(selected_features_)
selected_features_dfT = selected_features_df.T
selected_features_dfT.columns = tau 
selected_features_dfT.to_csv('selected_features_std.csv')
    
tau_scores_std_df = pd.DataFrame(tau_scores_std)
tau_scores_std_df.to_csv('tau_scores_std.csv')


# Visualisation
# -----------------------------------------------------------------------------
# Getting the values to plot
x = tau
lr = [tau_scores_std_df.loc['LogisticRegression'][x]['Mean score'] 
      for x in range(len(tau))]
pag = [tau_scores_std_df.loc['PassiveAggressiveClassifier'][x]['Mean score'] 
       for x in range(len(tau))]
rf = [tau_scores_std_df.loc['RandomForest'][x]['Mean score'] 
      for x in range(len(tau))]
knn = [tau_scores_std_df.loc['KNN'][x]['Mean score'] 
       for x in range(len(tau))]
svc = [tau_scores_std_df.loc['SVC'][x]['Mean score'] 
       for x in range(len(tau))]


# Creating figure
fig,ax = plt.subplots(1, figsize=(15, 10))
plt.plot(x, lr, label = 'Logistic Regression', linewidth=3.0)  
plt.plot(x, pag, label = 'Passive Aggressive Classifier', linewidth=3.0)  
plt.plot(x, rf, label = 'Random Forest', linewidth=3.0)  
plt.plot(x, knn, label = 'KNN', linewidth=3.0)  
plt.plot(x, svc, label = 'SVC', linewidth=3.0)   

# Setting ticks and labels
ax.yaxis.set_tick_params(labelsize=20)
ax.xaxis.set_tick_params(labelsize=20, rotation=15)
ax.set_ylabel('Score', fontsize=20)
ax.set_xlabel('Value for tau', fontsize=20)
ax.legend(fontsize=15, loc='lower left')
ax.set_title('Scores with different values for the parameter Tau', fontsize=22)
plt.grid()
plt.show()
fig.savefig('tau_scores_std_plot.pdf', bbox_inches='tight') 
    


# Standardised data with Yeo-Johnson transformed data in block D.
# -----------------------------------------------------------------------------
mcc_scorer = make_scorer(matthews_corrcoef)
metric = mcc_scorer
# scaler = StandardScaler()


# Creating models
lg_model = LogisticRegression(C=0.01, solver='liblinear')

aggressive_model = PassiveAggressiveClassifier(C=0.0001, 
                                               loss='hinge')

forest_model = RandomForestClassifier(criterion='gini',
                                      max_depth=70,
                                      max_features='auto',
                                      n_estimators=100, 
                                      random_state=1,
                                      n_jobs=-1)

knn_model = KNeighborsClassifier(n_neighbors=5,
                                 algorithm='auto',
                                 p=1, weights='uniform')

svc_model = SVC(kernel='rbf', C=10.0, gamma=0.01, random_state=1)


# Creating lists with model and modelnames
models = [lg_model, aggressive_model, forest_model, knn_model, svc_model]
modelnames = ['LogisticRegression', 'PassiveAggressiveClassifier', 
             'RandomForest', 'KNN', 'SVC']



# Getting scores
# Checking performance for different cutoffs for tau1 and tau2 parameter
# of RENT. 
tau = np.arange(0.2, 0.96, 0.05)
selected_features_ = []
tau_scores_yeo = {}

for i, value in enumerate(tau):

    scores_ = []
    
    # Selecting features
    selected_features = modelRent.select_features(tau_1_cutoff=value, 
                                                  tau_2_cutoff=value, 
                                                  tau_3_cutoff=0.975)
    features = selected_features
    feature_names = XYeo.columns[features]
    selected_features_.append(feature_names)
    train_data_ = XYeo.iloc[:, features]

    scores_yeo = {}
    
    for model, modelname in zip(models, modelnames):
        
        # Creating pipeline
        pipeline = Pipeline(steps=[('m', model)])
        
        # Evaluating model
        rkf_scores = evaluate_model(pipeline,
                                    X=train_data_, 
                                    y=y, 
                                    repeats=10,
                                    splits=5,
                                    metric=metric) 
        
        # Getting mean scores and storing
        rkf_scores_mean = round(np.mean(rkf_scores),4)
        rkf_scores_std = round(np.std(rkf_scores, ddof=1),4)
        scores_yeo[modelname] = {'Mean score' : rkf_scores_mean, 
                                 'Std score' : rkf_scores_std}
        
        tau_scores_yeo[i] = scores_yeo


# Creating dataframe and exporting files
selected_features_df = pd.DataFrame(selected_features_)
selected_features_dfT = selected_features_df.T
selected_features_dfT.columns = tau 
selected_features_dfT.to_csv('selected_features_yeo.csv')


# Creating dataframe and exporting file     
tau_scores_yeo_df = pd.DataFrame(tau_scores_yeo)
tau_scores_yeo_df.to_csv('tau_scores_yeo.csv')


# Visualisation
# -----------------------------------------------------------------------------

# Getting the values to plot
x = tau
lr = [tau_scores_yeo_df.loc['LogisticRegression'][x]['Mean score'] 
      for x in range(len(tau))]
pag = [tau_scores_yeo_df.loc['PassiveAggressiveClassifier'][x]['Mean score'] 
       for x in range(len(tau))]
rf = [tau_scores_yeo_df.loc['RandomForest'][x]['Mean score'] 
      for x in range(len(tau))]
knn = [tau_scores_yeo_df.loc['KNN'][x]['Mean score'] 
       for x in range(len(tau))]
svc = [tau_scores_yeo_df.loc['SVC'][x]['Mean score'] 
       for x in range(len(tau))]


# Creating figure
fig,ax = plt.subplots(1, figsize=(15, 10))
plt.plot(x, lr, label = 'Logistic Regression', linewidth=3.0)  
plt.plot(x, pag, label = 'Passive Aggressive Classifier', linewidth=3.0)  
plt.plot(x, rf, label = 'Random Forest', linewidth=3.0)  
plt.plot(x, knn, label = 'KNN', linewidth=3.0)  
plt.plot(x, svc, label = 'SVC', linewidth=3.0)  


# Setting ticks and labels
ax.yaxis.set_tick_params(labelsize=20)
ax.xaxis.set_tick_params(labelsize=20, rotation=15)
ax.set_ylabel('Score', fontsize=20)
ax.set_xlabel('Value for tau', fontsize=20)
ax.legend(fontsize=15, loc='lower left')
ax.set_title('Score for different values for the parameter Tau', fontsize=22)
plt.grid()
plt.show()
fig.savefig('tau_scores_yeo_plot.pdf', bbox_inches='tight') 



# MinMax scaled data and BoxCox transformed data in block D. 
# Standardised data in rest of the blocks.
# -----------------------------------------------------------------------------
mcc_scorer = make_scorer(matthews_corrcoef)
metric = mcc_scorer
# scaler = StandardScaler()

# Creating models
lg_model = LogisticRegression(C=0.01, solver='liblinear')

aggressive_model = PassiveAggressiveClassifier(C=0.0001, 
                                               loss='hinge')

forest_model = RandomForestClassifier(criterion='gini',
                                      max_depth=70,
                                      max_features='auto',
                                      n_estimators=140, 
                                      random_state=1,
                                      n_jobs=-1)

knn_model = KNeighborsClassifier(n_neighbors=2,
                                 algorithm='auto',
                                 p=1, weights='distance')

svc_model = SVC(kernel='rbf', 
                 C=10.0, 
                 gamma=0.01, 
                 random_state=1)


# Creating lists with model and modelnames
models = [lg_model, aggressive_model, forest_model, knn_model, svc_model]
modelname = ['LogisticRegression', 'PassiveAggressiveClassifier', 
             'RandomForest', 'KNN', 'SVC']


# Getting scores
# Checking performance for different cutoffs for tau1 and tau2 parameter
# of RENT. 
tau = np.arange(0.2, 0.96, 0.05)
selected_features_ = []
tau_scores_box = {}

for i, value in enumerate(tau):

    scores_ = []
    
    # Selecting features
    selected_features = modelRent.select_features(tau_1_cutoff=value, 
                                                  tau_2_cutoff=value, 
                                                  tau_3_cutoff=0.975)
    features = selected_features
    feature_names = XBox.columns[features]
    selected_features_.append(feature_names)
    train_data_ = XBox.iloc[:, features]
    
    scores_box = {}
    
    for model, modelname in zip(models, modelnames):
        
        # Creating pipeline
        pipeline = Pipeline(steps=[('m', model)])
        
        # Evaluating model
        rkf_scores = evaluate_model(pipeline,
                                    X=train_data_, 
                                    y=y, 
                                    repeats=10,
                                    splits=5,
                                    metric=metric) 
        
        # Getting mean scores and storing results
        rkf_scores_mean = round(np.mean(rkf_scores),4)
        rkf_scores_std = round(np.std(rkf_scores, ddof=1),4)
        scores_box[modelname] = {'Mean score' : rkf_scores_mean, 
                                 'Std score' : rkf_scores_std}
        
        tau_scores_box[i] = scores_box



# Creating dataframe and exporting file     
tau_scores_box_df = pd.DataFrame(tau_scores_box)
tau_scores_box_df.to_csv('tau_scores_box.csv')



# Visualisation
# -----------------------------------------------------------------------------

# Getting the values to plot
x = tau
lr = [tau_scores_box_df.loc['LogisticRegression'][x]['Mean score'] 
      for x in range(len(tau))]
pag = [tau_scores_box_df.loc['PassiveAggressiveClassifier'][x]['Mean score'] 
       for x in range(len(tau))]
rf = [tau_scores_box_df.loc['RandomForest'][x]['Mean score'] 
      for x in range(len(tau))]
knn = [tau_scores_box_df.loc['KNN'][x]['Mean score'] 
       for x in range(len(tau))]
svc = [tau_scores_box_df.loc['SVC'][x]['Mean score'] 
       for x in range(len(tau))]

# Creating figure
fig,ax = plt.subplots(1, figsize=(15, 10))
plt.plot(x, lr, label = 'Logistic Regression', linewidth=3.0)  
plt.plot(x, pag, label = 'Passive Aggressive Classifier', linewidth=3.0)  
plt.plot(x, rf, label = 'Random Forest', linewidth=3.0)  
plt.plot(x, knn, label = 'KNN', linewidth=3.0)  
plt.plot(x, svc, label = 'SVC', linewidth=3.0)  


# Setting ticks and labels
ax.yaxis.set_tick_params(labelsize=20)
ax.xaxis.set_tick_params(labelsize=20, rotation=15)
ax.set_ylabel('Score', fontsize=20)
ax.set_xlabel('Value for tau', fontsize=20)
ax.legend(fontsize=15, loc='lower left')
ax.set_title('Score for different values of the parameter Tau', fontsize=22)
plt.grid()
plt.show()
fig.savefig('tau_scores_box_plot.pdf', bbox_inches='tight') 



# =============================================================================
# Evaluating classifiers with selected features
# =============================================================================

# Standardised data 
# -----------------------------------------------------------------------------

mcc_scorer = make_scorer(matthews_corrcoef)
metric = mcc_scorer
scaler = StandardScaler()

# Creating models
lg_model = LogisticRegression(C=0.01, solver='liblinear')

aggressive_model = PassiveAggressiveClassifier(C=0.0001, 
                                               loss='hinge')

forest_model = RandomForestClassifier(criterion='gini',
                                      max_features='auto',
                                      max_depth=10,
                                      n_estimators=40, 
                                      random_state=1,
                                      n_jobs=-1)

knn_model = KNeighborsClassifier(n_neighbors=10,
                                 algorithm='auto',
                                 p=2, weights='distance')

svc_model = SVC(kernel='rbf', C=10.0, gamma=0.01, random_state=1)



# Creating lists with model and modelnames
models = [lg_model, aggressive_model, forest_model, knn_model, svc_model]
modelnames = ['LogisticRegression', 'PassiveAggressiveClassifier', 
              'RandomForest', 'KNN', 'SVC']



# Empty dictionary for storing results
scores_std = {}

# Selecting features
selected_features = modelRent.select_features(tau_1_cutoff=0.6, 
                                              tau_2_cutoff=0.6, 
                                              tau_3_cutoff=0.975)

# Creating new dataset with selected features
features = selected_features
feature_names = XYeo.columns[features]
# selected_features.append(feature_names)
train_data_ = XYeo.iloc[:, features]

    
# Evaluating the different models    
for model, modelname in zip(models, modelnames):
    
    # Creating pipeline
    pipeline = Pipeline(steps=[('m', model)])
    
    # Evaluating model
    rkf_scores = evaluate_model(pipeline,
                                X=train_data_, 
                                y=y, 
                                repeats=10,
                                splits=5,
                                metric=metric) 
    
    # Getting mean scores and storing
    rkf_scores_mean = round(np.mean(rkf_scores),4)
    rkf_scores_std = round(np.std(rkf_scores, ddof=1),4)
    scores_std[modelname] = {'Mean score' : rkf_scores_mean, 
                             'Std score' : rkf_scores_std}



# Creating dataframe and exporting file      
scores_std_df = pd.DataFrame(scores_std)
scores_std_df.to_csv('scores_std.csv')