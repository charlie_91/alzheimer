#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 14 15:49:46 2021

@author: charlottolofsson
"""


# =============================================================================
# Modules
# =============================================================================

# Data handling and plotting
import pandas as pd
import numpy as np

# Preprocessing and models
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.model_selection import cross_val_score
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import PowerTransformer
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

# Scoring
from sklearn.metrics import matthews_corrcoef
from sklearn.metrics import make_scorer



# =============================================================================
# Loading the data
# =============================================================================


# Block A
dataA = pd.read_csv('../data/blocks/DataA_.csv', index_col=0)
dataA_dummy = pd.get_dummies(dataA)
dataA_descr = dataA_dummy.describe()


# Block B
dataB = pd.read_csv('../data/blocks/DataB_.csv', index_col=0)
dataB_dummy = pd.get_dummies(dataB)
dataB_descr = dataB.describe()


# Block C
dataC = pd.read_csv('../data/blocks/DataC_.csv', index_col=0)
dataC_descr = dataC.describe()


# Block D
dataD = pd.read_csv('../data/blocks/DataD_.csv', index_col=0)
dataD_descr = dataD.describe()


# Block E
dataE = pd.read_csv('../data/blocks/DataE_.csv', index_col=0)
dataE_descr = dataE.describe()


# Target
target_name = 'a'
target = pd.read_csv('../data/blocks/targetFull.csv', index_col=0)



# =============================================================================
# Concatenating block into one block
# =============================================================================

dataAll = pd.concat([dataA_dummy, dataB_dummy, dataC, dataD, dataE], 
                     axis=1)

# Final data
X = dataAll.copy()


# =============================================================================
# Standardising data separately
# =============================================================================

"""
Standardising separately in order to only transform data in block D.
"""

sc = StandardScaler()
minmax = MinMaxScaler(feature_range=(1,2))


dataAstd = sc.fit_transform(dataA_dummy)
dataAsc = pd.DataFrame(dataAstd, columns=dataA_dummy.columns, 
                       index=dataA_dummy.index)


dataBstd = sc.fit_transform(dataB_dummy)
dataBsc = pd.DataFrame(dataBstd, columns=dataB_dummy.columns, 
                       index=dataB_dummy.index)


dataCstd = sc.fit_transform(dataC)
dataCsc = pd.DataFrame(dataCstd, columns=dataC.columns, 
                       index=dataC.index)


dataDstd = sc.fit_transform(dataD)
dataDsc = pd.DataFrame(dataDstd, columns=dataD.columns, 
                       index=dataD.index)

dataDminmax = minmax.fit_transform(dataD)
dataDmm = pd.DataFrame(dataDminmax, columns=dataD.columns, 
                       index=dataD.index)


dataEstd = sc.fit_transform(dataE)
dataEsc = pd.DataFrame(dataEstd, columns=dataE.columns, 
                       index=dataE.index)


# =============================================================================
# Power transformation on block D
# =============================================================================

# Yeo Johnson transformation 
pt_yeo = PowerTransformer(method='yeo-johnson')
dataD_yeo = pt_yeo.fit_transform(dataDstd)
dataDYeo = pd.DataFrame(dataD_yeo, columns=dataD.columns, index=dataD.index)


# BoxCox transformation
pt_box = PowerTransformer(method='box-cox')
dataD_box = pt_box.fit_transform(dataDmm)
dataDBox = pd.DataFrame(dataD_box, columns=dataD.columns, index=dataD.index)


# =============================================================================
# Concatenate blocks into one block
# =============================================================================

# Standardised data
dataSc = pd.concat([dataAsc, dataBsc, dataCsc, dataDsc, dataEsc], 
                    axis=1)

XSc = dataSc.copy()


# Yeo Johnson transformation of data in block D
dataYeo = pd.concat([dataAsc, dataBsc, dataCsc, dataDYeo, dataEsc], 
                     axis=1)

XYeo = dataYeo.copy()


# BoxCox transformation of block D
dataBox = pd.concat([dataAsc, dataBsc, dataCsc, dataDBox, dataEsc], 
                     axis=1)

XBox = dataBox.copy()


# =============================================================================
# Target
# =============================================================================

y = np.squeeze(target.values.reshape(1, -1))
# Checking distribution of classes
sum(target.loc[:, 'a'] == 1)    
sum(target.loc[:, 'a'] == 0)    


# Checking shapes
XSc.shape   
XYeo.shape
XBox.shape                   
y.shape                      


# =============================================================================
# Define function for model evaluation
# =============================================================================

# evaluate a model with a given number of repeats
def evaluate_model(model, X, y, repeats, splits, metric):
 	
    # prepare the cross-validation procedure
 	cv = RepeatedStratifiedKFold(n_splits=splits, 
                                 n_repeats=repeats, 
                                 random_state=1)
 	
    # evaluate model
 	scores = cross_val_score(model, X, y, 
                             scoring=metric, 
                             cv=cv, 
                             n_jobs=-1,
                             verbose=0)
 	return scores



# =============================================================================
# Run repeated K-fold
# =============================================================================

"""
Running repeated K-fold cross-validation and testing performance with 
different methods for data transformation.

1. Standardised data
2. Standardised data and Yeo Johnson transformation
3. Normalised data and BoxCox transformation
4. Standardised data and Yeo Johnson tranformation of data in block D.
5. Normalised data and BoxCox transformation of data in block D. Rest of the 
blocks are standardised. 

Grid search has been performed for the different methods and the best 
scores for the parameters are used in the different cases. 
"""


# Standardised data 
# -----------------------------------------------------------------------------
mcc_scorer = make_scorer(matthews_corrcoef)
metric = mcc_scorer
scaler = StandardScaler()

# Creating models
lg_model = LogisticRegression(C=0.01,
                              solver='liblinear')

aggressive_model = PassiveAggressiveClassifier(C=0.0001, 
                                               loss='hinge')

forest_model = RandomForestClassifier(criterion='gini',
                                      max_features='auto',
                                      max_depth=10,
                                      n_estimators=40, 
                                      random_state=1,
                                      n_jobs=-1)

knn_model = KNeighborsClassifier(n_neighbors=10,
                                 algorithm='auto',
                                 p=2, weights='distance')

svc_model = SVC(kernel='rbf', 
                 C=10.0, 
                 gamma=0.01, 
                 random_state=1)



# Creating lists with model and modelnames
models = [lg_model, aggressive_model, forest_model, knn_model, svc_model]
modelname = ['LogisticRegression', 'PassiveAggressiveClassifier', 
             'RandomForest', 'KNN', 'SVC']



# Getting scores
scores_std = {}
for model, modelname in zip(models, modelname):
    pipeline = Pipeline(steps=[('s', scaler),
                               ('m', model)])
    
    rkf_scores = evaluate_model(pipeline,
                            X=X, 
                            y=y, 
                            repeats=10,
                            splits=5,
                            metric=metric) 
    
    rkf_scores_mean = round(np.mean(rkf_scores),4)
    rkf_scores_std = round(np.std(rkf_scores, ddof=1),4)
    scores_std[modelname] = {'Mean score ' : rkf_scores_mean, 
                             'Std score' : rkf_scores_std}


# Creating dataframe and exporting file    
scores_std_df = pd.DataFrame(scores_std)
scores_std_df.to_csv('scores_std.csv')
    



    
# Standardised data and Yeo-Johnson transformation
# -----------------------------------------------------------------------------
mcc_scorer = make_scorer(matthews_corrcoef)
metric = mcc_scorer
scaler = StandardScaler()
power = PowerTransformer(method='yeo-johnson')


# Creating models
lg_model = LogisticRegression(C=0.01, solver='liblinear')

aggressive_model = PassiveAggressiveClassifier(C=0.0001,
                                               loss='hinge')

forest_model = RandomForestClassifier(criterion='gini',
                                      max_features='sqrt',
                                      n_estimators=100, 
                                      max_depth=100,
                                      random_state=1,
                                      n_jobs=-1)

knn_model = KNeighborsClassifier(n_neighbors=2,
                                 algorithm='auto',
                                 p=1, weights='distance', 
                                 metric='minkowski')

svc_model = SVC(kernel='rbf', C=10.0, 
                gamma=0.05, random_state=1)



# Creating lists with model and modelnames
models = [lg_model, aggressive_model, forest_model, knn_model, svc_model]
modelname = ['LogisticRegression', 'PassiveAggressiveClassifier', 
             'RandomForest', 'KNN', 'SVC']



# Getting scores
scores_yeo = {}
for model, modelname in zip(models, modelname):
    pipeline = Pipeline(steps=[('s', scaler),
                               ('p', power),
                               ('m', model)])
    
    rkf_scores = evaluate_model(pipeline,
                            X=X, 
                            y=y, 
                            repeats=10,
                            splits=5,
                            metric=metric) 
    
    rkf_scores_mean = round(np.mean(rkf_scores),4)
    rkf_scores_std = round(np.std(rkf_scores, ddof=1),4)
    scores_yeo[modelname] = {'Mean score ' : rkf_scores_mean, 
                             'Std score' : rkf_scores_std}


# Creating dataframe and exporting file      
scores_yeo_df = pd.DataFrame(scores_yeo)
scores_yeo_df.to_csv('scores_yeo.csv')
    
    
    
    
# MinMax scaled data and BoxCox transformed
# -----------------------------------------------------------------------------
mcc_scorer = make_scorer(matthews_corrcoef)
metric = mcc_scorer
scaler = MinMaxScaler(feature_range=(1,2))
power = PowerTransformer(method='box-cox')


# Creating models
lg_model = LogisticRegression(C=0.0001, solver='lbfgs')

aggressive_model = PassiveAggressiveClassifier(C=0.0001, 
                                               loss='hinge')

forest_model = RandomForestClassifier(criterion='gini',
                                      max_depth=10,
                                      max_features='auto',
                                      n_estimators=20, 
                                      random_state=1,
                                      n_jobs=-1)

knn_model = KNeighborsClassifier(n_neighbors=2,
                                 algorithm='auto',
                                 p=1, weights='uniform')

svc_model = SVC(kernel='linear', C=0.0001, random_state=1)



# Creating lists with model and modelnames
models = [lg_model, aggressive_model, forest_model, knn_model, svc_model]
modelname = ['LogisticRegression', 'PassiveAggressiveClassifier', 
             'RandomForest', 'KNN', 'SVC']



# Getting scores
scores_box = {}
for model, modelname in zip(models, modelname):
    pipeline = Pipeline(steps=[('s', scaler),
                               ('p', power),
                               ('m', model)])
    
    rkf_scores = evaluate_model(pipeline,
                            X=X, 
                            y=y, 
                            repeats=10,
                            splits=5,
                            metric=metric) 
    
    rkf_scores_mean = round(np.mean(rkf_scores),4)
    rkf_scores_std = round(np.std(rkf_scores, ddof=1),4)
    scores_box[modelname] = {'Mean score ' : rkf_scores_mean, 
                             'Std score' : rkf_scores_std}


# Creating dataframe and exporting file  
scores_box_df = pd.DataFrame(scores_box)
scores_box_df.to_csv('scores_box.csv')




# Standardised data with Yeo-Johnson transformed data in block D.
# -----------------------------------------------------------------------------
mcc_scorer = make_scorer(matthews_corrcoef)
metric = mcc_scorer
# scaler = StandardScaler()


# Creating models
lg_model = LogisticRegression(C=0.01, solver='liblinear')

aggressive_model = PassiveAggressiveClassifier(C=0.0001, 
                                               loss='hinge')

forest_model = RandomForestClassifier(criterion='gini',
                                      max_depth=70,
                                      max_features='auto',
                                      n_estimators=100, 
                                      random_state=1,
                                      n_jobs=-1)

knn_model = KNeighborsClassifier(n_neighbors=5,
                                 algorithm='auto',
                                 p=1, weights='uniform')

svc_model = SVC(kernel='rbf', C=10.0, gamma=0.01, random_state=1)


# Creating lists with model and modelnames
models = [lg_model, aggressive_model, forest_model, knn_model, svc_model]
modelname = ['LogisticRegression', 'PassiveAggressiveClassifier', 
             'RandomForest', 'KNN', 'SVC']



# Getting scores
scores_yeoD = {}
for model, modelname in zip(models, modelname):
    pipeline = Pipeline(steps=[('m', model)])
    
    rkf_scores = evaluate_model(pipeline,
                                X=XYeo, 
                                y=y, 
                                repeats=10,
                                splits=5,
                                metric=metric) 
    
    rkf_scores_mean = round(np.mean(rkf_scores),4)
    rkf_scores_std = round(np.std(rkf_scores, ddof=1),4)
    scores_yeoD[modelname] = {'Mean score ' : rkf_scores_mean, 
                             'Std score' : rkf_scores_std}
    
    
# Creating dataframe and exporting file      
scores_yeoD_df = pd.DataFrame(scores_yeoD)
scores_yeoD_df.to_csv('scores_yeoD.csv')




# MinMax scaled data and BoxCox transformed data in block D. 
# Standardised data in rest of the blocks.
# -----------------------------------------------------------------------------
mcc_scorer = make_scorer(matthews_corrcoef)
metric = mcc_scorer
# scaler = StandardScaler()

# Creating models
lg_model = LogisticRegression(C=0.01, solver='liblinear')

aggressive_model = PassiveAggressiveClassifier(C=0.0001, 
                                               loss='hinge')

forest_model = RandomForestClassifier(criterion='gini',
                                      max_depth=70,
                                      max_features='auto',
                                      n_estimators=140, 
                                      random_state=1,
                                      n_jobs=-1)

knn_model = KNeighborsClassifier(n_neighbors=2,
                                 algorithm='auto',
                                 p=1, weights='distance')

svc_model = SVC(kernel='rbf', 
                 C=10.0, 
                 gamma=0.01, 
                 random_state=1)


# Creating lists with model and modelnames
models = [lg_model, aggressive_model, forest_model, knn_model, svc_model]
modelname = ['LogisticRegression', 'PassiveAggressiveClassifier', 
             'RandomForest', 'KNN', 'SVC']


# Getting scores
scores_boxD = {}
for model, modelname in zip(models, modelname):
    pipeline = Pipeline(steps=[('m', model)])
    
    rkf_scores = evaluate_model(pipeline,
                            X=XBox, 
                            y=y, 
                            repeats=10,
                            splits=5,
                            metric=metric) 
    
    rkf_scores_mean = round(np.mean(rkf_scores),4)
    rkf_scores_std = round(np.std(rkf_scores, ddof=1),4)
    scores_boxD[modelname] = {'Mean score ' : rkf_scores_mean, 
                              'Std score' : rkf_scores_std}


# Creating dataframe and exporting file  
scores_boxD_df = pd.DataFrame(scores_boxD)
scores_boxD_df.to_csv('scores_boxD.csv')
