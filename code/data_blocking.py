#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 29 09:27:54 2021

@author: charlottolofsson
"""

# =============================================================================
# Modules
# =============================================================================

import pandas as pd
import numpy as np
import pyreadstat

# =============================================================================
# Loading and preprocessing the data
# =============================================================================

data, meta = pyreadstat.read_sav('../data/NMBU_June_2021.sav', 
                                 encoding='latin1')
data_apoe, meta = pyreadstat.read_sav('../data/NMBU_June_2021_apoe.sav', 
                                      encoding='latin1')

# data_apoe = data_apoe.set_index('newID')
# data = data.set_index('newID')
unique = data_apoe.newID.unique()


"""
# Framingham Risk Score
# -----------------------------------------------------------------------------
data_scores, meta = pyreadstat.read_sav('../data/FRS_DDI_211008_Charlott.sav')
data_syntax = pd.read_csv('../data/Syntax_FRS_normal_and_simple.sps')

# Relevant features
score_features = ['Smoking_currently', 'dm', 'frs_total', 
                  'frs_simp_total', 'frs_total_wo_age', 
                  'frs_simp_total_wo_age']
scores_red = data_scores.loc[:, score_features]


# Concatenating dataframes
# -----------------------------------------------------------------------------
data_new = pd.concat([data_apoe, scores_red], axis=1)
data = data_new.copy()

"""

data = data_apoe.copy()

ass1 = data.loc[data['assessment'] == 1]
ass2 = data.loc[data['assessment'] == 2]
ass3 = data.loc[data['assessment'] == 3]
data = ass1.copy()


# Unique patients
targets = list(data.index.unique())


# =============================================================================
# Dividing into blocks
# =============================================================================

# Block A: Background information
# -----------------------------------------------------------------------------

blockAFeatures = ['subj_group']
blockA = data.loc[:, blockAFeatures]
blockACategories = list(blockA.subj_group.unique())

# Mapping numerical values to categories
DataASeries = blockA.squeeze()
DataAMap = DataASeries.map({0 : 'cognitive symptom group', 
                            6 : 'cognitive symptom group - extension from previous study', 
                            1 : 'parkinsonism symptom group', 
                            2 : 'spouse control group', 
                            3 : 'ordinary control group', 
                            4 : 'orthopedic control group', 
                            5 : 'family history control group'}) 
blockA['subj_group'] = DataAMap
blockA = blockA.dropna(axis=0)


blockACategories = list(blockA.subj_group.unique())

"""
dataA = blockA.dropna(axis=0)
# Unique patients
uniqueA = list(dataA.newID.unique())
uniqueA = pd.DataFrame(uniqueA)
uniqueA.to_csv('uniqueA.csv')
"""


# Block B: Environment and heritage
# -----------------------------------------------------------------------------

# Environmental factors
# -----------------------------------------------------------------------------
blockBFeaturesEnv = ['age', 'edu_years', 'edu_level', 'smok', 'bp_recum_sys']
# blockBFeaturesEnv = ['age', 'edu_years', 'edu_level', 'frs_total_wo_age']
blockBEnv = data.loc[:, blockBFeaturesEnv]


# Creating feature for cardiovascular disease
cardiovascularFeatures = ['stroke_isch', 'stroke_hem', 'tia', 'coronary', 
                          'afib', 'heartfail', 'heartsurg', 'ht', 'claudi', 
                          'hyperchol']
cardioVas = data.loc[:, cardiovascularFeatures]
cardioVas['sum'] = cardioVas.sum(axis=1)
cardioVascularDisease = cardioVas['sum'].apply(lambda x: 1 if x > 0 else x)
blockBEnv['cardiovascular_disease'] = cardioVascularDisease


# Changing value for smoking
smoking = blockBEnv['smok'].apply(lambda x: 1 if x > 0 else 0)
blockBEnv['smok'] = smoking


# Change value for blood-pressure
out_bp_recum_sys = blockBEnv.loc[blockBEnv['bp_recum_sys'] < 80]
bp_recum_sys = blockBEnv['bp_recum_sys'].apply(lambda x: 150 if x < 80 else x)
blockBEnv['bp_recum_sys'] = bp_recum_sys


# Heritage factors
# -----------------------------------------------------------------------------
blockBFeturesHer = ['gender', 'degree1_dem', 'bl_apoe_max']
blockBHer = data.loc[:, blockBFeturesHer]


# Combining environment and heritage
# -----------------------------------------------------------------------------
blockB = pd.concat([blockBEnv, blockBHer], axis=1)

# Replacing empty strings with nan
blockB['bl_apoe_max'].replace('', np.nan, inplace=True)
blockB['gender'].replace('', np.nan, inplace=True)

# Counting nan values
blockB.isnull().sum()

# Unique values for bl_apoe
bl_apoe = list(blockB.bl_apoe_max.unique())

"""
dataB = blockB.dropna(axis=0)
# Unique patients
uniqueB = list(dataB.newID.unique())
uniqueB = pd.DataFrame(uniqueB)
uniqueB.to_csv('uniqueB.csv')
"""


# Block C: Blood tests and cognitive tests
# -----------------------------------------------------------------------------

blockCFeatures = ['mmse_total', 'clock_score', 'tmta_sec', 'tmtb_sec', 
                  'vosp_tscore', 'cowat_tscore', 'cerad_recall']
blockC = data.loc[:, blockCFeatures]


"""
Change value to max score if the value is over max score for 'tmta_sec' and 
'tmtb_sec'.
Max score for 'tmta_sec' is 71. 
Max score for 'tmtb_sec' is 166.
"""
tmta_sec = blockC['tmta_sec'].apply(lambda x: 71 if x > 71 else x)
blockC['tmta_sec'] = tmta_sec
tmtb_sec = blockC['tmtb_sec'].apply(lambda x: 166 if x > 166 else x)
blockC['tmtb_sec'] = tmtb_sec


"""
dataC = blockC.dropna(axis=0)
# Unique patients
uniqueC = list(dataC.newID.unique())
uniqueC = pd.DataFrame(uniqueC)
uniqueC.to_csv('uniqueC.csv')
"""


# Block D: Lesion and white matter hyperintensity
# -----------------------------------------------------------------------------
blockDFeatures = ['LesP1', 'LesP3', 'LesO1', 'LesO3', 'LesF1', 
                  'LesF3', 'LesT1', 'LesT3', 'LesP2', 'LesP4', 
                  'LesO2', 'LesO4', 'LesF2', 'LesF4', 'LesT2', 
                  'LesT4']
blockDdiv = data.loc[:, blockDFeatures]

# Dividing the features with TIV.
tiv = data.loc[:, 'TIV']
blockD = blockDdiv.div(tiv.iloc[0], axis='rows')

# Adding the features that don't need to be divided with TIV
psmd = data.loc[:, 'PSMD']
wmho_rv = data.loc[:, 'WMHo_rV']
blockD['PSMD'] = psmd
blockD['WMHo_rV'] = wmho_rv
#blockD = blockD.drop(index=91, axis=0)

# Combining values for the four different regions of LesF, LesO, LesP and LesT
blockD['LesF'] = blockD['LesF1'] + blockD['LesF2'] + blockD['LesF3'] + blockD['LesF4']
blockD['LesO'] = blockD['LesO1'] + blockD['LesO2'] + blockD['LesO3'] + blockD['LesO4']
blockD['LesP'] = blockD['LesP1'] + blockD['LesP2'] + blockD['LesP3'] + blockD['LesP4']
blockD['LesT'] = blockD['LesT1'] + blockD['LesT2'] + blockD['LesT3'] + blockD['LesT4']

blockDFeatures2 = ['LesP1', 'LesP3', 'LesO1', 'LesO3', 'LesF1', 
                   'LesF3', 'LesT1', 'LesT3', 'LesP2', 'LesP4', 
                   'LesO2', 'LesO4', 'LesF2', 'LesF4', 'LesT2', 
                   'LesT4']

blockD = blockD.drop(blockDFeatures2, axis=1)


"""
dataD = blockD.dropna(axis=0)
# Unique patients
uniqueD = list(dataD.newID.unique())
uniqueD = pd.DataFrame(uniqueD)
uniqueD.to_csv('uniqueD.csv')
"""


# Block E: MR images of subcortial brain structures
# Combat ATN and combined right and left
# -----------------------------------------------------------------------------
blockEFeatures = ['Anterior_hippocampus.combat_ATN', 
                  'Posterior_hippocampus.combat_ATN', 
                  'MISC.combat_ATN', 'Meninges_PHC.combat_ATN', 
                  'ERC.combat_ATN', 'Br35.combat_ATN', 'Br36.combat_ATN',
                  'PHC.combat_ATN', 'ColSul.combat_ATN', 
                  'Meninges.combat_ATN']
# 'Scanner_ASHS', 'icv_ASHS'
blockE = data.loc[:, blockEFeatures]


"""
dataE = blockE.dropna(axis=0)
# Unique patients
uniqueE = list(dataE.newID.unique())
uniqueE = pd.DataFrame(uniqueE)
uniqueE.to_csv('uniqueE.csv')
"""


# Target
# -----------------------------------------------------------------------------
targetFeature = ['a']
target = data.loc[:, targetFeature]
targets = list(target.a.unique())

# Removing empty target values
targetAll = target.dropna(axis=0)
targetAll.to_csv('targetAll.csv')

# All the data
Data = pd.concat([blockA, blockB, blockC, blockD, blockE], axis=1)


# =============================================================================
# Creating csv files of the blocks
# =============================================================================

"""
Finding data for the blocks and creating csv files in three ways.
1. Block-wise data with patients that have target.
2. Block-wise data with patients that have targets and no missing 
   values in the block.
3. Block-wise data with patients that have targets and no missing
   values across all blocks.
"""

# Finding subject labels that have targets and selecting theese from data
# Creating dataframes for the different blocks
subjects = targetAll.index
Data = Data.loc[subjects]


# Block A
blockA = Data.loc[:, blockA.columns]
blockA.to_csv('blockA.csv')
dataA = blockA.dropna(axis=0)
dataA.to_csv('dataA.csv')


# Block B
blockB = Data.loc[:, blockB.columns]
blockB.to_csv('blockB.csv')
dataB = blockB.dropna(axis=0)
dataB.to_csv('dataB.csv')


# Block C
blockC = Data.loc[:, blockC.columns]
blockC.to_csv('blockC.csv')
dataC = blockC.dropna(axis=0)
dataC.to_csv('dataC.csv')


# Block D
blockD = Data.loc[:, blockD.columns]
blockD.to_csv('blockD.csv')
dataD = blockD.dropna(axis=0)
dataD.to_csv('dataD.csv')


# Block E 
blockE = Data.loc[:, blockE.columns]
blockE = blockE.T.drop_duplicates().T
blockE.to_csv('blockE.csv')
dataE = blockE.dropna(axis=0)
dataE.to_csv('dataE.csv')


# Creating dataframes for the different blocks with the subjects that have no
# missing values across all the blocks

DataAll = Data.dropna(axis=0)
DataAllID = list(DataAll.iloc[:, 0].unique())

# Finding index for the subjects with no missing data
subjects_test = pd.DataFrame(DataAll.index)
subjects_test.to_csv('test_subjects.csv')
subjects_test_idx = DataAll.index

# DataAll = dataAllInit[~dataAllInit.index.duplicated(keep='first')]
# targetFull = targetAll.loc[subjects].drop(columns=targetFeature).astype(int)
targetFull = targetAll.loc[subjects_test_idx, 'a']
targetFull.to_csv('targetFull.csv')

DataA = DataAll.loc[:, blockA.columns]
DataA.to_csv('DataA_.csv')

DataB = DataAll.loc[:, blockB.columns]
DataB.to_csv('DataB_.csv')

DataC = DataAll.loc[:, blockC.columns]
DataC.to_csv('DataC_.csv')

DataD = DataAll.loc[:, blockD.columns]
DataD.to_csv('DataD_.csv')

DataE = DataAll.loc[:, blockE.columns]
DataE.to_csv('DataE_.csv')

        