#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 14 16:29:06 2021

@author: charlottolofsson
"""


# =============================================================================
# Modules
# =============================================================================

# Data handling
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Models and evaluation
from sklearn.model_selection import RepeatedKFold
from sklearn.model_selection import cross_val_score

# Metrics
from sklearn.metrics import matthews_corrcoef
from sklearn.metrics import make_scorer


# =============================================================================
# Scoring function
# =============================================================================

def cnn_score(actual,prediction):
    cnn = matthews_corrcoef(actual, prediction)
    return cnn

scorer = make_scorer(cnn_score, greater_is_better=True)


# =============================================================================
# Function for plotting value for MCC as a function of parameter value
# =============================================================================


def model_evaluator(x, y, model, modelname, param_dict, splits, repeats):
    """
    A function that test the MCC for training and test data for different
    values for a given hyperparameter. The value for MCC is plotted as a 
    function of the value of the hperparameter. The results are stored in a
    DataFrame.

    Parameters
    ----------
    x : DataFrame
        Containing the data.
    y : Series
        Containing the target.
    modelname : String
        Name of the model.
    param_dict : Dictionary
        Dictionary with the name of the hyperparameter as key and the value
        is the values to test performance on.
    splits : Int
        Number of splits to use in RepeatedKFold.
    repeats : Int
        Number of repeats to use in RepeatedKFold.
        
    Returns
    -------
    acc_df : DataFrame
        DataFrame containing the value for MCC for training and test data
        for the given values of the hyperparameter.

    """
    
    
    # Empty lists to store the results
    scores = []
    scores_avg = []
    
    # Making a list of the hyperparameters to test
    params = []
    for key in param_dict.keys():
        params.append(key)
        
    # Getting the values to test for the given hyperparameter
    for param in params:
        param_range = param_dict[param]
        
        # Looping through the values
        for value in param_range:
                
            cv = RepeatedKFold(n_splits=splits, n_repeats=repeats, random_state=1)
           	# Evaluating model
            model.set_params(value)
            scoring_model = cross_val_score(model, x, y, scoring=scorer, cv=cv, n_jobs=1)
            # Storing results in lists
            scores.append(scoring_model)
            scores_avg.append(np.mean(scores))
        
            
        # Dataframe with scores
        score = {'score':scores_avg}
        score_df = pd.DataFrame(data=score)
        if modelname == 'Logistic Regression': 
            score_df[param] = ['10**{0}'.format(p) for p in param_range]
        elif modelname == 'SVC':
            score_df[param] = ['10**{0}'.format(p) for p in param_range]
        elif modelname == 'PassiveAggressiveClassifier':
            score_df[param] = ['10**{0}'.format(p) for p in param_range]
            
        else:
            score_df[param] = param_range


        # Plotting
        x = score_df.loc[:, param]
        y = score_df.loc[:, 'score']
        
        fig, ax = plt.subplots(1, 1, tight_layout=True)
        ax.plot(x, y, color='blue', marker='o', 
                 markersize=5, label='Average score with {0} splits and {1} repeats'.format(splits, repeats))
        
        ax.set_title(modelname)
        ax.set_ylabel('MCC', fontsize=13)
        ax.set_xlabel('Value for parameter: ' + param, fontsize=13)
        ax.set_xticks(x)            
        ax.set_xticklabels(x, fontsize=10, rotation=45)         
        ax.yaxis.set_tick_params(labelsize=13)
        ax.legend(fontsize=11, loc='lower right')
        ax.grid()
        
        fig.savefig(modelname + '_scores.png')
        
        
    return score_df



