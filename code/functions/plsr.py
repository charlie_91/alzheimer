#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  5 21:02:26 2021

@author: charlottolofsson
"""

# =============================================================================
# Modules
# =============================================================================

import pandas as pd
import hoggorm as ho
import hoggormplot as hop

# =============================================================================
# PLSR function
# =============================================================================

def plsr(data, target, components=3, plot=False):
    """
    A function that performs Partial Least Square Regression using the 
    hoggorm pakkage.
    https://github.com/olivertomic/hoggorm
    
    Parameters
    ----------
    data : DataFrame
        Containing the numeric values for the features.
    target : DataFrame
        Containing the targets.
    components : Integer, optional
        Number of components. The default is 3.
    plot : Boolean
        If true: returns plot of x scores, loadings, x  and y correlation loadings, 
        biplot, regression coefficients and explained variance in y. 
        Default is false.

    Returns
    -------
    plsr_results_x : Dictionary
        Dictionary containing results from PLSR for x.
    plsr_results_y : Dictionary
        Dictionary containing results from PLSR for y.

    """
    
    
    # Get the values from the data frame
    X = data.values
    y = target.values
    
    # Get the variable or columns names
    X_features = list(data.columns)
    y_features = list(target.columns)
    
    # Get the object or row names
    X_objects = list(data.index)
    # y_objects = list(target.index)
    
    # Initialising model
    model = ho.nipalsPLS1(arrX=X, Xstand=False, 
                      vecy=y,
                      cvType=["loo"], 
                      numComp=components)
    
    
    # X scores 
    X_scores = pd.DataFrame(model.X_scores())
    X_scores.index = X_objects
    X_scores.columns = ['Comp {0}'.format(x+1) for x in 
                         range(model.X_scores().shape[1])]
    
    
    # X loadings 
    X_loadings = pd.DataFrame(model.X_loadings())
    X_loadings.index = X_features
    X_loadings.columns = ['Comp {0}'.format(x+1) for x in 
                          range(model.X_loadings().shape[1])]
    
    # Y loadings 
    Y_loadings = pd.DataFrame(model.Y_loadings())
    Y_loadings.index = y_features
    Y_loadings.columns = ['Comp {0}'.format(x+1) for x in 
                          range(model.Y_loadings().shape[1])]
    
    
    # X correlation loadings 
    X_corrloadings = pd.DataFrame(model.X_corrLoadings())
    X_corrloadings.index = X_features
    X_corrloadings.columns = ['Comp {0}'.format(x+1) for x in 
                              range(model.X_corrLoadings().shape[1])]
    
    # Y correlation loadings 
    Y_corrloadings = pd.DataFrame(model.Y_corrLoadings())
    Y_corrloadings.index = y_features
    Y_corrloadings.columns = ['Comp {0}'.format(x+1) for x in 
                              range(model.Y_corrLoadings().shape[1])]
    
    
    
    # Calibrated explained variance of each component in X
    X_calExplVar = pd.DataFrame(model.X_calExplVar())
    X_calExplVar.columns = ['calibrated explained variance in X']
    X_calExplVar.index = ['Comp {0}'.format(x+1) for x in 
                          range(model.X_loadings().shape[1])]

    # Calibrated explained variance of each component in Y
    Y_calExplVar = pd.DataFrame(model.Y_calExplVar())
    Y_calExplVar.columns = ['calibrated explained variance in Y']
    Y_calExplVar.index = ['Comp {0}'.format(x+1) for x in 
                          range(model.Y_loadings().shape[1])]
    
    
    # Cumulative calibrated explained variance in X
    X_cumCalExplVar = pd.DataFrame(model.X_cumCalExplVar())
    X_cumCalExplVar.columns = ['cumulative calibrated explained variance in X']
    X_cumCalExplVar.index = ['Comp {0}'.format(x) for x in 
                             range(model.X_loadings().shape[1] + 1)]
    
    # Cumulative calibrated explained variance in Y
    Y_cumCalExplVar = pd.DataFrame(model.Y_cumCalExplVar())
    Y_cumCalExplVar.columns = ['cumulative calibrated explained variance in Y']
    Y_cumCalExplVar.index = ['Comp {0}'.format(x) for x in 
                             range(model.Y_loadings().shape[1] + 1)]
    
    
    # Cumulative calibrated explained variance for each variable in X
    X_cumCalExplVar_ind = pd.DataFrame(model.X_cumCalExplVar_indVar())
    X_cumCalExplVar_ind.columns = X_features
    X_cumCalExplVar_ind.index = ['Comp {0}'.format(x) for x in 
                                  range(model.X_loadings().shape[1] + 1)]

    
    # Validated explained variance of each component X
    X_valExplVar = pd.DataFrame(model.X_valExplVar())
    X_valExplVar.columns = ['validated explained variance in X']
    X_valExplVar.index = ['Comp {0}'.format(x+1) for x in 
                             range(model.X_loadings().shape[1])]
    
    # Validated explained variance of each component Y
    Y_valExplVar = pd.DataFrame(model.Y_valExplVar())
    Y_valExplVar.columns = ['validated explained variance in Y']
    Y_valExplVar.index = ['Comp {0}'.format(x+1) for x in 
                             range(model.Y_loadings().shape[1])]
   
    
    # Cumulative validated explained variance in X
    X_cumValExplVar = pd.DataFrame(model.X_cumValExplVar())
    X_cumValExplVar.columns = ['cumulative validated explained variance in X']
    X_cumValExplVar.index = ['Comp {0}'.format(x) for x in 
                                range(model.X_loadings().shape[1] + 1)]
    
    # Cumulative validated explained variance in Y
    Y_cumValExplVar = pd.DataFrame(model.Y_cumValExplVar())
    Y_cumValExplVar.columns = ['cumulative validated explained variance in Y']
    Y_cumValExplVar.index = ['Comp {0}'.format(x) for x in 
                             range(model.Y_loadings().shape[1] + 1)]
    

    
    if plot:
        hop.plot(model, comp=[1, 2], 
                 plots=[1, 2, 3, 4, 5, 6], 
                 objNames=X_objects, 
                 XvarNames=X_features,
                 YvarNames=y_features)
        

    plsr_results_x = {'Scores' : X_scores, 'Loadings' : X_loadings, 
                      'Correlation_loadings' : X_corrloadings,
                      'Calibrated explained variance' : X_calExplVar,
                      'Cumulative calibrated explained variance' : X_cumCalExplVar}
        
    plsr_results_y = {'Loadings' : Y_loadings, 
                      'Correlation loadings' : Y_corrloadings,
                      'Calibrated explained variance' : Y_calExplVar,
                      'Cumulative calibrated explained variance' : Y_cumCalExplVar}    

    
    return [plsr_results_x, plsr_results_y]
    
    
    
    
    
    