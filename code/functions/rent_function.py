#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 15 17:04:57 2021

@author: charlottolofsson
"""


# =============================================================================
# Modules
# =============================================================================


from sklearn.linear_model import LogisticRegression as LR
from RENT import RENT, stability
from sklearn.metrics import matthews_corrcoef

import warnings
warnings.filterwarnings("ignore")


# =============================================================================
# Function for running RENT
# =============================================================================


def rent(x, y, xtest, ytest, C_params, l1_ratios, tau_1, tau_2, k=100, poly=False):
    """
    https://github.com/NMBU-Data-Science/RENT/blob/master/examples/Classification_example.ipynb
    """
    
    # Define a range of regularisation parameters C for elastic net.     
    # Define a reange of l1-ratios for elastic net. 
    
    
    if poly == True:
        
        modelRent = RENT.RENT_Classification(data=x, 
                                             target=y, 
                                             feat_names=x.columns, 
                                             C=C_params, 
                                             l1_ratios=l1_ratios,
                                             autoEnetParSel=False,
                                             poly='ON',
                                             testsize_range=(0.25,0.25),
                                             scoring='mcc',
                                             classifier='logreg',
                                             K=k,
                                             random_state=0,
                                             verbose=1)
        
        
        # Training the model
        modelRent.train()
        
        # Getting best parameters
        params = modelRent.get_enet_params()
        
        # Selected features
        selected_features = modelRent.select_features(tau_1_cutoff=tau_1, 
                                                      tau_2_cutoff=tau_2, 
                                                      tau_3_cutoff=0.975)
        
        
        # Summary 
        summary = modelRent.get_summary_criteria()
        
        # Features selected based on criteria
        features = summary.iloc[:, selected_features]
        feature_names = features.columns
        
        # Splitting into individual features
        individual_features = []
        for i in feature_names:
            i = i.split('*')
            for k in i:
                k = k.split(',')
                for l in k:
                    individual_features.append(l)
                    
        features_list = []
        for feature in individual_features:
            if feature in x.columns:
                features_list.append(feature)


        # Creating train and test data with selected features
        train_data = x.loc[:, features_list]
        test_data = xtest.loc[:, features_list]

        # Creating model
        pred_model = LR(penalty='none', 
                        max_iter=8000, 
                        solver='saga', 
                        random_state=0).fit(train_data, y)
        
        # Scoring model on test data
        score = matthews_corrcoef(ytest, pred_model.predict(test_data))
        
        
        return [modelRent, params, feature_names, summary, score]
        
        
    else:

        modelRent = RENT.RENT_Classification(data=x, 
                                             target=y, 
                                             feat_names=x.columns, 
                                             C=C_params, 
                                             l1_ratios=l1_ratios,
                                             autoEnetParSel=False,
                                             poly='OFF',
                                             testsize_range=(0.25,0.25),
                                             scoring='mcc',
                                             classifier='logreg',
                                             K=k,
                                             random_state=0,
                                             verbose=1)
        
        # Training the model
        modelRent.train()
        
        # Getting best parameters
        params = modelRent.get_enet_params()
        
        # Selected features
        selected_features = modelRent.select_features(tau_1_cutoff=tau_1, 
                                                      tau_2_cutoff=tau_2, 
                                                      tau_3_cutoff=0.975)
    
        # Summary
        summary = modelRent.get_summary_criteria()
    
        # Identify feature names
        feature_names = x.columns[selected_features]
        
        # Plot
        # modelRent.plot_selection_frequency()
        
        # Checking performance
        train_data = x.iloc[:, selected_features]
        test_data = xtest.iloc[:, selected_features]
        
        pred_model = LR(penalty='none', max_iter=8000, solver='saga', random_state=0).\
            fit(train_data, y)
    
        score = matthews_corrcoef(ytest, pred_model.predict(test_data))
        
        
        
        return [modelRent, params, feature_names, summary, score]

    

