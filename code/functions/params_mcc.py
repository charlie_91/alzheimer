#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 17 14:26:23 2021

@author: charlottolofsson
"""

# =============================================================================
# Modules
# =============================================================================

# Data handling
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Processing
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

# Modules
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

# Metric
from sklearn.metrics import matthews_corrcoef


# =============================================================================
# Function for plotting value for MCC as a function of parameter value
# =============================================================================


def paramsMcc(x, y, blockname, modelname, param_dict):
    """
    A function that test the MCC for training and test data for different
    values for a given hyperparameter. The value for MCC is plotted as a 
    function of the value of the hperparameter. The results are stored in a
    DataFrame.

    Parameters
    ----------
    x : DataFrame
        Containing the data.
    y : Series
        Containing the target.
    modelname : String
        Name of the model.
    param_dict : Dictionary
        Dictionary with the name of the hyperparameter as key and the value
        is the values to test performance on.

    Returns
    -------
    acc_df : DataFrame
        DataFrame containing the value for MCC for training and test data
        for the given values of the hyperparameter.

    """
    
    # Empty lists to store the results
    TrainMcc = []
    TestMcc = []
    
    # Making a list of the hyperparameters to test
    params = []
    for key in param_dict.keys():
        params.append(key)
    # Getting the values to test for the given hyperparameter
    for param in params:
        param_range = param_dict[param]
        # Looping through the values
        for value in param_range:
            # Looping through increasing values for random_state of train_test_split
            accTrainList = []
            accTestList = []  
            
            for rs in range(1, 101):
                # Splitting data
                X_train, X_test, y_train, y_test = train_test_split(
                    x, y, test_size=0.3, random_state=rs, stratify=y)
                
                # Standardising
                sc = StandardScaler()
                sc.fit(X_train)
                X_train_std = sc.transform(X_train)
                X_test_std = sc.transform(X_test)
                
                # Initializing model
                if modelname == 'Logistic Regression':
                    model = LogisticRegression(C=10.**value, solver='liblinear', 
                                               random_state=1)
                    
                if modelname == 'Passive Aggressive Classifier':
                    model = PassiveAggressiveClassifier(C=10.**value, random_state=1)
                    
                if modelname == 'Random Forest':
                    model = RandomForestClassifier(n_estimators=value, 
                                                   random_state=1)
                    
                if modelname == 'KNN':
                    model = KNeighborsClassifier(n_neighbors=value)
                
                if modelname == 'SVC':
                    model = SVC(C=10.**value, random_state=1)
                
                # Fitting model
                model.fit(X_train_std, y_train)
                # Predicting
                y_pred_train = model.predict(X_train_std)
                y_pred_test = model.predict(X_test_std)
                # Getting value for MCC score
                mcc_train = matthews_corrcoef(y_train, y_pred_train)
                mcc_test = matthews_corrcoef(y_test, y_pred_test)
                # Compute accuracies for train and test data for 1 train test split
                accTrainList.append(mcc_train)
                accTestList.append(mcc_test)
            
            
            # Average training accuracy 
            accTrain_aver = np.mean(accTrainList)
            # accTrain_std = np.std(accTrainList)
            
            # Average test accuracy
            accTest_aver = np.mean(accTestList)
            # accTest_std = np.std(accTestList)
            
            # Average accuracy for each value of the hyperparameter
            TrainMcc.append(accTrain_aver)
            TestMcc.append(accTest_aver)
            
        
            
        # Dataframe with accuracies
        accuracies = {'train acc':TrainMcc, 'test acc':TestMcc}
        acc_df = pd.DataFrame(data=accuracies)
        if modelname == 'Logistic Regression': 
            acc_df[param] = ['10**{0}'.format(p) for p in param_range]
        elif modelname == 'Passive Aggressive Classifier':
            acc_df[param] = ['10**{0}'.format(p) for p in param_range]
        elif modelname == 'SVC':
            acc_df[param] = ['10**{0}'.format(p) for p in param_range]
        else:
            acc_df[param] = param_range

        # Plotting
        x = acc_df.loc[:, param]
        y1 = acc_df.loc[:, 'train acc']
        y2 = acc_df.loc[:, 'test acc']
        
        fig, ax = plt.subplots(1, 1, tight_layout=True)
        ax.plot(x, y1, color='blue', marker='o', 
                 markersize=5, label='Training accuracy')
        ax.plot(x, y2, color='green', linestyle='--',
                 marker='s', markersize=5,
                 label='Test accuracy')
        
        ax.set_title(blockname + '_' + modelname)
        ax.set_ylabel('MCC', fontsize=13)
        ax.set_xlabel('Value for parameter: ' + param, fontsize=13)
        ax.set_xticks(x)            
        ax.set_xticklabels(x, fontsize=10, rotation=45)         
        ax.yaxis.set_tick_params(labelsize=13)
        ax.legend(fontsize=11, loc='upper left')
        ax.grid()
        
        fig.savefig(blockname + modelname + '_params_mcc.png')
        fig.show()
        
        
    return acc_df
                        
                
            