#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  3 13:14:55 2021

@author: charlottolofsson
"""

# =============================================================================
# Modules
# =============================================================================

import pandas as pd
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import StandardScaler


# =============================================================================
# Data preparation function
# =============================================================================

def dataPrep(data, block_features, categorical=False, std=False):
    """
    A function that prepares the data for further analysis. Takes out the 
    relevant block features, removes patients with missing values and
    one-hot-encodes categorical features to numerical values.

    Parameters
    ----------
    data : DataFrame
        DataFrame containing the patient data.
    block_features : List
        List of the features that should be in the block.

    Returns
    -------
    data_num : DataFrame
        DataFrame with numerical features and only containing patients
        with no missing values.
    """
    
    block_data = pd.DataFrame(data.loc[:, block_features])
    block_data = block_data.dropna(axis=0)
    
    
    if categorical:
        # One hot encoding categorical features
        encoder = OneHotEncoder()
        encoder.fit(block_data)
        
        # Making a list of the different categories
        data_features = []
        for category in range(len(encoder.categories_)):
            data_features.extend(list(encoder.categories_[category]))
        
        # Making a list of the pasient IDs
        # data_objects = list(blockData.index)
        
        # Dataframe with numerical values
        data_num = pd.DataFrame(encoder.transform(block_data).toarray(), columns=data_features)
        block_data = data_num.set_index(block_data.index)
        block_features = data_features
        
    if std:
        sc = StandardScaler()
        data_std = sc.fit_transform(block_data)
        block_data = pd.DataFrame(data_std, columns=block_data.columns, 
                                  index=block_data.index)
        
        
    return block_data
    