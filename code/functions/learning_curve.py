#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 14 23:37:17 2021

@author: charlottolofsson
"""


# =============================================================================
# Modules
# =============================================================================

import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import learning_curve
from sklearn.metrics import matthews_corrcoef
from sklearn.metrics import make_scorer

# =============================================================================
# Scoring function
# =============================================================================

def cnn_score(actual,prediction):
    cnn = matthews_corrcoef(actual, prediction)
    return cnn


scorer = make_scorer(cnn_score, greater_is_better=True)

# =============================================================================
# Function for plotting
# =============================================================================

def learning_curve_plot(estimator, estimator_name, x_train, y_train):
    """
    A function that plots the learning curve for a given classifier
    Parameters
    ----------
    estimator : sklearn PipeLine
        A PipeLine with classifier. 
    estimator_name : str
        Name of the estimator, for plotting and saving figure.
    x_train : NumPy array
        Containg the training data.
    y_train : NumPy array
        Containing the targets.

    Returns
    -------
    Saves the figure.

    """
    
    # Using the module learning_curve from sklearn to obtain the scores
    train_sizes, train_scores, test_scores = learning_curve(estimator=estimator, 
                                                        X=x_train, y=y_train, 
                                                        train_sizes=np.linspace(0.1,1.0,10),
                                                        cv=10,
                                                        scoring=scorer,
                                                        n_jobs=1)

    # Finding the mean and std value of the scores
    train_mean = np.mean(train_scores, axis=1)
    train_std = np.std(train_scores, axis=1)
    test_mean = np.mean(test_scores, axis=1)
    test_std = np.std(test_scores, axis=1)
    
    # Plotting the results
    plt.figure()
    plt.plot(train_sizes, train_mean, 
             color='blue', marker='o', 
             markersize=5, label='Training score')
    plt.fill_between(train_sizes, 
                     train_mean - train_std,
                     train_mean + train_std, 
                     alpha=0.15, color='blue')
    plt.plot(train_sizes, test_mean,
             color='green', linestyle='--',
             marker='s', markersize=5,
             label='Validation score')
    plt.fill_between(train_sizes, 
                     test_mean - test_std,
                     test_mean + test_std, 
                     alpha=0.15, color='green')
    
    plt.grid()
    plt.xlabel('Number of training examples')
    plt.ylabel('MCC')
    plt.legend(loc='upper left')
    plt.ylim([0.0, 1.03])
    plt.title(estimator_name)
    plt.savefig(estimator_name + '_learning_curve.png')
    plt.show()
    
    
    