#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 15:46:14 2021

@author: charlottolofsson
"""

# =============================================================================
# Modules
# =============================================================================

import pandas as pd
import hoggorm as ho
import hoggormplot as hop

# =============================================================================
# PCA function
# =============================================================================

def pca(data, components=3, plot=False):
    """
    A function that performs Principal Component Analysis using the 
    hoggorm package
    https://github.com/olivertomic/hoggorm

    Parameters
    ----------
    data : DataFrame
        Containing the numeric values for the features.
    components : Integer, optional
        Number of principal components. The default is 3.
    plot : Boolean
        If true: returns plot of x scores, loadings, x correlation loadings, 
        biplot and explained variance. Default is false.

    Returns
    -------
    pca_results : Dictionary
        A dictionary containing five DataFrames with results from the principal
        component analysis. The loading, scores, calibrated variance
        for each component, cumulative calibrated variance and cumulative
        explained variance for each variable. 

    """
    data_values = data.values
    data_features = list(data.columns)
    data_objects = list(data.index)
    
    # Initialising model
    model = ho.nipalsPCA(arrX=data_values, Xstand=False, cvType=["loo"], 
                         numComp=components)
    
    # Scores
    scores = pd.DataFrame(model.X_scores())
    scores.index = data_objects
    scores.columns = ['PC{0}'.format(x+1) for x in 
                      range(model.X_scores().shape[1])]
    
    # Loadings
    loadings = pd.DataFrame(model.X_corrLoadings())
    loadings.index = data_features
    loadings.columns = ['PC{0}'.format(x+1) for x in 
                        range(model.X_corrLoadings().shape[1])]

    
    # Calibrated variance
    # For each component
    calExplVar = pd.DataFrame(model.X_calExplVar())
    calExplVar.columns = ['calibrated explained variance']
    calExplVar.index = ['PC{0}'.format(x+1) for x in 
                        range(model.X_loadings().shape[1])]
    
    # Cumulative explained variance
    cumCalExplVar = pd.DataFrame(model.X_cumCalExplVar())
    cumCalExplVar.columns = ['cumulative calibrated explained variance']
    cumCalExplVar.index = ['PC{0}'.format(x) for x in 
                           range(model.X_loadings().shape[1] + 1)]
    
    # Cumulative explained variance for each variable 
    cumCalExplVar_ind = pd.DataFrame(model.X_cumCalExplVar_indVar())
    cumCalExplVar_ind.columns = data_features
    cumCalExplVar_ind.index = ['PC{0}'.format(x) for x in 
                               range(model.X_loadings().shape[1] + 1)]
    
    if plot:
        hop.plot(model, comp=[1, 2], 
        plots=[1, 2, 3, 4, 6], 
        objNames=data_objects, 
        XvarNames=data_features)
        
        
    pca_results = {'Scores' : scores, 'Loadings' : loadings, 
                   'Calibrated explained variance' : calExplVar,
                   'Cumulative calibrated explained variance' : cumCalExplVar,
                   'Cumulative calibrated explained variance for each variable' :
                       cumCalExplVar_ind}
        
    
    return pca_results
        
    