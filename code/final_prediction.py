#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 11 22:26:39 2021

@author: charlottolofsson
"""


# =============================================================================
# Modules
# =============================================================================

# Data handling and plotting
import pandas as pd
import numpy as np
from numpy import average

# Preprocessing and models
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import PowerTransformer
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from RENT import RENT, stability

# Scoring
from sklearn.metrics import matthews_corrcoef
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
from sklearn.metrics import make_scorer

# Functions from other scripts
import sys
sys.path.append('../code/functions')
from rent_function import rent


# =============================================================================
# Loading data
# =============================================================================

# Test labels 
target = pd.read_csv('../data/UniqueBlocks/targetFull.csv', index_col=0)
targetAll = pd.read_csv('../data/UniqueBlocks/targetAll.csv', index_col=0)
test_index = pd.read_csv('../data/UniqueBlocks/test_subjects.csv')
test_index = test_index.iloc[:, 1]
y_test = np.array(targetAll.loc[test_index]).reshape(-1,)


# Predictions
predictions_df = pd.read_csv('../data/predictions/predictions.csv', index_col=0)


# =============================================================================
# Weighted average k-fold scores
# =============================================================================

# Weights based on k-fold scores
weights = [0.149, 0.455, 0.409, 0.338, 0.379]

# Without BlockA
# predictions_df = predictions_df.loc[:, ['BlockB', 'BlockC', 'BlockD', 'BlockE']]
# weights = [0.5351, 0.3986, 0.3483, 0.3771]

weighted_avg = []

for i in range(len(predictions_df.index)):
    values = predictions_df.loc[i , :]
    values_arr = np.array(values)
    avg = average(values.values, weights=weights)
    weighted_avg.append(avg)
    
weighted_predictions = []

for number in weighted_avg:
    if number > 0.3:
        weighted_predictions.append(1)
    else:
        weighted_predictions.append(0)
         
        
# =============================================================================
# Final evaluation
# =============================================================================
        
y_true = y_test.copy()
y_pred = np.array(weighted_predictions)

mcc_score = matthews_corrcoef(y_true, y_pred)
acc_score = accuracy_score(y_true, y_pred)
# f1_score = f1_score(y_true, y_pred)


# =============================================================================
# Define function for model evaluation
# =============================================================================

# evaluate a model with a given number of repeats
def evaluate_model(model, X, y, repeats, splits, metric):
 	
    # prepare the cross-validation procedure
 	cv = RepeatedStratifiedKFold(n_splits=splits, 
                                 n_repeats=repeats, 
                                 random_state=1)
 	
    # evaluate model
 	scores = cross_val_score(model, X, y, 
                             scoring=metric, 
                             cv=cv, 
                             n_jobs=-1,
                             verbose=0)
 	return scores



# =============================================================================
# Evaluating classifiers
# =============================================================================

# Creating scorer for Matthews correlation coefficient
mcc_scorer = make_scorer(matthews_corrcoef)
metric = mcc_scorer
scaler = StandardScaler()
power = PowerTransformer(method='yeo-johnson')


# =============================================================================
# Training classifiers on the new dataset
# =============================================================================

x = predictions_df.copy()
y = y_test.copy()

# Creating models
lg_model = LogisticRegression()

aggressive_model = PassiveAggressiveClassifier()

forest_model = RandomForestClassifier()

knn_model = KNeighborsClassifier()

svc_model = SVC()


# List of models and names
models = [lg_model, aggressive_model, forest_model, knn_model, svc_model]
modelnames = ['LogisticRegression', 'PassiveAggressiveClassifier', 
              'RandomForest', 'KNN', 'SVC']



# Defining parameters for grid search 
# -----------------------------------------------------------------------------

# Logistic Regression
param_range_lr  = [0.0001, 0.001, 0.01, 0.1, 1.0, 10.0, 100.0, 1000.0]
param_grid_lr   = [{'C' : param_range_lr, 
                    'solver' : ['lbfgs', 'liblinear']}]


# Passive Aggressive Classifier
param_range_pag   = [0.0001, 0.001, 0.01, 0.1, 1.0, 10.0, 100.0, 1000.0]
param_grid_pag   = [{'C' : param_range_pag,
                     'loss': ['hinge','squared_hinge']}]


# Random Forest
param_range_forest1   = [20, 40, 60, 80, 100, 120, 140, 160]
param_range_forest2   = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, None]
param_grid_forest  = [{'n_estimators' : param_range_forest1, 
                       'criterion' : ['gini','entropy'], 
                       'max_depth': param_range_forest2,
                       'max_features': ['auto','sqrt', 'log']}]



# KNN
param_range_knc = [2, 4, 5, 6, 7, 8, 9, 10]
param_grid_knc = [{'n_neighbors' : param_range_knc,
                   'weights' : ['uniform', 'distance'], 
                   'algorithm' : ['auto', 'ball_tree', 'kd_tree', 'brute'],
                   'p' : [1, 2]}]



# SVC
param_range_svc1 = [0.0001, 0.001, 0.01, 0.1, 1.0, 10.0, 100.0] 
param_range_svc2 = [0.0001, 0.0005, 0.001, 0.01, 0.05, 0.1, 1.0]         
param_grid_svc  = [{'C': param_range_svc1, 'kernel': ['linear']},
                   {'C': param_range_svc1, 'gamma': param_range_svc2, 
                    'kernel': ['rbf']}]



# List of the parameters for all the models
params = [param_grid_lr, param_grid_pag, param_grid_forest, 
          param_grid_knc, param_grid_svc]


# Grid search
# -----------------------------------------------------------------------------
# Dictionary for storing results for all the blocks
grid_search = {}

    
for model, modelname, param in zip(models, modelnames, params):
    
    # Grid search
    gs = GridSearchCV(estimator=model, 
                      param_grid=param, 
                      scoring=metric, 
                      cv=5,
                      n_jobs=-1)
    
    gs = gs.fit(x, y)
    best = gs.best_estimator_
    
    # Storing best scores
    grid_search[modelname] =  [gs.best_score_, gs.best_params_]
    
    
    
# Creating dataframe and exporting as csv file    
grid_search_df = pd.DataFrame(grid_search)
grid_search_df.to_csv('grid_search.csv')


# =============================================================================
# Repeated stratified k-fold with best parameters
# =============================================================================


# Dictionary for storing results for all the blocks
k_fold = {}


for model, modelname in zip(models, modelnames):
    
    
    # Fitting model
    model.fit(x, y)
    
    # Fitting model with best parameters
    best_parameters = grid_search[modelname][1]
    model.set_params(**best_parameters)
    
    # Repeated stratified k-fold
    rkf_scores = evaluate_model(model,
                                X=x, 
                                y=y, 
                                repeats=10,
                                splits=5,
                                metric=metric) 
    
    # Getting mean scores and storing in dictionary
    rkf_scores_mean = round(np.mean(rkf_scores),4)
    rkf_scores_std = round(np.std(rkf_scores, ddof=1),4)
    k_fold[modelname] = {'Mean score ' : rkf_scores_mean, 
                         'Std score' : rkf_scores_std}
    


# Creating dataframe and exporting as csv file  
k_fold_df = pd.DataFrame(k_fold)
k_fold_df.to_csv('k_fold.csv')



