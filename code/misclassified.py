#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 28 22:35:06 2021

@author: charlottolofsson
"""



# =============================================================================
# Modules
# =============================================================================

# Data handling and plotting
import pyreadstat
import pandas as pd
import numpy as np


# Functions from other scripts
import sys
sys.path.append('../code/functions')
from rent_function import rent


# =============================================================================
# Loading the data
# =============================================================================

# Block A
dataA = pd.read_csv('../data/blocks/dataA.csv', index_col=0)
dataA_dummy = pd.get_dummies(dataA)

# Block B
dataB = pd.read_csv('../data/blocks/dataB.csv', index_col=0)
dataB_dummy = pd.get_dummies(dataB)

# Block C
dataC = pd.read_csv('../data/blocks/dataC.csv', index_col=0)

# Block D
dataD = pd.read_csv('../data/blocks/dataD.csv', index_col=0)

# Block E
dataE = pd.read_csv('../data/blocks/dataE.csv', index_col=0)


# Target
target_name = 'a'
target = pd.read_csv('../data/blocks/targetFull.csv', index_col=0)

targetAll = pd.read_csv('../data/blocks/targetAll.csv', index_col=0)




# Separating train and test data
# -----------------------------------------------------------------------------

test_index = pd.read_csv('../data/blocks/test_subjects.csv')
test_index = test_index.iloc[:, 1]


# Training data

xA = dataA_dummy.drop(test_index)
yA = np.array(targetAll.loc[xA.index]).reshape(-1,)
yA_ = targetAll.loc[xA.index]

xB = dataB_dummy.drop(test_index)
yB = np.array(targetAll.loc[xB.index]).reshape(-1,)
yB_ = targetAll.loc[xB.index]

xC = dataC.drop(test_index)
yC = np.array(targetAll.loc[xC.index]).reshape(-1,)
yC_ = targetAll.loc[xC.index]

xD = dataD.drop(test_index)
yD = np.array(targetAll.loc[xD.index]).reshape(-1,)
yD_ = targetAll.loc[xD.index]

xE = dataE.drop(test_index)
yE = np.array(targetAll.loc[xE.index]).reshape(-1,)
yE_ = targetAll.loc[xE.index]



# =============================================================================
# Re-indexing for splitting
# =============================================================================


# Block B
xB['idx'] = xB.index                                               
new_indexB = [x for x in range(len(xB))]                            
xB = xB.set_index(pd.Index(new_indexB))                             
yB_ = yB_.set_index(pd.Index(new_indexB))                           
xB_ = xB.drop(['idx'], axis=1)  


# Block C
xC['idx'] = xC.index                                               
new_indexC = [x for x in range(len(xC))]                            
xC = xC.set_index(pd.Index(new_indexC))                             
yC_ = yC_.set_index(pd.Index(new_indexC))                           
xC_ = xC.drop(['idx'], axis=1)                                      


# Block D
xD['idx'] = xD.index                                               
new_indexD = [x for x in range(len(xD))]                            
xD = xD.set_index(pd.Index(new_indexD))                             
yD_ = yD_.set_index(pd.Index(new_indexD))                           
xD_ = xD.drop(['idx'], axis=1)      


# Block E
xE['idx'] = xE.index                                               
new_indexE = [x for x in range(len(xE))]                            
xE = xE.set_index(pd.Index(new_indexE))                             
yE_ = yE_.set_index(pd.Index(new_indexE))                           
xE_ = xE.drop(['idx'], axis=1)                                      



# =============================================================================
# Finding misclassified patients
# =============================================================================

"""
Reading the files containing the 40 different 'objects' from the RENT analysis.
Locating the columns that contain the % misclassification and creating
an average for all the objects. The objects that are classified incorrectly
more than 70 % across the different models are marked as misclassified. 
"""

objectsB = pd.read_csv('material/rent/BlockWise/V/RepeatedRent/b_objects.csv')
countB = objectsB.iloc[:, 4::4]
countB['sum'] = countB.sum(axis=1)
countB['avg'] = countB['sum']/30
misB = countB.loc[countB['avg'] > 70]


objectsC = pd.read_csv('material/rent/BlockWise/V/RepeatedRent/c_objects.csv')
countC= objectsC.iloc[:, 4::4]
countC['sum'] = countC.sum(axis=1)
countC['avg'] = countC['sum']/30
misC = countC.loc[countC['avg'] > 70]


objectsD = pd.read_csv('material/rent/BlockWise/V/RepeatedRent/d_objects.csv')
countD = objectsD.iloc[:, 4::4]
countD['sum'] = countD.sum(axis=1)
countD['avg'] = countD['sum']/30
misD = countD.loc[countD['avg'] > 70]


objectsE = pd.read_csv('material/rent/BlockWise/V/RepeatedRent/e_objects.csv')
countE = objectsE.iloc[:, 4::4]
countE['sum'] = countB.sum(axis=1)
countE['avg'] = countB['sum']/30
misE = countE.loc[countE['avg'] > 70]




"""
Making lists of the incorrectly classified patient using the new index for
splitting. Making separate dataframes from the incorrectly- and correctly
classified. 
"""

# Block B
icBD = xB.loc[misB.index, :]
ccBD = xB.drop(icBD.index)

icBD = icBD.set_index('idx')
icBD.to_csv('incorrectly_classified_b.csv')
ccBD = ccBD.set_index('idx')
ccBD.to_csv('correctly_classified_b.csv')


# Block C
icCD = xC.loc[misC.index, :]
ccCD = xC.drop(icCD.index)

icCD = icCD.set_index('idx')
icCD.to_csv('incorrectly_classified_c.csv')
ccCD = ccCD.set_index('idx')
ccCD.to_csv('correctly_classified_c.csv')


# Block D
icDD = xD.loc[misD.index, :]
ccDD = xD.drop(icDD.index)

icDD = icDD.set_index('idx')
icDD.to_csv('incorrectly_classified_d.csv')
ccDD = ccDD.set_index('idx')
ccDD.to_csv('correctly_classified_d.csv')


# Block E
icED = xE.loc[misE.index, :]
ccED = xE.drop(icED.index)

icED = icED.set_index('idx')
icED.to_csv('incorrectly_classified_e.csv')
ccED = ccED.set_index('idx')
ccED.to_csv('correctly_classified_e.csv')




# =============================================================================
# Matching index with original ID
# =============================================================================

# Reading the file 

data, meta = pyreadstat.read_sav('../data/NMBU_June_2021.sav', 
                                 encoding='latin1')
ID_df = data.loc[:, 'newID']


# Creating a mapping of index and patient ID

ind = list(ID_df.index)
newID = list(ID_df.values)
mapping = {}

for new_idx, original_idx in zip(ind, newID):
    mapping[new_idx] = original_idx
    
    
# Mapping the misclassified indicies to there original ID label. 
    
icBSeries = pd.Series(list(icBD.index))
icBID = icBSeries.map(mapping) 

icCSeries = pd.Series(list(icCD.index))
icCID = icCSeries.map(mapping)

icDSeries = pd.Series(list(icDD.index))
icDID = icDSeries.map(mapping)

icESeries = pd.Series(list(icED.index))
icEID = icESeries.map(mapping)


# Creating a dictionary with misclassified ID's for all the blocks

misclassified = {}

# misclassified_idx['Index'] = list(ID_df.index)
# misclassified_idx['ID'] = list(ID_df.values)
misclassified['BlockB'] = list(icBID.values)
misclassified['BlockC'] = list(icCID.values)
misclassified['BlockD'] = list(icDID.values)
misclassified['BlockE'] = list(icEID.values)

misclassified_df = pd.DataFrame.from_dict(misclassified, orient='index')
misclassified_df_T = misclassified_df.transpose()
misclassified_df_T.to_csv('misclassified.csv')

