#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 14 21:32:28 2021

@author: charlottolofsson
"""

# =============================================================================
# Modules
# =============================================================================

import pandas as pd
import pyreadstat
import missingno as msno


# =============================================================================
# Loading and preprocessing the data
# =============================================================================


data, meta = pyreadstat.read_sav('../data/NMBU_June_2021.sav', 
                                 encoding='latin1')
data_apoe, meta = pyreadstat.read_sav('../data/NMBU_June_2021_apoe.sav', 
                                      encoding='latin1')

# Framingham Risk Score
# -----------------------------------------------------------------------------
data_scores, meta = pyreadstat.read_sav('../data/FRS_DDI_211008_Charlott.sav')
data_syntax = pd.read_csv('../data/Syntax_FRS_normal_and_simple.sps')

# Relevant features
score_features = ['Smoking_currently', 'dm', 'frs_total', 
                  'frs_simp_total', 'frs_total_wo_age', 
                  'frs_simp_total_wo_age']
scores_red = data_scores.loc[:, score_features]


# Concatenating dataframes
# -----------------------------------------------------------------------------
data_new = pd.concat([data_apoe, scores_red], axis=1)
data = data_new.copy()



# =============================================================================
# Target
# =============================================================================

target = data.loc[:, 'a']
target = target.to_frame()
fig = msno.matrix(target)
fig_copy = fig.get_figure()
fig_copy.savefig('target_missing.png', bbox_inches='tight')

target = target.dropna(axis=0)


# =============================================================================
# Blocks
# =============================================================================

# Block A
blockA = pd.read_csv('../data/blocks/blockA.csv', index_col=0)
blockA_dummy = pd.get_dummies(blockA)
blockA_descr = blockA_dummy.describe()


# Block B
blockB = pd.read_csv('../data/blocks/blockB.csv', index_col=0)
blockB_dummy = pd.get_dummies(blockB)
blockB_descr = blockB_dummy.describe()


# Block C
blockC = pd.read_csv('../data/blocks/blockC.csv', index_col=0)
blockC_descr = blockC.describe()


# Block D
blockD = pd.read_csv('../data/blocks/blockD.csv', index_col=0)
blockD_descr = blockD.describe()


# Block E
blockE = pd.read_csv('../data/blocks/blockE.csv', index_col=0)
blockE_descr = blockE.describe()



# =============================================================================
# Visualising of missing values in the blocks
# =============================================================================


# Lists of data and names
BlockData = [blockA, blockB, blockC, blockD, blockE]
BlockName = ['BlockA', 'BlockB', 'BlockC', 'BlockD', 'BlockE']



# Matrix plot of missing values
for block, name in zip(BlockData, BlockName):
    fig = msno.matrix(block)
    fig_copy = fig.get_figure()
    fig_copy.savefig(name + '_missing.png', bbox_inches='tight')

    

# Heatmap of missing values    
for block, name in zip(BlockData, BlockName):
    fig = msno.heatmap(block)
    fig_copy = fig.get_figure()
    fig_copy.savefig(name + '_heatmap_missing.png', bbox_inches='tight')
    


# Bar plot of non-missing
figA = msno.bar(blockA, color='purple')
fig_copy = figA.get_figure()
fig_copy.savefig('BlockA_non-missing.png')


figB = msno.bar(blockB, color='cyan')
fig_copy = figB.get_figure()
fig_copy.savefig('BlockB_non-missing.png')


figC = msno.bar(blockC, color='blue')
fig_copy = figC.get_figure()
fig_copy.savefig('BlockC_non-missing.png')


figD = msno.bar(blockD, color='pink')
fig_copy = figD.get_figure()
fig_copy.savefig('BlockD_non-missing.png')


figE = msno.bar(blockE, color='blue')
fig_copy = figE.get_figure()
fig_copy.savefig('BlockE_non-missing.png')


# Removing missing and visualising again
for block, name in zip(BlockData, BlockName):
    newdata = block.dropna(axis=0)
    fig = msno.matrix(newdata)
    fig_copy = fig.get_figure()
    fig_copy.savefig(name + '_no_missing.png', bbox_inches='tight')
    



