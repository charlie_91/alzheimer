#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 16 10:11:42 2021

@author: charlottolofsson
"""

# =============================================================================
# Modules
# =============================================================================

# Data handling and plotting
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

# Preprocessing and models
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import PowerTransformer


# Functions from other scripts
import sys
sys.path.append('../code/functions')
from data_preparation import dataPrep


# =============================================================================
# Loading the data
# =============================================================================

# Different dimensions for the blocks
# -----------------------------------------------------------------------------
# Block A
dataA = pd.read_csv('../data/blocks/dataA.csv', index_col=0)
dataA_dummy = pd.get_dummies(dataA)
dataA_descr = dataA_dummy.describe()
dataA_dummy.plot.hist()


# Block B
dataB = pd.read_csv('../data/blocks/dataB.csv', index_col=0)
dataB_dummy = pd.get_dummies(dataB)
dataB_descr = dataB_dummy.describe()
dataB_dummy.plot.hist()


# Block C
dataC = pd.read_csv('../data/blocks/dataC.csv', index_col=0)
dataC_descr = dataC.describe()
dataC.plot.hist()


# Block D
dataD = pd.read_csv('../data/blocks/dataD.csv', index_col=0)
dataD_descr = dataD.describe()
dataD.plot.hist()


# Block E
dataE = pd.read_csv('../data/blocks/dataE.csv', index_col=0)
dataE_descr = dataE.describe()
dataE.plot.hist()


# List of the data
blockdata = [dataA_dummy, dataB_dummy, dataC, dataD, dataE]

# All blocks concatenated into one dataframe
dataAll = pd.concat([dataA_dummy, dataB_dummy, dataC, dataD, dataE], axis=1)



# Data with subjects with no missing values across all the blocks
# -----------------------------------------------------------------------------
# Block A
DataA = pd.read_csv('../data/blocks/DataA_.csv', index_col=0)
DataA_dummy = pd.get_dummies(DataA)
DataA_descr = dataA_dummy.describe()


# Block B
DataB = pd.read_csv('../data/blocks/DataB_.csv', index_col=0)
DataB_dummy = pd.get_dummies(DataB)
DataB_descr = DataB_dummy.describe()


# Block C
DataC = pd.read_csv('../data/blocks/DataC_.csv', index_col=0)
DataC_descr = DataC.describe()


# Block D
DataD = pd.read_csv('../data/blocks/DataD_.csv', index_col=0)
DataD_descr = DataD.describe()


# Block E
DataE = pd.read_csv('../data/blocks/DataE_.csv', index_col=0)
DataE_descr = DataE.describe()



# All blocks concatenated into one dataframe
DataAll = pd.concat([DataA_dummy, DataB_dummy, DataC, DataD, DataE], axis=1)

# Target
target_name = 'a'
target = pd.read_csv('../data/blocks/targetFull.csv', index_col=0)



# =============================================================================
# Histogram and density plot
# =============================================================================


# Looping through all the features and creating histrogram visualisation
for block in blockdata:
    for feature in block.columns: 
        x = block.index
        y = block[feature]
        plt.hist(y, bins=100)
        plt.gca().set(title=feature, ylabel='Frequency')
        plt.show()
        
        
        
    
# Creating figures for the features with interesting plots 
# -----------------------------------------------------------------------------
sns.set_style('white')
kwargs = dict(hist_kws={'alpha':.6}, kde_kws={'linewidth':2})



# bp_recum_sys
fig = plt.figure(figsize=(7,5), dpi= 80)
sns.distplot(dataAll.loc[:, 'bp_recum_sys'], color='orange', 
             label='bp_recum_sys', **kwargs)
plt.xlabel('')
plt.legend(fontsize=13)
plt.grid()
fig.savefig('hist_bp.png')



# tmtb sec
fig = plt.figure(figsize=(7,5), dpi= 80)
sns.distplot(dataAll.loc[:, 'tmtb_sec'], color='orange', 
             label='tmtb_sec', **kwargs)
sns.distplot(dataAll.loc[:, 'tmta_sec'], color='blue', 
             label='tmta_sec', **kwargs)
plt.xlabel('')
plt.legend(fontsize=13)
plt.grid()
fig.savefig('hist_tmta_b.png')



# Les P
fig = plt.figure(figsize=(7,5), dpi= 80)
sns.distplot(dataAll.loc[:, 'LesP'], color='orange', 
             label='LesP', **kwargs)
plt.xlabel('')
plt.legend(fontsize=13)
plt.grid()
fig.savefig('hist_LesP.png')



# LesO
fig = plt.figure(figsize=(7,5), dpi= 80)
sns.distplot(dataAll.loc[:, 'LesO'], color='purple', 
             label='LesO', **kwargs)
plt.xlabel('')
plt.legend(fontsize=13)
plt.grid()
fig.savefig('hist_LesO.png')



# LesF
fig = plt.figure(figsize=(7,5), dpi= 80)
sns.distplot(dataAll.loc[:, 'LesF'], color='blue', 
             label='LesF', **kwargs)
plt.xlabel('')
plt.legend(fontsize=13)
plt.grid()
fig.savefig('hist_LesF.png')


# LesT
fig = plt.figure(figsize=(7,5), dpi= 80)
sns.distplot(dataAll.loc[:, 'LesT'], color='deeppink', 
             label='LesT', **kwargs)
plt.xlabel('')
plt.legend(fontsize=13)
plt.grid()
fig.savefig('hist_LesT.png')


# Les 
fig = plt.figure(figsize=(7,5), dpi= 80)
sns.distplot(dataAll.loc[:, 'LesP'], color='orange', 
             label='LesP', **kwargs)
sns.distplot(dataAll.loc[:, 'LesO'], color='purple', 
             label='LesO', **kwargs)
sns.distplot(dataAll.loc[:, 'LesF'], color='blue', 
             label='LesF', **kwargs)
sns.distplot(dataAll.loc[:, 'LesT'], color='deeppink', 
             label='LesT', **kwargs)
plt.xlabel('')
plt.legend(fontsize=13)
plt.grid()
fig.savefig('hist_Les.png')



# =============================================================================
# Histogram after transformation
# =============================================================================

# Scaler and power transformer
scaler = StandardScaler()
power = PowerTransformer(method='yeo-johnson')

lesP = pd.DataFrame(dataAll.loc[:, 'LesP'])
lesPsc = scaler.fit_transform(lesP)
lesPYeo = power.fit_transform(lesPsc)


# Les P
fig = plt.figure(figsize=(7,5), dpi= 80)
sns.distplot(lesPYeo, color='orange', 
             label='LesP', **kwargs)
plt.xlabel('')
plt.legend(fontsize=13)
plt.grid()
fig.savefig('hist_LesPYeo.png')


# =============================================================================
# Scatterplot
# =============================================================================

# Looping through all the features and creating scatterplots
for block in blockdata:
    for feature in block.columns:
        x = [x for x in range(len(block[feature]))]
        y = block[feature]
        plt.scatter(x, y)
        plt.gca().set(title=feature)
        plt.show()


# Creating figures for the features with interesting plots 
# -----------------------------------------------------------------------------
# bl_apoe_E2/E2
x = [x for x in range(len(dataAll['bl_apoe_E2/E2']))]
y = dataAll['bl_apoe_E2/E2']

fig, ax = plt.subplots(1, 1, tight_layout=True)
ax.scatter(x, y, color='purple', label='bl_apoe_E2/E2')
ax.set_ylabel('Value of measurement', fontsize=13)
ax.set_title('')
ax.yaxis.set_tick_params(labelsize=13)
plt.legend(fontsize=16, loc='upper right')
plt.xticks([])
plt.grid()
fig.savefig('scatter_bl_apoe_E2_E2.png')


# bp_recum_sys
x = [x for x in range(len(dataAll['bp_recum_sys']))]
y = dataAll['bp_recum_sys']

fig, ax = plt.subplots(1, 1, tight_layout=True)
ax.scatter(x, y, color='blue', label='bp_recum_sys')
ax.set_ylabel('Value of measurement', fontsize=13)
ax.set_title('')
ax.yaxis.set_tick_params(labelsize=13)
plt.legend(fontsize=16, loc='upper left')
plt.xticks([])
plt.grid()
fig.savefig('scatter_bp_recum_sys.png')

