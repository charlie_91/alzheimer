#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 16 18:02:00 2021

@author: charlottolofsson
"""


# =============================================================================
# Modules
# =============================================================================

# Data handling and plotting
import pandas as pd
import numpy as np

# Preprocessing and models
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import PowerTransformer
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.model_selection import cross_val_score
from RENT import RENT, stability


# Functions from other scripts
import sys
sys.path.append('../code/functions')
from params_mcc import paramsMcc


# =============================================================================
# Loading the data
# =============================================================================

"""
Loading the data and creating new dataframes with the features selected
from RENT. 
"""


# Block A
dataA = pd.read_csv('../data/blocks/dataA.csv', index_col=0)
dataA_dummy = pd.get_dummies(dataA)

# Block B
dataB = pd.read_csv('../data/blocks/dataB.csv', index_col=0)
dataB_dummy = pd.get_dummies(dataB)
B_features = ['age', 'bl_apoe_E4/E4', 'bl_apoe_E3/E4', 'bl_apoe_E2/E4',
              'smok', 'bl_apoe_E3/E4', 'degree1_dem', 'cardiovascular_disease',
              'edu_years', 'bp_recum_sys', 'edu_level', 'gender_female',
              'gender_male']
DataB = dataB_dummy.loc[:, B_features]

# Block C
dataC = pd.read_csv('../data/blocks/dataC.csv', index_col=0)
C_features = ['mmse_total', 'cerad_recall', 'cowat_tscore', 'tmtb_sec']
DataC = dataC.loc[:, C_features]

"""
# Block D
dataD = pd.read_csv('../data/blocks/dataD.csv', index_col=0)
D_features = ['PSMD', 'WMHo_rV', 'LesO', 'LesF', 'LesT', 'LesP']
DataD = dataD.loc[:, D_features]
"""

# Block D
# Combined Les features
dataD = pd.read_csv('../data/combined_Les/dataD.csv', index_col=0)
D_features = ['PSMD', 'WMHo_rV', 'LesO', 'LesF', 'LesT', 'LesP']
DataD = dataD.loc[:, D_features]


# Block E
dataE = pd.read_csv('../data/blocks/dataE.csv', index_col=0)
E_features = ['left_ERC.combat_stag', 'right_PHC.combat_stag', 
              'left_Anterior_hippocampus.combat_stag', 
              'right_Posterior_hippocampus.combat_stag',
              'right_MISC.combat_stag', 'left_Meninges_PHC.combat_stag',
              'left_Br36.combat_stag', 'left_ColSul.combat_stag',
              'right_Br36.combat_stag', 'right_Meninges_PHC.combat_stag',
              'right_Meninges.combat_stag','left_Br35.combat_stag',
              'left_MISC.combat_stag', 'right_ERC.combat_stag', 
              'left_Meninges.combat_stag', 'left_PHC.combat_stag',
              'right_ColSul.combat_stag', 
              'left_Posterior_hippocampus.combat_stag']
DataE = dataE.loc[:, E_features]

# Block F
dataF = pd.read_csv('../data/blocks/dataF.csv', index_col=0)
F_features = ['left_ERC.combat_ATN', 'right_PHC.combat_ATN', 
              'right_MISC.combat_ATN', 
              'right_Posterior_hippocampus.combat_ATN',
              'left_ColSul.combat_ATN', 'left_MISC.combat_ATN',
              'left_Br36.combat_ATN', 'right_Br35.combat_ATN',
              'right_ERC.combat_ATN', 
              'left_Posterior_hippocampus.combat_ATN', 
              'left_PHC.combat_ATN', 'left_Br36.combat_ATN',
              'left_Meninges_PHC.combat_ATN', 'right_ColSul.combat_ATN',
              'left_Anterior_hippocampus.combat_ATN']
DataF = dataF.loc[:, F_features]


# Target
target_name = 'a'
target = pd.read_csv('../data/blocks/targetFull.csv', index_col=0)

targetAll = pd.read_csv('../data/blocks/targetAll.csv', index_col=0)




# =============================================================================
# Standardising data separately
# =============================================================================

"""
Standardising separately in order to only transform data in block D.
"""

sc = StandardScaler()
minmax = MinMaxScaler(feature_range=(1,2))


dataAstd = sc.fit_transform(dataA_dummy)
dataAsc = pd.DataFrame(dataAstd, columns=dataA_dummy.columns, 
                       index=dataA_dummy.index)


dataBstd = sc.fit_transform(DataB)
dataBsc = pd.DataFrame(dataBstd, columns=DataB.columns, 
                       index=DataB.index)


dataCstd = sc.fit_transform(DataC)
dataCsc = pd.DataFrame(dataCstd, columns=DataC.columns, 
                       index=DataC.index)


dataDstd = sc.fit_transform(DataD)
dataDsc = pd.DataFrame(dataDstd, columns=DataD.columns, 
                       index=DataD.index)

dataDminmax = minmax.fit_transform(DataD)
dataDmm = pd.DataFrame(dataDminmax, columns=DataD.columns, 
                       index=DataD.index)


dataEstd = sc.fit_transform(DataE)
dataEsc = pd.DataFrame(dataEstd, columns=DataE.columns, 
                       index=DataE.index)


dataFstd = sc.fit_transform(DataF)
dataFsc = pd.DataFrame(dataFstd, columns=DataF.columns, 
                       index=DataF.index)



# =============================================================================
# Power transformation on block D
# =============================================================================

# Yeo Johnson transformation 
pt_yeo = PowerTransformer(method='yeo-johnson')
dataD_yeo = pt_yeo.fit_transform(dataDstd)
dataDYeo = pd.DataFrame(dataD_yeo, columns=DataD.columns, index=DataD.index)


# BoxCox transformation
pt_box = PowerTransformer(method='box-cox')
dataD_box = pt_box.fit_transform(dataDmm)
dataDBox = pd.DataFrame(dataD_box, columns=DataD.columns, index=DataD.index)




# Separating train and test data
# -----------------------------------------------------------------------------

test_index = pd.read_csv('../data/blocks/test_subjects.csv')
test_index = test_index.iloc[:, 1]


# Training data

xA = dataAsc.drop(test_index)
yA = np.array(targetAll.loc[xA.index]).reshape(-1,)
yA_ = targetAll.loc[xA.index]

xB = dataBsc.drop(test_index)
yB = np.array(targetAll.loc[xB.index]).reshape(-1,)
yB_ = targetAll.loc[xB.index]

xC = dataCsc.drop(test_index)
yC = np.array(targetAll.loc[xC.index]).reshape(-1,)
yC_ = targetAll.loc[xC.index]

xD = dataDYeo.drop(test_index)
yD = np.array(targetAll.loc[xD.index]).reshape(-1,)
yD_ = targetAll.loc[xD.index]

xE = dataEsc.drop(test_index)
yE = np.array(targetAll.loc[xE.index]).reshape(-1,)
yE_ = targetAll.loc[xE.index]

xF = dataFsc.drop(test_index)
yF = np.array(targetAll.loc[xF.index]).reshape(-1,)
yF_ = targetAll.loc[xF.index]

       


"""
# Test data 

xA_test = dataAsc.loc[test_index]
yA_test = np.array(targetAll.loc[xA_test.index]).reshape(-1,)

xB_test = dataBsc.loc[test_index]
yB_test = np.array(targetAll.loc[xB_test.index]).reshape(-1,)

xC_test = dataCsc.loc[test_index]
yC_test = np.array(targetAll.loc[xC_test.index]).reshape(-1,)

xD_test = dataDsc.loc[test_index]
yD_test = np.array(targetAll.loc[xD_test.index]).reshape(-1,)

xE_test = dataEsc.loc[test_index]
yE_test = np.array(targetAll.loc[xE_test.index]).reshape(-1,)

xF_test = dataFsc.loc[test_index]
yF_test = np.array(targetAll.loc[xF_test.index]).reshape(-1,)

"""

# Lists of the data and names
#Xtrain = [xA, xB, xC, xD, xE, xF]
Xtrain = [xD]

#Ytrain = [yA, yB, yC, yD, yE, yF]
Ytrain = [yD]

# Xtest = [xA_test, xB_test, xC_test, xD_test, 
#          xE_test, xF_test, xE2_test, xF2_test]

# Ytest = [yA_test, yB_test, yC_test, yD_test, 
#          yE_test, yF_test, yE2_test, yF2_test]

#block_names = ['BlockA', 'BlockB', 'BlockC', 
#               'BlockD', 'BlockE', 'BlockF']
block_names = ['Block D']




# =============================================================================
# Define function for model evaluation
# =============================================================================

# evaluate a model with a given number of repeats
def evaluate_model(model, X, y, repeats, splits, metric):
 	
    # prepare the cross-validation procedure
 	cv = RepeatedStratifiedKFold(n_splits=splits, 
                                 n_repeats=repeats, 
                                 random_state=1)
 	
    # evaluate model
 	scores = cross_val_score(model, X, y, 
                             scoring=metric, 
                             cv=cv, 
                             n_jobs=-1,
                             verbose=0)
 	return scores



# =============================================================================
# Evaluating classifiers
# =============================================================================



# List of model names
modelnames = ['Logistic Regression', 'Passive Aggressive Classifier', 
              'Random Forest', 'KNN']
# modelnames = ['SVC']



# Defining range of hyperparameters

# Logistic Regression
c_range = np.arange(-8, 11)
param_dict_lr = {'C' : c_range}


# Passive Aggressive Classifier
param_dict_pag = {'C' : c_range}


# Random Forest
estimators_range = np.arange(10, 320, 20)
param_dict_rf = {'n_estimators' : estimators_range} 
# param_dict_rf = {'m__n_estimators' : estimators_range} 


# KNN
neighbors_range = np.arange(2, 25, 1)
param_dict_knn = {'n_neighbors' : neighbors_range}


# SVC
c_range = np.arange(-4,  5)
param_dict_svc = {'c' : c_range}



# List of the parameters for all the models
params = [param_dict_lr, param_dict_pag, param_dict_rf, 
          param_dict_knn, param_dict_svc]
#params = [param_dict_svc]




# =============================================================================
# MCC scores as function of hyperparameter
# =============================================================================


# Dictionary for storing results for all the blocks
mcc = {}


for x, y, name in zip(Xtrain, Ytrain, block_names):
    print(name)
    
    # Dictionary for storing results for all the models
    scores = {}
    
    for modelname, param in zip(modelnames, params):
        print(modelname)
        
        # Getting scores for model
        scores[modelname] =  paramsMcc(x, y, name, modelname, param)

    
    mcc[name] = scores
    
    
    
# Creating dataframe and exporting as csv file    
mcc_df = pd.DataFrame(mcc)
mcc_df.to_csv('mcc.csv')



