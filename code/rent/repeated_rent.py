#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 26 12:45:12 2021

@author: charlottolofsson
"""


# =============================================================================
# Modules
# =============================================================================

# Data handling and plotting
import pandas as pd
import numpy as np

# Preprocessing and models
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import PowerTransformer
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.model_selection import cross_val_score
from sklearn.linear_model import LogisticRegression
from RENT import RENT, stability

# Scoring
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import matthews_corrcoef

# Functions from other scripts
import sys
sys.path.append('../code/functions')
from rent_function import rent


# =============================================================================
# Loading the data
# =============================================================================

# Block A
dataA = pd.read_csv('../data/blocks/dataA.csv', index_col=0)
dataA_dummy = pd.get_dummies(dataA)

# Block B
dataB = pd.read_csv('../data/blocks/dataB.csv', index_col=0)
dataB_dummy = pd.get_dummies(dataB)

"""
# Block B 
# New bl_apoe measurements
dataB = pd.read_csv('../data/blocks/dataB_apoe.csv', index_col=0)
dataB_dummy = pd.get_dummies(dataB)
"""

# Block C
dataC = pd.read_csv('../data/blocks/dataC.csv', index_col=0)

# Block D
dataD = pd.read_csv('../data/blocks/dataD.csv', index_col=0)

# Block D
# Combined Les features
dataD = pd.read_csv('../data/combined_Les/dataD.csv', index_col=0)

# Block E
dataE = pd.read_csv('../data/blocks/dataE.csv', index_col=0)

# Block F
dataF = pd.read_csv('../data/blocks/dataF.csv', index_col=0)



# Target
target_name = 'a'
target = pd.read_csv('../data/blocks/targetFull.csv', index_col=0)

targetAll = pd.read_csv('../data/blocks/targetAll.csv', index_col=0)


# =============================================================================
# Standardising data separately
# =============================================================================

"""
Standardising separately in order to only transform data in block D.
"""

sc = StandardScaler()
minmax = MinMaxScaler(feature_range=(1,2))


dataAstd = sc.fit_transform(dataA_dummy)
dataAsc = pd.DataFrame(dataAstd, columns=dataA_dummy.columns, 
                       index=dataA_dummy.index)


dataBstd = sc.fit_transform(dataB_dummy)
dataBsc = pd.DataFrame(dataBstd, columns=dataB_dummy.columns, 
                       index=dataB_dummy.index)


dataCstd = sc.fit_transform(dataC)
dataCsc = pd.DataFrame(dataCstd, columns=dataC.columns, 
                       index=dataC.index)


dataDstd = sc.fit_transform(dataD)
dataDsc = pd.DataFrame(dataDstd, columns=dataD.columns, 
                       index=dataD.index)

dataDminmax = minmax.fit_transform(dataD)
dataDmm = pd.DataFrame(dataDminmax, columns=dataD.columns, 
                       index=dataD.index)


dataEstd = sc.fit_transform(dataE)
dataEsc = pd.DataFrame(dataEstd, columns=dataE.columns, 
                       index=dataE.index)


dataFstd = sc.fit_transform(dataF)
dataFsc = pd.DataFrame(dataFstd, columns=dataF.columns, 
                       index=dataF.index)




# =============================================================================
# Power transformation on block D
# =============================================================================

# Yeo Johnson transformation 
pt_yeo = PowerTransformer(method='yeo-johnson')
dataD_yeo = pt_yeo.fit_transform(dataDstd)
dataDYeo = pd.DataFrame(dataD_yeo, columns=dataD.columns, index=dataD.index)


# BoxCox transformation
pt_box = PowerTransformer(method='box-cox')
dataD_box = pt_box.fit_transform(dataDmm)
dataDBox = pd.DataFrame(dataD_box, columns=dataD.columns, index=dataD.index)




# Separating train and test data
# -----------------------------------------------------------------------------

test_index = pd.read_csv('../data/blocks/test_subjects.csv')
test_index = test_index.iloc[:, 1]


# Training data

xA = dataAsc.drop(test_index)
yA = np.array(targetAll.loc[xA.index]).reshape(-1,)
yA_ = targetAll.loc[xA.index]

xB = dataBsc.drop(test_index)
yB = np.array(targetAll.loc[xB.index]).reshape(-1,)
yB_ = targetAll.loc[xB.index]

xC = dataCsc.drop(test_index)
yC = np.array(targetAll.loc[xC.index]).reshape(-1,)
yC_ = targetAll.loc[xC.index]

xD = dataDsc.drop(test_index)
yD = np.array(targetAll.loc[xD.index]).reshape(-1,)
yD_ = targetAll.loc[xD.index]

xE = dataEsc.drop(test_index)
yE = np.array(targetAll.loc[xE.index]).reshape(-1,)
yE_ = targetAll.loc[xE.index]

xF = dataFsc.drop(test_index)
yF = np.array(targetAll.loc[xF.index]).reshape(-1,)
yF_ = targetAll.loc[xF.index]



# Test data 

xA_test = dataAsc.loc[test_index]
yA_test = np.array(targetAll.loc[xA_test.index]).reshape(-1,)

xB_test = dataBsc.loc[test_index]
yB_test = np.array(targetAll.loc[xB_test.index]).reshape(-1,)

xC_test = dataCsc.loc[test_index]
yC_test = np.array(targetAll.loc[xC_test.index]).reshape(-1,)

xD_test = dataDsc.loc[test_index]
yD_test = np.array(targetAll.loc[xD_test.index]).reshape(-1,)

xE_test = dataEsc.loc[test_index]
yE_test = np.array(targetAll.loc[xE_test.index]).reshape(-1,)

xF_test = dataFsc.loc[test_index]
yF_test = np.array(targetAll.loc[xF_test.index]).reshape(-1,)




# Lists of the data and names
Xtrain = [xA, xB, xC, xD, xE, xF]

Ytrain = [yA, yB, yC, yD, yE, yF]

Xtest = [xA_test, xB_test, xC_test, 
         xD_test, xE_test, xF_test]

Ytest = [yA_test, yB_test, yC_test,
         yD_test, yE_test, yF_test]

block_names = ['BlockA', 'BlockB', 'BlockC', 
               'BlockD', 'BlockE', 'BlockF']



# =============================================================================
# Define function for model evaluation
# =============================================================================

# Evaluate a model with a given number of repeats
def evaluate_model(model, X, y, repeats, splits, metric):
 	
    # Prepare the cross-validation procedure
 	cv = RepeatedStratifiedKFold(n_splits=splits, 
                                 n_repeats=repeats, 
                                 random_state=1)
 	
    # Evaluate model
 	scores = cross_val_score(model, X, y, 
                             scoring=metric, 
                             cv=cv, 
                             n_jobs=-1,
                             verbose=0)
 	return scores
 
    
    
# =============================================================================
# Re-indexing for splitting
# =============================================================================


# Block B
xB['idx'] = xB.index                                               
new_indexB = [x for x in range(len(xB))]                            
xB = xB.set_index(pd.Index(new_indexB))                             
yB_ = yB_.set_index(pd.Index(new_indexB))                           
xB_ = xB.drop(['idx'], axis=1)  


# Block C
xC['idx'] = xC.index                                               
new_indexC = [x for x in range(len(xC))]                            
xC = xC.set_index(pd.Index(new_indexC))                             
yC_ = yC_.set_index(pd.Index(new_indexC))                           
xC_ = xC.drop(['idx'], axis=1)                                      


# Block D
xD['idx'] = xD.index                                               
new_indexD = [x for x in range(len(xD))]                            
xD = xD.set_index(pd.Index(new_indexD))                             
yD_ = yD_.set_index(pd.Index(new_indexD))                           
xD_ = xD.drop(['idx'], axis=1)      


# Block E
xE['idx'] = xE.index                                               
new_indexE = [x for x in range(len(xE))]                            
xE = xE.set_index(pd.Index(new_indexE))                             
yE_ = yE_.set_index(pd.Index(new_indexE))                           
xE_ = xE.drop(['idx'], axis=1)                                      


# Block F
xF['idx'] = xF.index                                               
new_indexF = [x for x in range(len(xF))]                            
xF = xF.set_index(pd.Index(new_indexF))                             
yF_ = yF_.set_index(pd.Index(new_indexF))                           
xF_ = xF.drop(['idx'], axis=1)                                      
                                
                                    


# =============================================================================
# Repeated stratified k-fold    
# =============================================================================

"""
Running repeated stratified k-fold with four splits and ten repeats, creating
40 RENT models, for more robust evaluation of the features selected by RENT. 

Variables need to be changed for evaluation of the different blocks. 
This includes the data and the parameters C, l1-ratio and tau. 
"""


# Define a range of regularisation parameters C for elastic net. 
# C_params = [0.01, 0.1, 1, 10]
C_params = [0.1]                                                     # Change!

# Define a reange of l1-ratios for elastic net.  
# l1_ratios = [0, 0.1, 0.25, 0.5, 0.75, 0.9, 1]
l1_ratios = [0.1]                                                    # Change!


rent_features = {}
rent_feature_names = {}
rent_objects = {}
rent_scores = {}


# Function for training the model
def train_model(x_train, y_train, x_test, y_test, fold_no):
    
    modelRent = RENT.RENT_Classification(data=x_train, 
                                     target=y_train, 
                                     feat_names=x_train.columns, 
                                     C=C_params, 
                                     l1_ratios=l1_ratios,
                                     autoEnetParSel=False,
                                     poly='OFF',
                                     testsize_range=(0.25,0.25),
                                     scoring='mcc',
                                     classifier='logreg',
                                     K=500,
                                     random_state=0,
                                     verbose=1)
    
    # Training the model
    modelRent.train()
    
    # Getting best parameters
    # params = modelRent.get_enet_params()
    
    # Selected features
    selected_features = modelRent.select_features(tau_1_cutoff=0.75,  # Change!
                                                  tau_2_cutoff=0.75,  # Change!
                                                  tau_3_cutoff=0.975)
    
    # Summary
    # summary = modelRent.get_summary_criteria()
    summary_objects = modelRent.get_summary_objects()
    
    rent_objects[fold_no] = summary_objects
    
    # Identify feature names
    feature_names = x_train.columns[selected_features]
    
    rent_features[fold_no] = selected_features
    rent_feature_names[fold_no] = feature_names

    
    
    #model.fit(x_train, y_train)
    #predictions = model.predict(x_test)
    print('Fold', str(fold_no),'Features:', feature_names)
    
    
    
    # Creating train and test data with selected features
    train_data = x_train.loc[:, feature_names]
    test_data = x_test.loc[:, feature_names]
    
    # Train prediction model 
    prediction_model = LogisticRegression(penalty='none', 
                                           max_iter=8000, 
                                           solver='saga', 
                                           random_state=0).fit(train_data, y_train)
    
    # Predicting and evaluating with different scores        
    pred = prediction_model.predict(test_data)
    
    mcc_score = matthews_corrcoef(y_test, pred)
    
    f1score = f1_score(y_test, pred)
    
    f1score_neg = f1_score(y_test, pred, pos_label=0)
    
    acc_acore = accuracy_score(y_test, pred)
    
    # Creating dictionary with scores
    scores = {'MCC score' : round(mcc_score,4), 
              'f1 score positive class' : round(f1score,4),
              'f1 score negative class': round(f1score_neg,4),
              'accuracy' : round(acc_acore,4)}
    
    
    
    rent_scores[fold_no] = scores
    

# Repeated stratified k-fold.
rskf = RepeatedStratifiedKFold(n_splits=4, n_repeats=10, random_state=0) 

# Data to use
x = xD_.copy()                                                       # Change!
y = yD_.copy()                                                       # Change!


# Function creating the data and training the model
fold_no = 1
for train_index, test_index in rskf.split(x, y):                 
    x_train = x.loc[train_index, :]                                
    y_train = y.loc[train_index, :]                                
    y_train = np.array(y_train).reshape(-1,)
    x_test = x.loc[test_index, :]                                  
    y_test = y.loc[test_index, :]                                  
    y_test = np.array(y_test).reshape(-1,)
    
    train_model(x_train, y_train, x_test, y_test, fold_no)
    fold_no += 1
    
    
    
# Creating dataframe of feature names and exporting as csv file      
rent_feature_names_df = pd.DataFrame.from_dict(rent_feature_names, 
                                               orient='index')
rent_feature_names_df.to_csv('d_repeated_rent.csv')                   # Change!


# Creating dataframe of scores and exporting as csv file      
rent_scores_df = pd.DataFrame.from_dict(rent_scores, orient='index')
rent_scores_df.to_csv('d_scores_rent.csv')                            # Change!


# Counting number of times a feature is selected
counts = {}
size = len(rent_feature_names_df.columns)

for column in range(size):
    count = dict(rent_feature_names_df[column].value_counts())
    counts[column] = count
    
# Creating dataframe
counts_df = pd.DataFrame.from_dict(counts)
counts_df.to_csv('d_feature_counts.csv')                              # Change!



    
# =============================================================================
# Objects
# =============================================================================

# Creating combined dataframe of objects from each split. 

df1 = rent_objects[1]
df2 = rent_objects[2]
df3 = rent_objects[3]
df4 = rent_objects[4]
df5 = rent_objects[5]
df6 = rent_objects[6]
df7 = rent_objects[7]
df8 = rent_objects[8]
df9 = rent_objects[9]
df10 = rent_objects[10]

df11 = rent_objects[11]
df12 = rent_objects[12]
df13 = rent_objects[13]
df14 = rent_objects[14]
df15 = rent_objects[15]
df16 = rent_objects[16]
df17 = rent_objects[17]
df18 = rent_objects[18]
df19 = rent_objects[19]
df20 = rent_objects[20]

df21 = rent_objects[21]
df22 = rent_objects[22]
df23 = rent_objects[23]
df24 = rent_objects[24]
df25 = rent_objects[25]
df26 = rent_objects[26]
df27 = rent_objects[27]
df28 = rent_objects[28]
df29 = rent_objects[29]
df30 = rent_objects[30]

df31 = rent_objects[31]
df32 = rent_objects[32]
df33 = rent_objects[33]
df34 = rent_objects[34]
df35 = rent_objects[35]
df36 = rent_objects[36]
df37 = rent_objects[37]
df38 = rent_objects[38]
df39 = rent_objects[39]
df40 = rent_objects[40]


df_tot = pd.concat([df1, df2, df3, df4, df5, df6, df7, df8, df9, df10,
                    df11, df12, df13, df14, df15, df16, df17, df18, df19,
                    df20, df21, df22, df23, df24, df25, df26, df27, df28,
                    df29, df30, df31, df32, df33, df34, df35, df36, df37,
                    df38, df39, df40], axis=1)

df_tot.to_csv('d_objects.csv')                                       # Change!



    