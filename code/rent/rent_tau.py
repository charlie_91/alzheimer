#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  1 13:37:21 2021

@author: charlottolofsson
"""


# =============================================================================
# Modules
# =============================================================================

# Data handling and plotting
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# Preprocessing and models
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import PowerTransformer
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.model_selection import cross_val_score
from sklearn.pipeline import Pipeline
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import PassiveAggressiveClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from RENT import RENT, stability

# Scoring
from sklearn.metrics import matthews_corrcoef
from sklearn.metrics import make_scorer

# Functions from other scripts
import sys
sys.path.append('../code/functions')
from rent_function import rent


# =============================================================================
# Loading the data
# =============================================================================

# Block A
dataA = pd.read_csv('../data/blocks/dataA.csv', index_col=0)
dataA_dummy = pd.get_dummies(dataA)
dataA_descr = dataA_dummy.describe()

# Block B
dataB = pd.read_csv('../data/blocks/dataB.csv', index_col=0)
dataB_dummy = pd.get_dummies(dataB)
dataB_descr = dataB_dummy.describe()

# Block C
dataC = pd.read_csv('../data/blocks/dataC.csv', index_col=0)
dataC_descr = dataC.describe()

# Block D
dataD = pd.read_csv('../data/combined_Les/dataD.csv', index_col=0)
dataD_descr = dataD.describe()

# Block E
dataE = pd.read_csv('../data/blocks/dataE.csv', index_col=0)
dataE_descr = dataE.describe()

# Block F
dataF = pd.read_csv('../data/blocks/dataF.csv', index_col=0)
dataF_descr = dataF.describe()

# Block E2
dataE2 = pd.read_csv('../data/blocks/dataE2.csv', index_col=0)
dataE2_descr = dataE2.describe()

# Block F
dataF2 = pd.read_csv('../data/blocks/dataF2.csv', index_col=0)
dataF2_descr = dataF2.describe()

# Target
target_name = 'a'
target = pd.read_csv('../data/blocks/targetFull.csv', index_col=0)

targetAll = pd.read_csv('../data/blocks/targetAll.csv', index_col=0)




# =============================================================================
# Standardising data separately
# =============================================================================

"""
Standardising separately in order to only transform data in block D.
"""

sc = StandardScaler()
minmax = MinMaxScaler(feature_range=(1,2))


dataAstd = sc.fit_transform(dataA_dummy)
dataAsc = pd.DataFrame(dataAstd, columns=dataA_dummy.columns, 
                       index=dataA_dummy.index)


dataBstd = sc.fit_transform(dataB_dummy)
dataBsc = pd.DataFrame(dataBstd, columns=dataB_dummy.columns, 
                       index=dataB_dummy.index)


dataCstd = sc.fit_transform(dataC)
dataCsc = pd.DataFrame(dataCstd, columns=dataC.columns, 
                       index=dataC.index)


dataDstd = sc.fit_transform(dataD)
dataDsc = pd.DataFrame(dataDstd, columns=dataD.columns, 
                       index=dataD.index)

dataDminmax = minmax.fit_transform(dataD)
dataDmm = pd.DataFrame(dataDminmax, columns=dataD.columns, 
                       index=dataD.index)


dataEstd = sc.fit_transform(dataE)
dataEsc = pd.DataFrame(dataEstd, columns=dataE.columns, 
                       index=dataE.index)


dataFstd = sc.fit_transform(dataF)
dataFsc = pd.DataFrame(dataFstd, columns=dataF.columns, 
                       index=dataF.index)


dataE2std = sc.fit_transform(dataE2)
dataE2sc = pd.DataFrame(dataE2std, columns=dataE2.columns, 
                        index=dataE2.index)


dataF2std = sc.fit_transform(dataF2)
dataF2sc = pd.DataFrame(dataF2std, columns=dataF2.columns, 
                        index=dataF2.index)




# =============================================================================
# Power transformation on block D
# =============================================================================

# Yeo Johnson transformation 
pt_yeo = PowerTransformer(method='yeo-johnson')
dataD_yeo = pt_yeo.fit_transform(dataDstd)
dataDYeo = pd.DataFrame(dataD_yeo, columns=dataD.columns, index=dataD.index)


# BoxCox transformation
pt_box = PowerTransformer(method='box-cox')
dataD_box = pt_box.fit_transform(dataDmm)
dataDBox = pd.DataFrame(dataD_box, columns=dataD.columns, index=dataD.index)




# Separating train and test data
# -----------------------------------------------------------------------------

test_index = pd.read_csv('../data/blocks/test_subjects.csv')
test_index = test_index.iloc[:, 1]


# Training data

xA = dataAsc.drop(test_index)
yA = np.array(targetAll.loc[xA.index]).reshape(-1,)
yA_ = targetAll.loc[xA.index]

xB = dataBsc.drop(test_index)
yB = np.array(targetAll.loc[xB.index]).reshape(-1,)
yB_ = targetAll.loc[xB.index]

xC = dataCsc.drop(test_index)
yC = np.array(targetAll.loc[xC.index]).reshape(-1,)
yC_ = targetAll.loc[xC.index]

xD = dataDsc.drop(test_index)
yD = np.array(targetAll.loc[xD.index]).reshape(-1,)
yD_ = targetAll.loc[xD.index]

xE = dataEsc.drop(test_index)
yE = np.array(targetAll.loc[xE.index]).reshape(-1,)
yE_ = targetAll.loc[xE.index]

xF = dataFsc.drop(test_index)
yF = np.array(targetAll.loc[xF.index]).reshape(-1,)
yF_ = targetAll.loc[xF.index]

xE2 = dataE2sc.drop(test_index)
yE2 = np.array(targetAll.loc[xE2.index]).reshape(-1,)
yE2_ = targetAll.loc[xE2.index]

xF2 = dataF2sc.drop(test_index)
yF2 = np.array(targetAll.loc[xF2.index] ).reshape(-1,) 
yF2_ = targetAll.loc[xF2.index]           


# Test data 

xA_test = dataAsc.loc[test_index]
yA_test = np.array(targetAll.loc[xA_test.index]).reshape(-1,)

xB_test = dataBsc.loc[test_index]
yB_test = np.array(targetAll.loc[xB_test.index]).reshape(-1,)

xC_test = dataCsc.loc[test_index]
yC_test = np.array(targetAll.loc[xC_test.index]).reshape(-1,)

xD_test = dataDsc.loc[test_index]
yD_test = np.array(targetAll.loc[xD_test.index]).reshape(-1,)

xE_test = dataEsc.loc[test_index]
yE_test = np.array(targetAll.loc[xE_test.index]).reshape(-1,)

xF_test = dataFsc.loc[test_index]
yF_test = np.array(targetAll.loc[xF_test.index]).reshape(-1,)

xE2_test = dataE2sc.loc[test_index]
yE2_test = np.array(targetAll.loc[xE2_test.index]).reshape(-1,)

xF2_test = dataF2sc.loc[test_index]
yF2_test = np.array(targetAll.loc[xF2_test.index]).reshape(-1,)




# Lists of the data and names
Xtrain = [xA, xB, xC, xD, xE, xF, xE2, xF2]

Ytrain = [yA, yB, yC, yD, yE, yF, yE2, yF2]

Xtest = [xA_test, xB_test, xC_test, xD_test, 
         xE_test, xF_test, xE2_test, xF2_test]

Ytest = [yA_test, yB_test, yC_test, yD_test, 
         yE_test, yF_test, yE2_test, yF2_test]

block_names = ['BlockA', 'BlockB', 'BlockC', 'BlockD', 
               'BlockE', 'BlockF', 'BlockE2', 'BlockF2']



# =============================================================================
# Define function for model evaluation
# =============================================================================

# evaluate a model with a given number of repeats
def evaluate_model(model, X, y, repeats, splits, metric):
 	
    # prepare the cross-validation procedure
 	cv = RepeatedStratifiedKFold(n_splits=splits, 
                                 n_repeats=repeats, 
                                 random_state=1)
 	
    # evaluate model
 	scores = cross_val_score(model, X, y, 
                             scoring=metric, 
                             cv=cv, 
                             n_jobs=-1,
                             verbose=0)
 	return scores




# =============================================================================
# Evaluating classifiers
# =============================================================================

# Creating scorer for Matthews correlation coefficient
mcc_scorer = make_scorer(matthews_corrcoef)
metric = mcc_scorer
scaler = StandardScaler()
power = PowerTransformer(method='yeo-johnson')



# Creating models
lg_model = LogisticRegression()

aggressive_model = PassiveAggressiveClassifier()

forest_model = RandomForestClassifier()

knn_model = KNeighborsClassifier()

svc_model = SVC()


# List of models and names
models = [lg_model, aggressive_model, forest_model, knn_model, svc_model]
modelnames = ['LogisticRegression', 'PassiveAggressiveClassifier', 
              'RandomForest', 'KNN', 'SVC']



# Defining parameters for grid search 

# Logistic Regression
param_range_lr  = [0.0001, 0.001, 0.01, 0.1, 1.0, 10.0, 100.0, 1000.0]
param_grid_lr   = [{'m__C' : param_range_lr, 
                    'm__solver' : ['lbfgs', 'liblinear']}]


# Passive Aggressive Classifier
param_range_pag   = [0.0001, 0.001, 0.01, 0.1, 1.0, 10.0, 100.0, 1000.0]
param_grid_pag   = [{'m__C' : param_range_pag,
                     'm__loss': ['hinge','squared_hinge']}]


# Random Forest
param_range_forest1   = [20, 40, 60, 80, 100, 120, 140, 160]
param_range_forest2   = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, None]
param_grid_forest  = [{'m__n_estimators' : param_range_forest1, 
                       'm__criterion' : ['gini','entropy'], 
                       'm__max_depth': param_range_forest2}]

# 'm__max_features': ['auto','sqrt', 'log']



# KNN
param_range_knc = [2, 4, 5, 6, 7, 8, 9, 10]
param_grid_knc = [{'m__n_neighbors' : param_range_knc,
                   'm__weights' : ['uniform', 'distance'], 
                   'm__algorithm' : ['auto', 'ball_tree', 'kd_tree', 'brute'],
                   'm__p' : [1, 2]}]



# SVC
param_range_svc1 = [0.0001, 0.001, 0.01, 0.1, 1.0, 10.0, 100.0] 
param_range_svc2 = [0.0001, 0.0005, 0.001, 0.01, 0.05, 0.1, 1.0]         
param_grid_svc  = [{'m__C': param_range_svc1, 'm__kernel': ['linear']},
                   {'m__C': param_range_svc1, 'm__gamma': param_range_svc2, 
                    'm__kernel': ['rbf']}]



# List of the parameters for all the models
params = [param_grid_lr, param_grid_pag, param_grid_forest, 
          param_grid_knc, param_grid_svc]




# =============================================================================
# Grid search
# =============================================================================


# Dictionary for storing results for all the blocks
grid_search_rent = {}


for x, y, name in zip(Xtrain, Ytrain, block_names):
    
    # Dictionary for storing results for all the models
    rent_scores = {}
    
    for model, modelname, param in zip(models, modelnames, params):
        
        # Creating pipeline
        pipeline = Pipeline(steps=[('s', scaler), ('m', model)])
        
        # Grid search
        gs = GridSearchCV(estimator=pipeline, 
                          param_grid=param, 
                          scoring=metric, 
                          cv=5,
                          n_jobs=-1)
        
        gs = gs.fit(x, y)
        best = gs.best_estimator_
        
        # Storing best scores
        rent_scores[modelname] =  [gs.best_score_, gs.best_params_]
        

    grid_search_rent[name] = rent_scores





# =============================================================================
# Checking scores for different values of tau
# =============================================================================

"""
Change data and value for C and L1 ratio for each block.

"""
x = xD.copy()
y = yD.copy()

# Define parameter C for elastic net. 
my_C_params = [0.1]                                                   # Change

# Define l1-ratio for elastic net.  
my_l1_ratios = [0.1]                                                  # Change

# Define setting for RENT
modelRent = RENT.RENT_Classification(data=x,                         
                                     target=y,                       
                                     feat_names=x.columns,           
                                     C=my_C_params, 
                                     l1_ratios=my_l1_ratios,
                                     autoEnetParSel=False,
                                     poly='OFF',
                                     testsize_range=(0.25,0.25),
                                     scoring='mcc',
                                     classifier='logreg',
                                     K=500,
                                     random_state=0,
                                     verbose=1)



# Training the model
modelRent.train()

# Checking performance for different cutoffs for tau1 and tau2 parameter
# of RENT. 
tau = np.arange(0.2, 0.96, 0.05)
selected_features_list = []
tau_scores = {}



for i, value in enumerate(tau):
    print(value)

    scores_ = []
    
    # Selecting features
    selected_features = modelRent.select_features(tau_1_cutoff=value, 
                                                  tau_2_cutoff=value, 
                                                  tau_3_cutoff=0.975)
    features = selected_features
    feature_names = x.columns[features]                              
    selected_features_list.append(feature_names)
    train_data_ = x.iloc[:, features]                                
    
    # Plot selection frequency
    modelRent.plot_selection_frequency()
    # Plot elementary models
    modelRent.plot_elementary_models()
        
    
    scores_std = {}
    
    for model, modelname in zip(models, modelnames):
        print(modelname)
        
        # Creating pipeline
        pipeline = Pipeline(steps=[('m', model)])
        
        # Fitting model with best parameters
        best_parameters = grid_search_rent[name][modelname][1]
        pipeline.set_params(**best_parameters)
        
        # Evaluating model
        rkf_scores = evaluate_model(pipeline,
                                    X=train_data_,  
                                    y=y,                             
                                    repeats=10,
                                    splits=5,
                                    metric=metric) 
        
        # Getting mean scores and storing
        rkf_scores_mean = round(np.mean(rkf_scores),4)
        rkf_scores_std = round(np.std(rkf_scores, ddof=1),4)
        scores_std[modelname] = {'Mean score' : rkf_scores_mean, 
                                 'Std score' : rkf_scores_std}

        tau_scores[i] = scores_std

    
# Creating dataframe and exporting file      
tau_scores_df = pd.DataFrame(tau_scores)
tau_scores_df.to_csv('BlockD_tau_scores.csv')                         # Change



# Visualisation
# -----------------------------------------------------------------------------
# Getting the values to plot

x = tau
lr = [tau_scores_df.loc['LogisticRegression'][x]['Mean score'] 
      for x in range(len(tau))]
pag = [tau_scores_df.loc['PassiveAggressiveClassifier'][x]['Mean score'] 
       for x in range(len(tau))]
rf = [tau_scores_df.loc['RandomForest'][x]['Mean score'] 
      for x in range(len(tau))]
knn = [tau_scores_df.loc['KNN'][x]['Mean score'] 
       for x in range(len(tau))]
svc = [tau_scores_df.loc['SVC'][x]['Mean score'] 
       for x in range(len(tau))]


# Creating figure
fig,ax = plt.subplots(1, figsize=(15, 10))
plt.plot(x, lr, label = 'Logistic Regression', linewidth=3.0)  
plt.plot(x, pag, label = 'Passive Aggressive Classifier', linewidth=3.0)  
plt.plot(x, rf, label = 'Random Forest', linewidth=3.0)  
plt.plot(x, knn, label = 'KNN', linewidth=3.0)  
plt.plot(x, svc, label = 'SVC', linewidth=3.0)  



# Setting ticks and labels
ax.yaxis.set_tick_params(labelsize=20)
ax.xaxis.set_tick_params(labelsize=20, rotation=15)
ax.set_ylabel('Score', fontsize=20)
ax.set_xlabel('Value for tau', fontsize=20)
ax.legend(fontsize=15, loc='lower left')
ax.set_title('Scores with different values for the parameter Tau', fontsize=22)
plt.grid()
plt.show()
fig.savefig('BlockD_tau_plot.pdf', bbox_inches='tight')               # Change