#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 16 22:28:52 2021

@author: charlottolofsson
"""


# =============================================================================
# Modules
# =============================================================================

import pandas as pd
import matplotlib.pyplot as plt


# =============================================================================
# Loading data
# =============================================================================

# Reading the files containing the scores for the different blocks

scoresB = pd.read_csv('material/rent/BlockWise/V/RepeatedRent/b_scores_rent.csv')
scoresB = scoresB.iloc[:, 1]

scoresC = pd.read_csv('material/rent/BlockWise/V/RepeatedRent/c_scores_rent.csv')
scoresC = scoresC.iloc[:, 1]

scoresD = pd.read_csv('material/rent/BlockWise/V/RepeatedRent/d_scores_rent.csv')
scoresD = scoresD.iloc[:, 1]

scoresE = pd.read_csv('material/rent/BlockWise/V/RepeatedRent/e_scores_rent.csv')
scoresE = scoresE.iloc[:, 1]



# Concatenating into one dataframe

scores = pd.concat([scoresB, scoresC, scoresD, scoresE], axis=1)
block_names = ['Block B', 'Block C', 'Block D', 'Block E']
scores.columns = block_names


# =============================================================================
# Plot
# =============================================================================

# Boxplot of the MCC scores for the different blocks

plt.figure()
plt.boxplot(scores, labels = ['Block B', 'Block C', 
                              'Block D', 'Block E'], showmeans=True)
plt.title('MCC test scores ', size=18)
plt.ylabel('Score', size=16)
plt.savefig('scores_box.png')
plt.show()

