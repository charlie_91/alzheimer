#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep  1 13:34:45 2021

@author: charlottolofsson
"""


# =============================================================================
# Modules
# =============================================================================

# Data handling and plotting
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# Preprocessing and models
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import PowerTransformer
from RENT import RENT, stability

# Functions from other scripts
import sys
sys.path.append('../code/functions')
from rent_function import rent


# =============================================================================
# Loading the data
# =============================================================================

# Block A
dataA = pd.read_csv('../data/dataA.csv', index_col=0)
dataA_dummy = pd.get_dummies(dataA)
dataA_descr = dataA_dummy.describe()

# Block B
dataB = pd.read_csv('../data/dataB.csv', index_col=0)
dataB_dummy = pd.get_dummies(dataB)
dataB_descr = dataB_dummy.describe()

# Block C
dataC = pd.read_csv('../data/dataC.csv', index_col=0)
dataC_descr = dataC.describe()

# Block D
dataD = pd.read_csv('../data/dataD.csv', index_col=0)
dataD_descr = dataD.describe()

# Block E
dataE = pd.read_csv('../data/dataE.csv', index_col=0)
dataE_descr = dataE.describe()

# Block F
dataF = pd.read_csv('../data/dataF.csv', index_col=0)
dataF_descr = dataF.describe()

# Block E2
dataE2 = pd.read_csv('../data/dataE2.csv', index_col=0)
dataE2_descr = dataE2.describe()

# Block F
dataF2 = pd.read_csv('../data/dataF2.csv', index_col=0)
dataF2_descr = dataF2.describe()

# Target
target_name = 'a'
target = pd.read_csv('../data/targetFull.csv', index_col=0)

targetAll = pd.read_csv('../data/targetAll.csv', index_col=0)




# =============================================================================
# Standardising data separately
# =============================================================================

"""
Standardising separately in order to only transform data in block D.
"""

sc = StandardScaler()
minmax = MinMaxScaler(feature_range=(1,2))


dataAstd = sc.fit_transform(dataA_dummy)
dataAsc = pd.DataFrame(dataAstd, columns=dataA_dummy.columns, 
                       index=dataA_dummy.index)


dataBstd = sc.fit_transform(dataB_dummy)
dataBsc = pd.DataFrame(dataBstd, columns=dataB_dummy.columns, 
                       index=dataB_dummy.index)


dataCstd = sc.fit_transform(dataC)
dataCsc = pd.DataFrame(dataCstd, columns=dataC.columns, 
                       index=dataC.index)


dataDstd = sc.fit_transform(dataD)
dataDsc = pd.DataFrame(dataDstd, columns=dataD.columns, 
                       index=dataD.index)

dataDminmax = minmax.fit_transform(dataD)
dataDmm = pd.DataFrame(dataDminmax, columns=dataD.columns, 
                       index=dataD.index)


dataEstd = sc.fit_transform(dataE)
dataEsc = pd.DataFrame(dataEstd, columns=dataE.columns, 
                       index=dataE.index)


dataFstd = sc.fit_transform(dataF)
dataFsc = pd.DataFrame(dataFstd, columns=dataF.columns, 
                       index=dataF.index)


dataE2std = sc.fit_transform(dataE2)
dataE2sc = pd.DataFrame(dataE2std, columns=dataE2.columns, 
                        index=dataE2.index)


dataF2std = sc.fit_transform(dataF2)
dataF2sc = pd.DataFrame(dataF2std, columns=dataF2.columns, 
                        index=dataF2.index)




# =============================================================================
# Power transformation on block D
# =============================================================================

# Yeo Johnson transformation 
pt_yeo = PowerTransformer(method='yeo-johnson')
dataD_yeo = pt_yeo.fit_transform(dataDstd)
dataDYeo = pd.DataFrame(dataD_yeo, columns=dataD.columns, index=dataD.index)


# BoxCox transformation
pt_box = PowerTransformer(method='box-cox')
dataD_box = pt_box.fit_transform(dataDmm)
dataDBox = pd.DataFrame(dataD_box, columns=dataD.columns, index=dataD.index)




# Separating train and test data
# -----------------------------------------------------------------------------

test_index = pd.read_csv('../data/test_subjects.csv')
test_index = test_index.iloc[:, 1]


# Training data

xA = dataAsc.drop(test_index)
yA = np.array(targetAll.loc[xA.index]).reshape(-1,)
yA_ = targetAll.loc[xA.index]

xB = dataBsc.drop(test_index)
yB = np.array(targetAll.loc[xB.index]).reshape(-1,)
yB_ = targetAll.loc[xB.index]

xC = dataCsc.drop(test_index)
yC = np.array(targetAll.loc[xC.index]).reshape(-1,)
yC_ = targetAll.loc[xC.index]

xD = dataDsc.drop(test_index)
yD = np.array(targetAll.loc[xD.index]).reshape(-1,)
yD_ = targetAll.loc[xD.index]

xE = dataEsc.drop(test_index)
yE = np.array(targetAll.loc[xE.index]).reshape(-1,)
yE_ = targetAll.loc[xE.index]

xF = dataFsc.drop(test_index)
yF = np.array(targetAll.loc[xF.index]).reshape(-1,)
yF_ = targetAll.loc[xF.index]

xE2 = dataE2sc.drop(test_index)
yE2 = np.array(targetAll.loc[xE2.index]).reshape(-1,)
yE2_ = targetAll.loc[xE2.index]

xF2 = dataF2sc.drop(test_index)
yF2 = np.array(targetAll.loc[xF2.index] ).reshape(-1,) 
yF2_ = targetAll.loc[xF2.index]           


# Test data 

xA_test = dataAsc.loc[test_index]
yA_test = np.array(targetAll.loc[xA_test.index]).reshape(-1,)

xB_test = dataBsc.loc[test_index]
yB_test = np.array(targetAll.loc[xB_test.index]).reshape(-1,)

xC_test = dataCsc.loc[test_index]
yC_test = np.array(targetAll.loc[xC_test.index]).reshape(-1,)

xD_test = dataDsc.loc[test_index]
yD_test = np.array(targetAll.loc[xD_test.index]).reshape(-1,)

xE_test = dataEsc.loc[test_index]
yE_test = np.array(targetAll.loc[xE_test.index]).reshape(-1,)

xF_test = dataFsc.loc[test_index]
yF_test = np.array(targetAll.loc[xF_test.index]).reshape(-1,)

xE2_test = dataE2sc.loc[test_index]
yE2_test = np.array(targetAll.loc[xE2_test.index]).reshape(-1,)

xF2_test = dataF2sc.loc[test_index]
yF2_test = np.array(targetAll.loc[xF2_test.index]).reshape(-1,)




# Lists of the data and names
Xtrain = [xA, xB, xC, xD, xE, xF, xE2, xF2]

Ytrain = [yA, yB, yC, yD, yE, yF, yE2, yF2]

Xtest = [xA_test, xB_test, xC_test, xD_test, 
         xE_test, xF_test, xE2_test, xF2_test]

Ytest = [yA_test, yB_test, yC_test, yD_test, 
         yE_test, yF_test, yE2_test, yF2_test]

block_names = ['BlockA', 'BlockB', 'BlockC', 'BlockD', 
               'BlockE', 'BlockF', 'BlockE2', 'BlockF2']




# =============================================================================
# RENT
# =============================================================================


# Define a range of regularisation parameters C for elastic net. 
C_params = [0.01, 0.1, 1, 10]

# Define a reange of l1-ratios for elastic net.  
l1_ratios = [0, 0.1, 0.25, 0.5, 0.75, 0.9, 1]


rent_features = {}
rent_feature_names = {}

for x, y, name in zip(Xtrain, Ytrain, block_names):
    print(name)

    
    modelRent = RENT.RENT_Classification(data=x, 
                                         target=y, 
                                         feat_names=x.columns, 
                                         C=C_params, 
                                         l1_ratios=l1_ratios,
                                         autoEnetParSel=False,
                                         poly='OFF',
                                         testsize_range=(0.25,0.25),
                                         scoring='mcc',
                                         classifier='logreg',
                                         K=500,
                                         random_state=0,
                                         verbose=1)

    # Training the model
    modelRent.train()
    
    # Getting best parameters
    params = modelRent.get_enet_params()
    print(params)
    
    # Selected features
    selected_features = modelRent.select_features(tau_1_cutoff=0.80, 
                                                  tau_2_cutoff=0.80, 
                                                  tau_3_cutoff=0.975)
    
    # Summary
    summary = modelRent.get_summary_criteria()
    
    
    # Identify feature names
    feature_names = x.columns[selected_features]
    
    rent_features[name] = selected_features
    rent_feature_names[name] = feature_names
    
    
    # Score  matrices
    cv_score, cv_zeros, cv_harmonic_mean = modelRent.get_enetParam_matrices()
    # cv_score, cv_zeros, cv_harmonic_mean = modelRent.get_cv_matrices()
    
    # Visualising
    # CV score
    # -------------------------------------------------------------------------
    scores_array = np.array(cv_score, dtype=float)
    scores_arr = np.round(scores_array, 4)
    
    
    fig, ax = plt.subplots(figsize=(30,10))
    im = ax.imshow(scores_arr)
    
    # Setting ticks and ticklabels
    ax.set_xticks(np.arange(len(cv_score.columns)))
    ax.set_yticks(np.arange(len(cv_score.index)))
    ax.set_xticklabels(cv_score.columns)
    ax.set_yticklabels(cv_score.index)
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    
    # Loop over data dimensions and create text annotations.
    for i in range(len(cv_score.index)):
        for j in range(len(cv_score.columns)):
            text = ax.text(j, i, scores_arr[i, j],
                           ha="center", va="center", color="w")
    
    ax.set_title('scores')
    plt.savefig('score_' + name + '.png')
    plt.show()
    
    
    # CV zeros
    # -------------------------------------------------------------------------
    zeroes_array = np.array(cv_zeros, dtype=float)
    zeroes_arr = np.round(zeroes_array, 4)
    
    
    fig, ax = plt.subplots(figsize=(30,10))
    im = ax.imshow(zeroes_arr)
    
    # Setting ticks and ticklabels
    ax.set_xticks(np.arange(len(cv_zeros.columns)))
    ax.set_yticks(np.arange(len(cv_zeros.index)))
    ax.set_xticklabels(cv_zeros.columns)
    ax.set_yticklabels(cv_zeros.index)
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    
    # Loop over data dimensions and create text annotations.
    for i in range(len(cv_zeros.index)):
        for j in range(len(cv_zeros.columns)):
            text = ax.text(j, i, zeroes_arr[i, j],
                           ha="center", va="center", color="w")
    
    ax.set_title('zeroes')
    plt.savefig('zeroes_' + name + '.png')
    plt.show()
    
    
    
    # CV harmonic mean
    # -------------------------------------------------------------------------
    harmonic_mean_array = np.array(cv_harmonic_mean, dtype=float)
    harmonic_mean_arr = np.round(harmonic_mean_array, 4)
    
    
    fig, ax = plt.subplots(figsize=(30,10))
    im = ax.imshow(harmonic_mean_arr)
    
    # Setting ticks and ticklabels
    ax.set_xticks(np.arange(len(cv_harmonic_mean.columns)))
    ax.set_yticks(np.arange(len(cv_harmonic_mean.index)))
    ax.set_xticklabels(cv_harmonic_mean.columns)
    ax.set_yticklabels(cv_harmonic_mean.index)
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    
    # Loop over data dimensions and create text annotations.
    for i in range(len(cv_harmonic_mean.index)):
        for j in range(len(cv_harmonic_mean.columns)):
            text = ax.text(j, i, harmonic_mean_arr[i, j],
                           ha="center", va="center", color="w")
    
    ax.set_title('harmonic_mean')
    plt.savefig('harmonic_mean_' + name + '.png')
    plt.show()

    