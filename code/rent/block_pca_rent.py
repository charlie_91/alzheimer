#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 17 14:55:44 2021

@author: charlottolofsson
"""


# =============================================================================
# Modules
# =============================================================================

# Data handling and plotting
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# Preprocessing and models
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from RENT import RENT, stability

# Functions from other scripts
import sys
sys.path.append('../code/functions')
from pca import pca


# =============================================================================
# Loading the data
# =============================================================================

"""
Loading the data and creating new dataframes with the features selected
from RENT. 
"""


# Block A
dataA = pd.read_csv('../data/blocks/dataA.csv', index_col=0)
dataA_dummy = pd.get_dummies(dataA)

# Block B
dataB = pd.read_csv('../data/blocks/dataB.csv', index_col=0)
dataB_dummy = pd.get_dummies(dataB)
B_features = ['age', 'bl_apoe_max_E4/E4', 'bl_apoe_max_E3/E4', 'bl_apoe_max_E2/E4',
              'smok', 'degree1_dem', 'cardiovascular_disease', 'bl_apoe_max_E2/E3', 
              'edu_level', 'bp_recum_sys']
DataB = dataB_dummy.loc[:, B_features]

# Block C
dataC = pd.read_csv('../data/blocks/dataC.csv', index_col=0)
C_features = ['mmse_total', 'cerad_recall', 'cowat_tscore', 'tmtb_sec']
DataC = dataC.loc[:, C_features]

# Block D
dataD = pd.read_csv('../data/blocks/dataD.csv', index_col=0)
D_features = ['PSMD', 'WMHo_rV', 'LesO', 'LesF']
# D_features = ['PSMD', 'WMHo_rV', 'LesO', 'LesF', 'LesT', 'LesP']
DataD = dataD.loc[:, D_features]

# Block E
dataE = pd.read_csv('../data/blocks/dataE.csv', index_col=0)
E_features = ['ERC.combat_ATN', 'PHC.combat_ATN', 
              'MISC.combat_ATN', 'Anterior_hippocampus.combat_ATN']
# 'Posterior_hippocampus.combat_ATN'
# 'Br36.combat_ATN', 'Br35.combat_ATN',
DataE = dataE.loc[:, E_features]


# Target
target_name = 'a'
target = pd.read_csv('../data/blocks/targetFull.csv', index_col=0)

targetAll = pd.read_csv('../data/blocks/targetAll.csv', index_col=0)



# =============================================================================
# Standardising data 
# =============================================================================

"""
Standardising separately in order to only transform data in block D.
"""

sc = StandardScaler()


dataAstd = sc.fit_transform(dataA_dummy)
dataAsc = pd.DataFrame(dataAstd, columns=dataA_dummy.columns, 
                       index=dataA_dummy.index)


dataBstd = sc.fit_transform(DataB)
dataBsc = pd.DataFrame(dataBstd, columns=DataB.columns, 
                       index=DataB.index)


dataCstd = sc.fit_transform(DataC)
dataCsc = pd.DataFrame(dataCstd, columns=DataC.columns, 
                       index=DataC.index)


dataDstd = sc.fit_transform(DataD)
dataDsc = pd.DataFrame(dataDstd, columns=DataD.columns, 
                       index=DataD.index)


dataEstd = sc.fit_transform(DataE)
dataEsc = pd.DataFrame(dataEstd, columns=DataE.columns, 
                       index=DataE.index)


# Separating train and test data
# -----------------------------------------------------------------------------

test_index = pd.read_csv('../data/blocks/test_subjects.csv')
test_index = test_index.iloc[:, 1]


# Training data

xA = dataAsc.drop(test_index)
yA = np.array(targetAll.loc[xA.index]).reshape(-1,)
yA_ = targetAll.loc[xA.index]

xB = dataBsc.drop(test_index)
yB = np.array(targetAll.loc[xB.index]).reshape(-1,)
yB_ = targetAll.loc[xB.index]

xC = dataCsc.drop(test_index)
yC = np.array(targetAll.loc[xC.index]).reshape(-1,)
yC_ = targetAll.loc[xC.index]

xD = dataDsc.drop(test_index)
yD = np.array(targetAll.loc[xD.index]).reshape(-1,)
yD_ = targetAll.loc[xD.index]

xE = dataEsc.drop(test_index)
yE = np.array(targetAll.loc[xE.index]).reshape(-1,)
yE_ = targetAll.loc[xE.index]




# =============================================================================
# Incorrectly classified
# =============================================================================

"""
Making lists of the incorrectly classified patient using the new index for
splitting. Making separate dataframes from the incorrectly- and correctly
classified. Creating a new colum where 1 is for correctly classified and
0 is for incorrectly classified before concatenating the dataframes.
"""


# Block B
icBD = pd.read_csv('material/Rent/BlockWise/V/misclassified/incorrectly_classified_b.csv')
icBD = icBD.set_index('idx')
icBD['classified'] = [0 for x in range(len(icBD.index))]
ccBD = pd.read_csv('material/Rent/BlockWise/V/misclassified/correctly_classified_b.csv')
ccBD = ccBD.set_index('idx')
ccBD['classified'] = [1 for x in range(len(ccBD.index))]

BD = pd.concat([icBD, ccBD], axis=0)



# Block C
icCD = pd.read_csv('material/Rent/BlockWise/V/misclassified/incorrectly_classified_c.csv')
icCD = icCD.set_index('idx')
icCD['classified'] = [0 for x in range(len(icCD.index))]
ccCD = pd.read_csv('material/Rent/BlockWise/V/misclassified/correctly_classified_c.csv')
ccCD = ccCD.set_index('idx')
ccCD['classified'] = [1 for x in range(len(ccCD.index))]

CD = pd.concat([icCD, ccCD], axis=0)



# Block D
icDD = pd.read_csv('material/Rent/BlockWise/V/misclassified/incorrectly_classified_d.csv')
icDD = icDD.set_index('idx')
icDD['classified'] = [0 for x in range(len(icDD.index))]
ccDD = pd.read_csv('material/Rent/BlockWise/V/misclassified/correctly_classified_d.csv')
ccDD = ccDD.set_index('idx')
ccDD['classified'] = [1 for x in range(len(ccDD.index))]

DD = pd.concat([icDD, ccDD], axis=0)



# Block E
icED = pd.read_csv('material/Rent/BlockWise/V/misclassified/incorrectly_classified_e.csv')
icED = icED.set_index('idx')
icED['classified'] = [0 for x in range(len(icED.index))]
ccED = pd.read_csv('material//Rent/BlockWise/V/misclassified/correctly_classified_e.csv')
ccED = ccED.set_index('idx')
ccED['classified'] = [1 for x in range(len(ccED.index))]

ED = pd.concat([icED, ccED], axis=0)



# =============================================================================
# PCA
# =============================================================================

"""
Performing PCA on the blocks separately and plotting the scores. 
Misclassified and correctly classified samples from RENT analysis 
are given different colors.
"""


# Data
X = ED.copy()                                                     # Change!


# Standardising
sc = StandardScaler()
xstd = sc.fit_transform(X)
Xstd = pd.DataFrame(xstd, columns=X.columns, 
                       index=X.index)

# Remving column with misclassified - "target".
Xstd = Xstd.drop(['classified'], axis=1)


# PCA with 2 components
pcamodel = PCA(n_components=2)
principalComponents = pcamodel.fit_transform(Xstd)
principalDf = pd.DataFrame(data = principalComponents, 
                           columns = ['principal component 1', 
                                      'principal component 2'],
                           index=Xstd.index)

ex_var = pcamodel.explained_variance_ratio_
ex_var1 = round(ex_var[0]*100,2)
ex_var2 = round(ex_var[1]*100,2)

# Creating dataframe with the PC's and "target".
finalDf = pd.concat([principalDf, X[['classified']]], axis = 1)


# Creating plot
fig = plt.figure(figsize = (11,8))
ax = fig.add_subplot(1,1,1) 
ax.set_xlabel('Principal Component 1 ({0}%)'.format(ex_var1), fontsize = 15)
ax.set_ylabel('Principal Component 2 ({0}%)'.format(ex_var2), fontsize = 15)
ax.set_title('Block E', fontsize = 22)                            # Change!
targets = [1, 0]
colors = ['g', 'r']
for target, color in zip(targets,colors):
    indicesToKeep = finalDf['classified'] == target
    ax.scatter(finalDf.loc[indicesToKeep, 'principal component 1']
               , finalDf.loc[indicesToKeep, 'principal component 2'],
               label = True
               , c = color
               , s = 50)
    
ax.legend(['Correctly classified', 'Incorrectly classified'], fontsize=15)
ax.grid()
fig.savefig('pca_blockE.pdf')                                     # Change!


# =============================================================================
# PCA with hoggorm
# =============================================================================

# Block B
xB = BD.drop(['classified'], axis=1)  
pcaB = pca(xB, components=2, plot=True)

# Block C
xC = CD.drop(['classified'], axis=1)
pcaC = pca(xC, components=2, plot=True)

# Block D
xD = DD.drop(['classified'], axis=1)
pcaD = pca(xD, components=2, plot=True)

# Block E
xE = ED.drop(['classified'], axis=1)
pcaE = pca(xE, components=2, plot=True)



